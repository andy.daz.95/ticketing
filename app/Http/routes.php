<?php

Route::group(['middleware' => 'web'], function () {
	Route::get('/', 'AuthController@index');
	Route::post('/login', 'AuthController@login');

	Route::group(['middleware' => 'login'], function () {
		Route::get('/home', 'HomeController@index');
		Route::get('/logout', 'AuthController@logout');
		Route::get('/search/penggunakategori/{id}', 'PenggunaKategoriController@search');
		Route::get('/search/pengguna/{id}', 'PenggunaController@search');
		Route::get('/search/travel/{id}', 'TravelController@search');
		Route::get('/search/guide/{id}', 'GuideController@search');
		Route::get('/profile/{id}', 'PenggunaController@show');
		Route::get('/guide/{id}', 'GuideController@show');
		Route::get('/guide/search/{id}', 'GuideController@search');
		Route::get('/guide/find/{nama?}/{referensi?}', 'GuideController@find');
		Route::post('/laporantransaksitiketgrafik', 'TransaksiTiketController@grafik');
		Route::post('/laporantransaksitiket', 'TransaksiTiketController@detail');
		Route::post('/laporantransaksitiket/excel', 'TransaksiTiketController@excel');
		Route::post('/laporantransaksikomisiguide', 'TransaksiKomisiGuideController@detail');
		Route::post('/laporantransaksikomisiguide/excel', 'TransaksiKomisiGuideController@excel');
		Route::post('/laporantransaksikomisiguide/komisi/{id}', 'TransaksiKomisiGuideController@komisi');
		Route::post('/laporanlog', 'LogController@detail');
		Route::post('/laporanlog/excel', 'LogController@excel');
		Route::post('/pendapatanguide', 'GuideController@filter');
		Route::post('/pendapatanguide/excel', 'GuideController@excel');
		Route::post('/guide/store', 'GuideController@store');
		Route::post('/guide/primary', 'GuideController@primary');
		Route::post('/guide/update/{id}', 'GuideController@update');
		Route::put('/pengguna/profile/{id}', 'PenggunaController@profile');
		Route::delete('/guide/delete/{id}', 'GuideController@destroy');
	});

	Route::group(['middleware' => 'administrator'], function () {
		Route::get('/penggunakategori', 'PenggunaKategoriController@index');
		Route::get('/pengguna', 'PenggunaController@index');
		Route::get('/pengguna/{id}', 'PenggunaController@show');
		Route::get('/pengguna/find/{id}', 'PenggunaController@find');
		Route::get('/kategoritiket', 'KategoriTiketController@index');
		Route::get('/kategoritiket/{id}', 'KategoriTiketController@show');
		Route::get('/log', 'LogController@index');
		Route::get('/log/{id}', 'LogController@show');
		Route::post('/pengguna/store', 'PenggunaController@store');
		Route::post('/log/store', 'LogController@store');
		Route::post('/kategoritiket/store', 'KategoriTiketController@store');
		Route::put('/kategoritiket/update/{id}', 'KategoriTiketController@update');
		Route::put('/pengguna/update/{id}', 'PenggunaController@update');
		Route::put('/pengguna/reset/{id}', 'PenggunaController@reset');
		Route::put('/log/update/{id}', 'LogController@update');
		Route::delete('/log/delete/{id}', 'LogController@destroy');
		Route::delete('/pengguna/delete/{id}', 'PenggunaController@destroy');
		Route::delete('/kategoritiket/delete/{id}', 'KategoriTiketController@destroy');
	});

	Route::group(['middleware' => 'manager'], function () {
		Route::get('/laporantransaksitiketgrafik', 'TransaksiTiketController@presentation');
		Route::get('/laporantransaksitiket', 'TransaksiTiketController@report');
		Route::get('/laporantransaksikomisiguide', 'TransaksiKomisiGuideController@report');
		Route::get('/laporanlog', 'LogController@report');
	});

	Route::group(['middleware' => 'ticket'], function () {
		Route::get('/transaksitiket', 'TransaksiTiketController@index');
		Route::get('/transaksitiket/{id}', 'TransaksiTiketController@show');
		Route::get('/transaksitiket/search/guide/{id}', 'TransaksiTiketController@search');
		Route::get('/transaksitiket/free/guide/{id}', 'TransaksiTiketController@free');
		Route::get('/transaksitiketvoid', 'TransaksiTiketController@void');
		Route::get('/tiket/laporantransaksitiket', 'TransaksiTiketController@report');
		Route::post('/transaksitiket/store', 'TransaksiTiketController@store');
		Route::post('/transaksitiketvoid', 'TransaksiTiketController@tiketvoid');
		Route::post('/transaksitiketvoidproses', 'TransaksiTiketController@tiketvoidproses');
		Route::post('/transaksitiket/lastinvoice', 'TransaksiTiketController@lastinvoice');
		Route::post('/transaksitiket/datalastinvoice', 'TransaksiTiketController@datalastinvoice');
		Route::put('/transaksitiket/update/{id}', 'TransaksiTiketController@update');
		Route::delete('/transaksitiket/delete/{id}', 'TransaksiTiketController@destroy');
	});

	Route::group(['middleware' => 'gate'], function () {
	});

	Route::group(['middleware' => 'komisi'], function () {
		Route::get('/transaksikomisiguide', 'TransaksiKomisiGuideController@index');
		Route::get('/transaksikomisiguide/{id}', 'TransaksiKomisiGuideController@show');
		Route::get('/transaksikomisiguide/search/guide/{id}', 'TransaksiKomisiGuideController@search');
		Route::get('/komisi/laporantransaksikomisiguide', 'TransaksiKomisiGuideController@report');
		Route::get('/guide', 'GuideController@index');
		Route::get('/pendapatanguide', 'GuideController@pendapatan');
		Route::post('/transaksikomisiguide/store', 'TransaksiKomisiGuideController@store');
		Route::put('/transaksikomisiguide/update/{id}', 'TransaksiKomisiGuideController@update');
		Route::put('/komisi/transaksitiket/update/{id}', 'TransaksiTiketController@update');
		Route::delete('/transaksikomisiguide/delete/{id}', 'TransaksiKomisiGuideController@destroy');
	});

	Route::group(['middleware' => 'finance'], function () {
		Route::get('/travel', 'TravelController@index');
		Route::get('/travel/{id}', 'TravelController@show');
		Route::get('/travel/find/{id}', 'TravelController@find');
		Route::get('/travel/search/{id}', 'TravelController@search');
		Route::get('/finance/guide', 'GuideController@index');
		Route::get('/finance/transaksikomisiguide', 'TransaksiKomisiGuideController@finance');
		Route::get('/finance/transaksikomisiguide/{id}', 'TransaksiKomisiGuideController@show');
		Route::get('/finance/transaksikomisiguide/search/guide/{id}', 'TransaksiKomisiGuideController@find');
		Route::get('/finance/laporantransaksitiketgrafik', 'TransaksiTiketController@presentation');
		Route::get('/finance/laporantransaksitiket', 'TransaksiTiketController@report');
		Route::get('/finance/laporantransaksikomisiguide', 'TransaksiKomisiGuideController@report');
		Route::get('/finance/laporanlog', 'LogController@report');
		Route::get('/finance/pendapatanguide', 'GuideController@pendapatan');
		Route::post('/travel/store', 'TravelController@store');
		Route::post('/finance/pendapatanguide', 'GuideController@filter');
		Route::post('/finance/transaksikomisiguide/store', 'TransaksiKomisiGuideController@store');
		Route::put('/finance/transaksikomisiguide/update/{id}', 'TransaksiKomisiGuideController@update');
		Route::put('/travel/update/{id}', 'TravelController@update');
		Route::put('/finance/transaksitiket/update/{id}', 'TransaksiTiketController@update');
		Route::delete('/finance/transaksikomisiguide/delete/{id}', 'TransaksiKomisiGuideController@destroy');
		Route::delete('/travel/delete/{id}', 'TravelController@destroy');
	});
});


// this line below is used to log query to laravel.log, 
// do not enable in production
// \DB::listen(function($sql) {
//     \Log::info($sql->sql);
//     \Log::info($sql->bindings);
//     \Log::info($sql->time);
// });
