<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Travel;
use App\Guide;
use App\Http\Requests;
use App\Repositories\LogRepository;
use App\Repositories\TicketingRepository;
use DB;
use Excel;

class GuideController extends Controller
{
	public function __construct()
	{
	}

	public function index(Request $request)
	{
		$request->session()->flash('page', 0);
		$guides = DB::table('dt_travels')
					->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					->where('dt_guides.status_guide', '<>', 2)
					->select('dt_travels.*', 'dt_guides.*')
					->paginate(25);

		return view('administrators.guide',
		[
			'travels'=>Travel::where('status_travel', 1)->paginate(25),
			'guides'=>$guides
		]);
	}

	public function store(Request $request)
	{
		$id = TicketingRepository::id();

		if($request->hasFile('identitas'))
		{
			$request->file('identitas')
					->move('identitas', $id.'.'.$request->file('identitas')->getClientOriginalExtension());
		}

		$guide = new Guide;
		$guide->id_guide = $id;
		$guide->id_referensi = $request->has('referensi') ? $request->referensi : '';
		$guide->no_identitas = $request->has('no') ? $request->no : '';
		$guide->nama = $request->has('nama') ? $request->nama : '';
		$guide->jenis_kelamin = $request->has('kelamin') ? $request->kelamin : '';
		$guide->tempat_lahir = $request->has('tempat') ? $request->tempat : '';
		$guide->tgl_lahir = $request->has('tanggal') ? $request->tanggal : '';
		$guide->alamat = $request->has('alamat') ? $request->alamat : '';
		$guide->no_hp = $request->has('hp') ? $request->hp : '';
		$guide->email = $request->has('email') ? $request->email : '';
		$guide->jenis_guide = $request->has('guide') ? $request->guide : '';
		$guide->id_travel = $request->has('travel') ? $request->travel : '';
		$guide->jenis_transaksi = $request->has('transaksi') ? $request->transaksi : '';
		$guide->no_rek = $request->has('rekening') ? $request->rekening : '';
		$guide->bank = $request->has('bank') ? $request->bank : '';
		$guide->ktp = $request->has('ktp') ? $request->ktp : '';
		$guide->sim_a = $request->has('sima') ? $request->sima : '';
		$guide->sim_b = $request->has('simb') ? $request->simb : '';
		$guide->sim_c = $request->has('simc') ? $request->simc : '';
		$guide->lisensi = $request->has('lisensi') ? $request->lisensi : '';
		$guide->passport = $request->has('passport') ? $request->passport : '';
		$guide->nama_ibu_kandung = $request->has('ibu') ? $request->ibu : '';
		$guide->status_guide = $request->has('status') ? $request->status : '1';
		$guide->foto = $request->has('foto') ? $request->foto : '';
		$guide->sidik_jari = $request->has('sidik') ? $request->sidik : '';
		$guide->scan_identitas = $request->hasFile('identitas') ? $id.'.'.$request->file('identitas')->getClientOriginalExtension() : '';
		$guide->save();

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menambah Data Guide.';
		$log->store();

		return $id;
	}

	public function primary(Request $request)
	{
		$id = TicketingRepository::id();

		if($request->hasFile('identitas'))
		{
			$request->file('identitas')
					->move('identitas', $id.'.'.$request->file('identitas')->getClientOriginalExtension());
		}

		$tmp = explode('-', $request->tanggal);
		$tanggalLahir = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];

		$guide = new Guide;
		$guide->id_guide = $id;
		$guide->id_referensi = $request->has('referensi') ? $request->referensi : '';
		$guide->no_identitas = $request->has('no') ? $request->no : '';
		$guide->nama = $request->has('nama') ? $request->nama : '';
		$guide->jenis_kelamin = $request->has('kelamin') ? $request->kelamin : '';
		$guide->tempat_lahir = $request->has('tempat') ? $request->tempat : '';
		$guide->tgl_lahir = $request->has('tanggal') ? $tanggalLahir : '';
		$guide->alamat = $request->has('alamat') ? $request->alamat : '';
		$guide->no_hp = $request->has('hp') ? $request->hp : '';
		$guide->email = $request->has('email') ? $request->email : '';
		$guide->jenis_guide = $request->has('guide') ? $request->guide : '';
		$guide->id_travel = $request->has('travel') ? $request->travel : '';
		$guide->jenis_transaksi = $request->has('transaksi') ? $request->transaksi : '';
		$guide->no_rek = $request->has('rekening') ? $request->rekening : '';
		$guide->bank = $request->has('bank') ? $request->bank : '';
		$guide->ktp = $request->has('ktp') ? $request->ktp : '';
		$guide->sim_a = $request->has('sima') ? $request->sima : '';
		$guide->sim_b = $request->has('simb') ? $request->simb : '';
		$guide->sim_c = $request->has('simc') ? $request->simc : '';
		$guide->lisensi = $request->has('lisensi') ? $request->lisensi : '';
		$guide->passport = $request->has('passport') ? $request->passport : '';
		$guide->nama_ibu_kandung = $request->has('ibu') ? $request->ibu : '';
		$guide->status_guide = $request->has('status') ? $request->status : '1';
		$guide->foto = $request->has('foto') ? $request->foto : '';
		$guide->sidik_jari = $request->has('sidik') ? $request->sidik : '';
		$guide->scan_identitas = $request->hasFile('identitas') ? $id.'.'.$request->file('identitas')->getClientOriginalExtension() : '';
		$guide->save();

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menambah Data Guide.';
		$log->store();

		return redirect('guide');
	}

	public function show($id)
	{
		$guides = DB::table('dt_travels')
					->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					->where('dt_guides.id_guide', $id)
					->where('dt_guides.status_guide', '<>', 2)
					->select('dt_travels.*', 'dt_guides.*')
					->first();

		return json_encode($guides);
	}

	public function find($nama = null, $referensi = null)
	{
		$guides = Guide::where('status_guide', '<>', 2);

		if($nama && $nama != "undefined") { $guides->where('nama', 'like', "%$nama%"); }
		if($referensi && $referensi != "undefined") { $guides->where('id_referensi', 'like', "%$referensi%"); }

		return view('administrators.guide',
		[
			'travels'=>Travel::where('status_travel', 1)->paginate(25),
			'guides'=>$guides->paginate(25)
		]);
	}

	public function search($id)
	{
		if($id == 'all')
		{
			return Guide::where('status_guide', '<>', 2)->get();
		}else
		{
			return Guide::where('nama', 'like', "%$id%")->where('status_guide', '<>', 2)->get();
		}
	}

	public function pendapatan(Request $request)
	{
		$request->session()->flash('page', 1);
		$guides = DB::table('dt_travels')
					->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					->leftJoin('dt_transaksi_tikets', 'dt_transaksi_tikets.id_guide', '=', 'dt_guides.id_guide')
					->where('dt_guides.status_guide', '<>', 2)
					->select('dt_travels.nama_travel', 'dt_travels.guide_komisi', 'dt_travels.travel_komisi', 'dt_guides.id_guide', 'dt_guides.id_referensi', 'dt_guides.nama', 'dt_transaksi_tikets.harga', 'dt_transaksi_tikets.status_pembayaran_komisi', DB::raw('IFNULL(SUM(dt_transaksi_tikets.harga * dt_transaksi_tikets.komisi_guide / 100), 0) as pendapatan'))
					->groupBy('dt_travels.nama_travel', 'dt_guides.id_guide', 'dt_guides.nama', 'dt_transaksi_tikets.status_pembayaran_komisi')
					->paginate(25);

		return view('administrators.pendapatanguide', ['guides'=>$guides]);
	}

	public function filter(Request $request)
	{
		$query = DB::table('dt_travels')
					->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					->leftJoin('dt_transaksi_tikets', 'dt_transaksi_tikets.id_guide', '=', 'dt_guides.id_guide')
					->where('dt_guides.status_guide', '<>', 2)
					->whereBetween('dt_transaksi_tikets.wkt_transaksi', [$request->mulai, $request->selesai])
					->select('dt_travels.nama_travel', 'dt_guides.id_guide', 'dt_guides.id_referensi', 'dt_guides.nama', 'dt_transaksi_tikets.status_pembayaran_komisi', DB::raw('SUM(dt_transaksi_tikets.harga * dt_transaksi_tikets.komisi_guide / 100) as pendapatan'))
					->groupBy('dt_travels.nama_travel', 'dt_guides.id_guide', 'dt_guides.nama', 'dt_transaksi_tikets.status_pembayaran_komisi');

		if($request->status != "") { $query->where('dt_transaksi_tikets.status_pembayaran_komisi', $request->status); }

		return $query->get();
	}

	public function excel(Request $request)
	{
		DB::statement(DB::raw('set @row = 0'));
		$query = DB::table('dt_travels')
					->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					->leftJoin('dt_transaksi_tikets', 'dt_transaksi_tikets.id_guide', '=', 'dt_guides.id_guide')
					->where('dt_guides.status_guide', '<>', 2)
					->whereBetween('dt_transaksi_tikets.wkt_transaksi', [TicketingRepository::explodes($request->mulai, $request->waktuMulai), TicketingRepository::explodes($request->selesai, $request->waktuAkhir)])
					->select(DB::raw("@row := @row + 1 AS No,
									  dt_travels.nama_travel AS Travel,
									  dt_guides.id_guide AS IDGuide,
									  dt_guides.nama AS NamaGuide,
									  CASE (dt_transaksi_tikets.status_pembayaran_komisi) WHEN 0 THEN 'Belum Bayar' WHEN 1 THEN 'Sudah Bayar' END AS StatusBayar,
									  SUM(dt_transaksi_tikets.harga * dt_transaksi_tikets.komisi_guide / 100) AS Pendapatan"))
					->groupBy('dt_travels.nama_travel', 'dt_guides.id_guide', 'dt_guides.nama', 'dt_transaksi_tikets.status_pembayaran_komisi');

		if($request->status != "") { $query->where('dt_transaksi_tikets.status_pembayaran_komisi', $request->status); }

		$pendapatanguides = $query->get();

		Excel::create('PendapatanGuide', function($excel) use($pendapatanguides)
		{
			$excel->sheet('Data', function($sheet) use($pendapatanguides)
			{
				$sheet->fromArray(json_decode(json_encode($pendapatanguides), true));
			});
		})->download('xls');
	}

	public function update(Request $request, $id)
	{
		$file = TicketingRepository::id();

		if($request->hasFile('identitas'))
		{
			$request->file('identitas')
					->move('identitas', $file.'.'.$request->file('identitas')->getClientOriginalExtension());
		}

		$tmp = explode('-', $request->tanggal);
		$tglLahir = $tmp[2].'-'.$tmp[1].'-'.$tmp[0];

		Guide::where('id_guide', $id)->update
		([
			'id_referensi'=>$request->referensi,
			'no_identitas'=>$request->no,
			'nama'=>$request->nama,
			'jenis_kelamin'=>$request->kelamin,
			'tempat_lahir'=>$request->tempat,
			'tgl_lahir'=>$tglLahir,
			'alamat'=>$request->alamat,
			'no_hp'=>$request->hp,
			'email'=>$request->email,
			'jenis_guide'=>$request->guide,
			'id_travel'=>$request->travel,
			'jenis_transaksi'=>$request->transaksi,
			'no_rek'=>$request->rekening,
			'bank'=>$request->bank,
			'ktp'=>$request->ktp,
			'sim_a'=>$request->sima,
			'sim_b'=>$request->simb,
			'sim_c'=>$request->simc,
			'lisensi'=>$request->lisensi,
			'passport'=>$request->passport,
			'nama_ibu_kandung'=>$request->ibu,
			'status_guide'=>$request->status,
			'foto'=>$request->has('foto') ? $request->foto : Guide::where('id_guide', $id)->pluck('foto')[0],
			'scan_identitas'=>$request->hasFile('identitas') ? $file.'.'.$request->file('identitas')->getClientOriginalExtension() : Guide::where('id_guide', $id)->pluck('scan_identitas')[0]
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Mengubah Data Guide.';
		$log->store();

		return redirect('guide');
	}

	public function destroy($id)
	{
		Guide::where('id_guide', $id)->update
		([
			'status_guide'=>2
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menghapus Data Guide.';
		$log->store();
	}
}
