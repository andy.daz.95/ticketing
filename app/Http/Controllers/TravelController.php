<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Travel;
use App\Http\Requests;
use App\Repositories\LogRepository;
use App\Repositories\TicketingRepository;

class TravelController extends Controller
{
    public function __construct()
	{
	}

	public function index(Request $request)
	{
		$request->session()->flash('page', 0);
		return view('administrators.travel', ['travels'=>Travel::where('status_travel', 1)->paginate(25)]);
	}

	public function store(Request $request)
	{
		$travel = new Travel;
		$travel->id_travel = TicketingRepository::id();
		$travel->nama_travel = $request->nama;
		$travel->travel_komisi = $request->travel;
		$travel->guide_komisi = $request->guide;
		$travel->bank = $request->bank;
		$travel->no_rek = $request->rekening;
		$travel->atas_nama = $request->atas;
		$travel->alamat_travel = $request->alamat;
		$travel->status_travel = 1;
		$travel->save();

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menambah Data Travel.';
		$log->store();
	}

	public function show($id)
	{
		return Travel::where('id_travel', $id)->first();
	}

	public function find($id)
	{
		return view('administrators.travel', ['travels'=>Travel::where('nama_travel', 'like', "$id%")->where('status_travel', 1)->paginate(25)]);
	}

	public function search($id)
	{
		if($id == 'all')
		{
			return Travel::where('status_travel', 1)->get();
		}

		else
		{
			return Travel::where('nama_travel', 'like', "%$id%")->where('status_travel', 1)->get();
		}
	}

	public function update(Request $request, $id)
	{
		Travel::where('id_travel', $id)->update
		([
			'nama_travel'=>$request->nama,
			'travel_komisi'=>$request->travel,
			'guide_komisi'=>$request->guide,
			'bank'=>$request->bank,
			'no_rek'=>$request->rekening,
			'atas_nama'=>$request->atas,
			'alamat_travel'=>$request->alamat,
			'status_travel'=>1
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Mengubah Data Travel.';
		$log->store();
	}

	public function destroy($id)
	{
		Travel::where('id_travel', $id)->update
		([
			'status_travel'=>0
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menghapus Data Travel.';
		$log->store();
	}
}
