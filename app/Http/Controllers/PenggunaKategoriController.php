<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PenggunaKategori;
use App\Http\Requests;

class PenggunaKategoriController extends Controller
{
	public function __construct()
	{
	}

	public function index(Request $request)
	{
		$request->session()->flash('page', 0);
		return view('administrators.penggunakategori', ['penggunakategoris'=>PenggunaKategori::paginate(25)]);
	}
}
