<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriTiket;
use App\Http\Requests;
use App\Repositories\LogRepository;

class KategoriTiketController extends Controller
{
	public function __construct()
	{
	}

	public function index(Request $request)
	{
		$request->session()->flash('page', 0);
		return view('administrators.kategoritiket', ['kategoritikets'=>KategoriTiket::where('status_tiket', 1)->where('id_tiket', '<>', 1)->paginate(25)]);
	}

	public function store(Request $request)
	{
		$kategoritiket = new KategoriTiket;
		$kategoritiket->kategori_tiket = $request->kategori;
		$kategoritiket->harga = $request->harga;
		$kategoritiket->diskon = $request->diskon;
		$kategoritiket->komisi_guide = $request->komisi;
		$kategoritiket->status_tiket = $request->status;
		$kategoritiket->save();

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menambah Data Kategori Tiket.';
		$log->store();
	}

	public function show($id)
	{
		return KategoriTiket::where('id_tiket', $id)->first();
	}

	public function update(Request $request, $id)
	{
		KategoriTiket::where('id_tiket', $id)->update
		([
			'kategori_tiket'=>$request->kategori,
			'harga'=>$request->harga,
			'diskon'=>$request->diskon,
			'komisi_guide'=>$request->komisi,
			'status_tiket'=>$request->status
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Mengubah Data Kategori Tiket.';
		$log->store();
	}

	public function destroy($id)
	{
		KategoriTiket::where('id_tiket', $id)->update
		([
			'status_tiket'=>0
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menghapus Data Kategori Tiket.';
		$log->store();
	}
}
