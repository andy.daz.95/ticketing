<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengguna;
use App\Http\Requests;
use App\Repositories\LogRepository;
use App\Repositories\TicketingRepository;
use DB;

class AuthController extends Controller
{
	public function __construct()
	{
	}

	public function index()
	{
		if(session()->has('login'))
		{
			return view('welcome');
		}

		return view('auth.login');
	}

	public function login(Request $request)
	{
		$pengguna = DB::table('dt_pengguna_kategoris')
					  ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
					  ->where('dt_penggunas.nama_pengguna', $request->username)
					  ->where('dt_penggunas.pass_enkrip', TicketingRepository::hash($request->password))
					  ->where('dt_penggunas.status_pengguna', 1)
					  ->select('dt_pengguna_kategoris.*', 'dt_penggunas.*')
					  ->first();

		if($pengguna)
		{
			$request->session()->put('login', $pengguna->id_pengguna);
			$request->session()->put('role', $pengguna->id_pengguna_kategori);
			$request->session()->put('name', $pengguna->nama_lengkap);
			$request->session()->put('category', $pengguna->pengguna_kategori);

			$log = new LogRepository;
			$log->pengguna = session('login');
			$log->aktivitas = 'Log In Pengguna.';
			$log->store();
		}

		else
		{
			$request->session()->put('failed', 1);
		}

		return redirect()->intended('home');
	}

	public function logout(Request $request)
	{
		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Log Out Pengguna.';
		$log->store();

		$request->session()->forget('login');
		$request->session()->forget('role');

		return redirect()->intended('/');
	}
}
