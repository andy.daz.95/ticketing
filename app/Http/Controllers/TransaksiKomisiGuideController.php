<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Pengguna;
use App\Guide;
use App\KategoriTiket;
use App\TransaksiTiket;
use App\TransaksiKomisiGuide;
use App\Http\Requests;
use App\Repositories\LogRepository;
use App\Repositories\TicketingRepository;
use DB;
use Excel;

class TransaksiKomisiGuideController extends Controller
{
	public function __construct()
	{
	}

	public function index(Request $request)
	{
		$request->session()->flash('page', 1);
		$transaksitikets = DB::table('dt_kategori_tikets')
							 ->join('dt_transaksi_tikets', 'dt_kategori_tikets.id_tiket', '=', 'dt_transaksi_tikets.id_tiket')
							 ->where('dt_transaksi_tikets.id_guide', 'XXX')
							 ->where('dt_transaksi_tikets.status_pembayaran_komisi', 0)
							 ->select('dt_kategori_tikets.*', 'dt_transaksi_tikets.*')
							 ->paginate(25);

		return view('administrators.transaksikomisiguide',
		[
			'guides'=>Guide::where('status_guide', '<>', 2)->get(),
			'kategoritikets'=>KategoriTiket::where('status_tiket', 1)->paginate(25),
			'transaksitikets'=>$transaksitikets
		]);
	}

	public function finance(Request $request)
	{
		$request->session()->flash('page', 1);
		$transaksitikets = DB::table('dt_kategori_tikets')
							 ->join('dt_transaksi_tikets', 'dt_kategori_tikets.id_tiket', '=', 'dt_transaksi_tikets.id_tiket')
							 ->where('dt_transaksi_tikets.id_guide', 'XXX')
							 ->where('dt_transaksi_tikets.status_pembayaran_komisi', 0)
							 ->select('dt_kategori_tikets.*', 'dt_transaksi_tikets.*')
							 ->paginate(25);

		return view('finances.transaksikomisiguide',
		[
			'guides'=>Guide::where('status_guide', '<>', 2)->get(),
			'kategoritikets'=>KategoriTiket::where('status_tiket', 1)->paginate(25),
			'transaksitikets'=>$transaksitikets
		]);
	}

	public function store(Request $request)
	{
		$id = TicketingRepository::id();

		$transaksikomisiguide = new TransaksiKomisiGuide;
		$transaksikomisiguide->id_transaksi_komisi = $id;
		$transaksikomisiguide->id_guide = $request->guide;
		$transaksikomisiguide->id_pengguna = $request->pengguna;
		$transaksikomisiguide->nilai_transaksi = $request->nilai;
		$transaksikomisiguide->wkt_transaksi = TicketingRepository::date();
		$transaksikomisiguide->status_transaksi = $request->status;
		$transaksikomisiguide->save();

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menambah Transaksi Komisi Guide.';
		$log->store();

		$response = new Response();
		$response->withCookie('komisi', $id);

		return $response;
	}

	public function show($id)
	{
	}

	public function search(Request $request)
	{
		$request->session()->flash('page', 1);
		$travels = DB::table('dt_travels')
					 ->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					 ->where('dt_guides.id_guide', $request->id)
					 ->where('dt_guides.status_guide', '<>', 2)
					 ->select('dt_travels.*', 'dt_guides.*')
					 ->first();

		$guides = DB::table('dt_travels')
					->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					->where('dt_guides.status_guide', '<>', 2)
					->select('dt_travels.*', 'dt_guides.*')
					->paginate(25);

		$transaksitikets = DB::table('dt_kategori_tikets')
							 ->join('dt_transaksi_tikets', 'dt_kategori_tikets.id_tiket', '=', 'dt_transaksi_tikets.id_tiket')
							 ->where('dt_transaksi_tikets.status_pembayaran_komisi', 0)
							 ->where('dt_transaksi_tikets.id_guide', $request->id)
							 ->select('dt_kategori_tikets.*', 'dt_transaksi_tikets.*')
							 ->get();

		return view('administrators.transaksikomisiguide',
		[
			'travels'=>$travels,
			'guides'=>$guides,
			'kategoritikets'=>KategoriTiket::where('status_tiket', 1)->paginate(25),
			'transaksitikets'=>$transaksitikets
		]);
	}

	public function find(Request $request)
	{
		$request->session()->flash('page', 1);
		$travels = DB::table('dt_travels')
					 ->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					 ->where('dt_guides.id_guide', $request->id)
					 ->where('dt_guides.status_guide', '<>', 2)
					 ->select('dt_travels.*', 'dt_guides.*')
					 ->first();

		$guides = DB::table('dt_travels')
					->rightJoin('dt_guides', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
					->where('dt_guides.status_guide', '<>', 2)
					->select('dt_travels.*', 'dt_guides.*')
					->paginate(25);

		$transaksitikets = DB::table('dt_kategori_tikets')
							 ->join('dt_transaksi_tikets', 'dt_kategori_tikets.id_tiket', '=', 'dt_transaksi_tikets.id_tiket')
							 ->where('dt_transaksi_tikets.status_pembayaran_komisi', 0)
							 ->where('dt_transaksi_tikets.id_guide', $request->id)
							 ->select('dt_kategori_tikets.*', 'dt_transaksi_tikets.*')
							 ->get();

		return view('finances.transaksikomisiguide',
		[
			'travels'=>$travels,
			'guides'=>$guides,
			'kategoritikets'=>KategoriTiket::where('status_tiket', 1)->paginate(25),
			'transaksitikets'=>$transaksitikets
		]);
	}

	public function update(Request $request, $id)
	{
	}

	public function destroy($id)
	{
	}

	public function excel(Request $request)
	{
		$query = DB::table('dt_guides')
				   ->join('dt_transaksi_komisi_guides', 'dt_guides.id_guide', '=', 'dt_transaksi_komisi_guides.id_guide')
				   ->leftJoin('dt_travels', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
				   ->whereBetween('dt_transaksi_komisi_guides.wkt_transaksi', [TicketingRepository::explodes($request->mulai, $request->waktuMulai), TicketingRepository::explodes($request->selesai, $request->waktuAkhir)])
				   ->orderBy('dt_transaksi_komisi_guides.wkt_transaksi', 'desc')
				   ->select(DB::raw("dt_transaksi_komisi_guides.id_transaksi_komisi AS IDTransaksiKomisi,
				   					 dt_guides.nama AS NamaGuide,
						   			 dt_travels.nama_travel AS NamaTravel,
						   			 dt_transaksi_komisi_guides.nilai_transaksi AS JumlahBayaran,
						   			 dt_transaksi_komisi_guides.wkt_transaksi AS WaktuTransaksi,
						   			 CASE WHEN (dt_transaksi_komisi_guides.status_transaksi = 1) THEN 'BankTransfer' ELSE 'Cash' END AS JenisPembayaran"));

		if($request->guide != "") { $query->where('dt_transaksi_komisi_guides.id_guide', $request->guide); }
		if($request->komisi != "") { $query->where('dt_transaksi_komisi_guides.id_pengguna', $request->komisi); }

		$transaksikomisiguides = $query->get();

		Excel::create('TransaksiKomisiGuide', function($excel) use($transaksikomisiguides)
		{
			$excel->sheet('Data', function($sheet) use($transaksikomisiguides)
			{
				$sheet->fromArray(json_decode(json_encode($transaksikomisiguides), true));
			});
		})->download('xls');
	}

	public function report(Request $request)
	{
		$request->session()->flash('page', 2);
		return view('administrators.laporantransaksikomisiguide',
		[
			'guides'=>Guide::where('status_guide', '<>', 2)->get(),
			'komisis'=>Pengguna::where('id_pengguna_kategori', 5)->where('status_pengguna', 1)->get()
		]);
	}

	public function detail(Request $request)
	{
		$query = DB::table('dt_guides')
				   ->join('dt_transaksi_komisi_guides', 'dt_guides.id_guide', '=', 'dt_transaksi_komisi_guides.id_guide')
				   ->leftJoin('dt_travels', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
				   ->whereBetween('dt_transaksi_komisi_guides.wkt_transaksi', [$request->mulai, $request->selesai])
				   ->orderBy('dt_transaksi_komisi_guides.wkt_transaksi', 'desc')
				   ->select('dt_guides.*', 'dt_transaksi_komisi_guides.*', 'dt_travels.*');

		if($request->guide != "") { $query->where('dt_transaksi_komisi_guides.id_guide', $request->guide); }
		if($request->komisi != "") { $query->where('dt_transaksi_komisi_guides.id_pengguna', $request->komisi); }

		return $query->get();
	}

	public function komisi(Request $request, $id)
	{
		$query = DB::table('dt_guides')
				   ->join('dt_transaksi_tikets', 'dt_guides.id_guide', '=', 'dt_transaksi_tikets.id_guide')
				   ->join('dt_kategori_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
				   ->join('dt_transaksi_komisi_guides', 'dt_transaksi_tikets.id_transaksi_komisi', '=', 'dt_transaksi_komisi_guides.id_transaksi_komisi')
				   ->leftJoin('dt_travels', 'dt_travels.id_travel', '=', 'dt_guides.id_travel')
				   ->where('dt_transaksi_tikets.id_transaksi_komisi', '=', $request->group)
				   ->where('dt_transaksi_tikets.status_pembayaran_komisi', '=', 1)
				   ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [$request->mulai, $request->selesai])
				   ->orderBy('dt_transaksi_tikets.wkt_transaksi', 'desc')
				   ->select('dt_guides.*', 'dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_travels.*');

		if($request->guide != "") { $query->where('dt_transaksi_tikets.id_guide', $request->guide); }
		if($request->komisi != "") { $query->where('dt_transaksi_tikets.id_pengguna', $request->komisi); }

		return $query->get();
	}
}
