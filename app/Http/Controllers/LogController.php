<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;
use App\Http\Requests;
use App\Repositories\TicketingRepository;
use DB;
use Excel;

class LogController extends Controller
{
	private $pengguna;
	private $aktivitas;

	public function __construct()
	{
	}

	public function index(Request $request)
	{
		$request->session()->flash('page', 0);
		$logs = DB::table('dt_penggunas')
				  ->join('dt_logs', 'dt_logs.id_pengguna', '=', 'dt_penggunas.id_pengguna')
				  ->select('dt_penggunas.*', 'dt_logs.*')
				  ->orderBy('wkt_log', 'desc')
				  ->paginate(200);

		return view('administrators.log', ['logs'=>$logs]);
	}

	public function store(Request $request)
	{
		$log = new Log;
		$log->id_log = TicketingRepository::id();
		$log->id_pengguna = $this->pengguna;
		$log->aktivitas = $this->aktivitas;
		$log->wkt_log = TicketingRepository::date();
		$log->save();
	}

	public function show($id)
	{
	}

	public function update(Request $request, $id)
	{
	}

	public function destroy($id)
	{
	}

	public function excel(Request $request)
	{
		$query = DB::table('dt_pengguna_kategoris')
				   ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
				   ->join('dt_logs', 'dt_logs.id_pengguna', '=', 'dt_penggunas.id_pengguna')
				   ->where('dt_penggunas.status_pengguna', '<>', 2)
				   ->whereBetween('dt_logs.wkt_log', [TicketingRepository::explodes($request->mulai, $request->waktuMulai), TicketingRepository::explodes($request->selesai, $request->waktuAkhir)])
				   ->orderBy('dt_logs.wkt_log', 'desc')
				   ->select('dt_logs.id_log AS IDLog',
					  		'dt_penggunas.nama_lengkap AS NamaLengkap',
					  		'dt_logs.aktivitas AS Aktivitas',
					  		'dt_logs.wkt_log AS Waktu');

		if($request->pengguna != "") { $query->where('dt_logs.id_pengguna', $request->pengguna); }

		$logs = $query->get();

		Excel::create('Log', function($excel) use($logs)
		{
			$excel->sheet('Data', function($sheet) use($logs)
			{
				$sheet->fromArray(json_decode(json_encode($logs), true));
			});
		})->download('xls');
	}

	public function report(Request $request)
	{
		$request->session()->flash('page', 2);
		$penggunas = DB::table('dt_pengguna_kategoris')
					   ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
					   ->where('dt_penggunas.status_pengguna', '<>', 2)
					   ->select('dt_pengguna_kategoris.*', 'dt_penggunas.*')
					   ->paginate(25);

		return view('administrators.laporanlog', ['penggunas'=>$penggunas]);
	}

	public function detail(Request $request)
	{
		$query = DB::table('dt_pengguna_kategoris')
				   ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
				   ->join('dt_logs', 'dt_logs.id_pengguna', '=', 'dt_penggunas.id_pengguna')
				   ->where('dt_penggunas.status_pengguna', '<>', 2)
				   ->whereBetween('dt_logs.wkt_log', [$request->mulai, $request->selesai])
				   ->orderBy('dt_logs.wkt_log', 'desc')
				   ->select('dt_pengguna_kategoris.*', 'dt_penggunas.*', 'dt_logs.*');

		if($request->pengguna != "") { $query->where('dt_logs.id_pengguna', $request->pengguna); }

		return $query->get();
	}
}
