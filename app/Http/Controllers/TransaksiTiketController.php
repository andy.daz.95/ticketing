<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengguna;
use App\Guide;
use App\KategoriTiket;
use App\TransaksiTiket;
use App\Http\Requests;
use App\Repositories\LogRepository;
use App\Repositories\TicketingRepository;
use App\Repositories\KategoriTiketRepository;
use DB;
use Excel;

class TransaksiTiketController extends Controller
{
	public function __construct()
	{
	}
	
	//FUNCTION TO CHANGE//
	public function index(Request $request)
	{

		$request->session()->flash('page', 1);
		if(session('login') != "000000007")
		{
			$kategoritiket = KategoriTiket::where('status_tiket', 1)->where('id_tiket', '<>', 1)->paginate(100);
		}else{
			$tiket = [
					11, //promo Januari
					23, //promo D
					24, //promo F
					68, //Disc D
					75, //Disc F
					52, //Disc Bazar Nak Bali
					27, //E Commerce ID
					83, //Entry Ticket F
					82, //Entry Ticket D
					78, //Compliment
					];
			$kategoritiket = KategoriTiket::where('status_tiket', 1)->whereIn('id_tiket', $tiket)->paginate(100);
		}
		return view('administrators.transaksitiket',
		[
			'guides'=>Guide::where('status_guide', '<>', 2)->get(),
			'kategoritikets'=>$kategoritiket,			

		]);
	}
	
	/*public function index(Request $request)
	{
		$request->session()->flash('page', 1);
		return view('administrators.transaksitiket',
		[
			'guides'=>Guide::where('status_guide', '<>', 2)->get(),
			'kategoritikets'=>KategoriTiket::where('status_tiket', 1)->where('id_tiket', '<>', 1)->paginate(100)
		]);
	}*/

	public function store(Request $request)
	{	
		function generate_ids($number){
			$ids = [];
			for($i = 0; $i < $number; $i++)
			{	
				$random = rand(0,999);
				$random = str_pad($random,3,0,STR_PAD_LEFT);
				$id = str_shuffle(strtotime("now").$random);
				array_push($ids, $id);
			}
			return $ids;
		}

		$unique = false;
		while ($unique == false){
			$data = json_decode(json_encode($request->all));
			$ids = generate_ids(count($data));
			$unique = TicketingRepository::check_duplicate($ids);
		}

		$result = array();
		$invoice = strtotime(TicketingRepository::date());
		
		$arrayToInsert = [];
		$wkt_transaksi = TicketingRepository::date();
		foreach(json_decode(json_encode($request->all)) as $key => $data)
		{
			// $ip = explode(".", $data[17]);
			// $client = substr($ip[3],-1);
			// $id = TicketingRepository::accurate($client);
			
			$id = $ids[$key];

			$arrayToInsert[$key] = [
				'id_transaksi' => $id,
				'id_tiket' =>  $data[0],
				'id_penjual' => $data[1],
				'harga' => $data[2],
				'diskon' => $data[3],
				'komisi_guide' => $data[4],
				'wkt_transaksi' => $wkt_transaksi,
				'status_transaksi' => $data[5],
				'id_guide' => $data[6],
				'status_scan' => $data[10],
				'status_pembayaran_komisi' => 0,
				'invoice' => $invoice,
				'jenis_pembayaran' => $data[12],
				'no_kartu' => $data[13],
				'expire_mm' => $data[14], 
				'expire_yy' => $data[15], 
				'no_security' => $data[16], 
			];

			array_push($result, $id.';'.$data[11].';'.session('login').';'.KategoriTiketRepository::show($data[0]).';'.$invoice);
		}
		DB::table('dt_transaksi_tikets')->insert($arrayToInsert);

		if($data[0] != 1)
		{
			$tanggal = TicketingRepository::date();
			$var_magin_left = 20;
		}

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menambah Transaksi Tiket.';
		$log->store();

		return $result;
	}

	public function show($id)
	{
	}

	public function free($id)
	{
		return view('administrators.free', 
		[
			'guide'=>Guide::where('status_guide', '<>', 2)->where('id_guide', $id)->first(),
			'guides'=>Guide::where('status_guide', '<>', 2)->paginate(25),
			'kategoritikets'=>KategoriTiket::where('status_tiket', 1)->where('id_tiket', '<>', 1)->paginate(25)
		]);
	}

	public function search(Request $request)
	{
	}

	public function update(Request $request, $id)
	{
		TransaksiTiket::where('id_transaksi', $id)->update
		([
			'id_transaksi_komisi'=>$request->cookie('komisi'),
			'status_pembayaran_komisi'=>$request->pembayaran
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Mengubah Transaksi Tiket.';
		$log->store();
	}

	public function destroy($id)
	{
	}

	public function excel(Request $request)
	{
		DB::statement(DB::raw('set @row = 0'));
		$query = DB::table(DB::raw('dt_kategori_tikets'))
				   ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
				   ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
				   ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [TicketingRepository::explodes($request->mulai, $request->waktuMulai), TicketingRepository::explodes($request->selesai, $request->waktuAkhir)])
				   ->orderBy('dt_transaksi_tikets.wkt_transaksi', 'desc')
				   ->select(DB::raw("@row := @row + 1 AS No,
				   					 dt_transaksi_tikets.wkt_transaksi AS WaktuTransaksi,
				   					 dt_transaksi_tikets.id_transaksi AS IDTransaksi,
				 	 	    		 dt_kategori_tikets.kategori_tiket AS KategoriTiket,
				 		    		 dt_transaksi_tikets.harga AS Harga,
				 		    		 dt_transaksi_tikets.diskon AS Diskon,
				 		    		 dt_penggunas.id_pengguna AS IDPetugasTiket,
				 		    		 dt_penggunas.nama_lengkap AS NamaPetugasTiket,
									 CASE (dt_transaksi_tikets.jenis_pembayaran) WHEN 0 THEN 'Cash' WHEN 1 THEN 'EDC' END AS JenisBayar"));

		if($request->tiket != "") { $query->where('dt_transaksi_tikets.id_tiket', $request->tiket); }
		if($request->petugas != "") { $query->where('dt_transaksi_tikets.id_penjual', $request->petugas); }

		$transaksitikets = $query->get();

		Excel::create('TransaksiTiket', function($excel) use($transaksitikets)
		{
			$excel->sheet('Data', function($sheet) use($transaksitikets)
			{
				$sheet->fromArray(json_decode(json_encode($transaksitikets), true));
			});
		})->download('xls');
	}
	
	public function report(Request $request)
	{
		$request->session()->flash('page', 2);
		$tiket = [
					11, //promo Januari
					23, //promo D
					24, //promo F
					68, //Disc D
					75, //Disc F
					52, //Disc Bazar Nak Bali
					27, //E Commerce ID
					83, //Entry Ticket F
					82, //Entry Ticket D
					78, //Compliment
					];
					
		if(session('login') != "000000007")
		{
			$tiket =KategoriTiket::where('status_tiket', 1)->get();
			$pengguna = Pengguna::where('id_pengguna_kategori', 3)->where('status_pengguna', 1)->get();
		}else{
			$tiket = KategoriTiket::where('status_tiket', 1)->whereIn('id_tiket', $tiket)->get();
			$pengguna = Pengguna::where('id_pengguna_kategori', 3)->where('id_pengguna',"000000007")->where('status_pengguna', 1)->get();
		}

		return view('administrators.laporantransaksitiket',
		[
				'kategoritikets'=>$tiket,
				'penggunas'=>$pengguna,
		]);
	}
	
	/*public function report(Request $request)
	{
		$request->session()->flash('page', 2);
		return view('administrators.laporantransaksitiket',
		[
			'kategoritikets'=>KategoriTiket::where('status_tiket', 1)->get(),
			'penggunas'=>Pengguna::where('id_pengguna_kategori', 3)->where('status_pengguna', 1)->get()
		]);
	}*/

	public function lastinvoice(Request $request)
	{
		return DB::table('dt_transaksi_tikets')
				 ->limit(1)
				 ->select('invoice')
				 ->where('id_penjual', session('login'))
				 ->orderBy('wkt_transaksi', 'desc')
				 ->get();
	}

	public function datalastinvoice(Request $request)
	{
		return DB::table('dt_transaksi_tikets')
				 ->join('dt_kategori_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
				 ->select('dt_transaksi_tikets.id_transaksi', 'dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.harga', 'dt_transaksi_tikets.diskon', 'dt_transaksi_tikets.invoice')
				 ->where('invoice', $request->referensi)
				 ->get();
	}
	
	public function void(Request $request)
	{
		$request->session()->flash('page', 1);
		if(session('login') != "000000007")
		{
			$kategoritiket = KategoriTiket::where('status_tiket', 1)->where('id_tiket', '<>', 1)->paginate(100);
		}else{
			$tiket = [
					11, //promo Januari
					23, //promo D
					24, //promo F
					68, //Disc D
					75, //Disc F
					52, //Disc Bazar Nak Bali
					27, //E Commerce ID
					83, //Entry Ticket F
					82, //Entry Ticket D
					78, //Compliment
					];
			$kategoritiket = KategoriTiket::where('status_tiket', 1)->whereIn('id_tiket', $tiket)->paginate(100);
		}
		return view('administrators.transaksitiketvoid', ['kategoritikets'=>$kategoritiket]);
	}
	
	/*public function void(Request $request)
	{
		$request->session()->flash('page', 1);
		return view('administrators.transaksitiketvoid', ['kategoritikets'=>KategoriTiket::where('status_tiket', 1)->get()]);
	}*/
	
	public function detail(Request $request)
	{
		$tiket = [
					11, //promo Januari
					23, //promo D
					24, //promo F
					68, //Disc D
					75, //Disc F
					52, //Disc Bazar Nak Bali
					27, //E Commerce ID
					83, //Entry Ticket F
					82, //Entry Ticket D
					78, //Compliment
					];
					
		$query = DB::table('dt_kategori_tikets')
				   ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
				   ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
				   ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [$request->mulai, $request->selesai])
				   ->orderBy('dt_transaksi_tikets.wkt_transaksi', 'desc')
				   ->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap');

		if(session('login') == "000000007") {$query = $query->whereIn('dt_transaksi_tikets.id_tiket',$tiket); }
		if($request->tiket != "") { $query->where('dt_transaksi_tikets.id_tiket', $request->tiket); }
		if($request->petugas != "") { $query->where('dt_transaksi_tikets.id_penjual', $request->petugas); }

		return $query->get();
	}
	/*public function detail(Request $request)
	{
		$query = DB::table('dt_kategori_tikets')
				   ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
				   ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
				   ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [$request->mulai, $request->selesai])
				   ->orderBy('dt_transaksi_tikets.wkt_transaksi', 'desc')
				   ->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap');

		if($request->tiket != "") { $query->where('dt_transaksi_tikets.id_tiket', $request->tiket); }
		if($request->petugas != "") { $query->where('dt_transaksi_tikets.id_penjual', $request->petugas); }

		return $query->get();
	}*/
	
	public function tiketvoid(Request $request)
	{	
		$id = session('login');
					
		$limit = [
					11, //promo Januari
					23, //promo D
					24, //promo F
					68, //Disc D
					75, //Disc F
					52, //Disc Bazar Nak Bali
					27, //E Commerce ID
					83, //Entry Ticket F
					82, //Entry Ticket D
					78, //Compliment
					];

		if($request->tiket == "")
		{
			if($request->cari!="")
			{
				$tiket = DB::table('dt_kategori_tikets')
						 ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
						 ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
						 ->where('dt_transaksi_tikets.status_scan', '0')
						 ->where('dt_transaksi_tikets.invoice', 'like', $request->cari.'%')
						 ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [date('Y-m-d')." ".$request->mulai, date('Y-m-d')." ".$request->selesai]);
						 if($id == "000000007")
						 {
						 	$tiket = $tiket->whereIn('dt_transaksi_tikets.id_tiket', $limit);
						 }
						 $tiket = $tiket->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap')
						 ->get();

						 return $tiket;
		 	}

			else
			{
				$tiket =  DB::table('dt_kategori_tikets')
						 ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
						 ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
						 ->where('dt_transaksi_tikets.status_scan', '0')
						 ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [date('Y-m-d')." ".$request->mulai, date('Y-m-d')." ".$request->selesai]);
						 if($id == "000000007")
						 {
						 	$tiket = $tiket->whereIn('dt_transaksi_tikets.id_tiket', $limit);
						 }
						 $tiket = $tiket->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap')
						 ->get();

						 return $tiket;
			}
		}

		else
		{
			if($request->cari!="")
			{
				$tiket = DB::table('dt_kategori_tikets')
						 ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
						 ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
						 ->where('dt_transaksi_tikets.id_tiket', $request->tiket)
						 ->where('dt_transaksi_tikets.invoice', 'like', $request->cari.'%')
						 ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [date('Y-m-d')." ".$request->mulai, date('Y-m-d')." ".$request->selesai]);
						 if($id == "000000007")
						 {
						 	$tiket = $tiket->whereIn('dt_transaksi_tikets.id_tiket', $limit);
						 }
						 $tiket = $tiket->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap')
						 ->get();

						 return $tiket;
			}

			else
			{
				$tiket = DB::table('dt_kategori_tikets')
						 ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
						 ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
						 ->where('dt_transaksi_tikets.id_tiket', $request->tiket)
						 ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [date('Y-m-d')." ".$request->mulai, date('Y-m-d')." ".$request->selesai]);
						 if($id == "000000007")
						 {
						 	$tiket = $tiket->whereIn('dt_transaksi_tikets.id_tiket', $limit);
						 }
						 $tiket = $tiket->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap')
						 ->get();

						return $tiket;
			}
		}
	}
	/*public function tiketvoid(Request $request)
	{
		if($request->tiket == "")
		{
			if($request->cari!="")
			{
				return DB::table('dt_kategori_tikets')
						 ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
						 ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
						 ->where('dt_transaksi_tikets.status_scan', '0')
						 ->where('dt_transaksi_tikets.invoice', 'like', $request->cari.'%')
						 ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [date('Y-m-d')." ".$request->mulai, date('Y-m-d')." ".$request->selesai])
						 ->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap')
						 ->get();
		 	}

			else
			{
				return DB::table('dt_kategori_tikets')
						 ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
						 ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
						 ->where('dt_transaksi_tikets.status_scan', '0')
						 ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [date('Y-m-d')." ".$request->mulai, date('Y-m-d')." ".$request->selesai])
						 ->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap')
						 ->get();
			}
		}

		else
		{
			if($request->cari!="")
			{
				return DB::table('dt_kategori_tikets')
						 ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
						 ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
						 ->where('dt_transaksi_tikets.id_tiket', $request->tiket)
						 ->where('dt_transaksi_tikets.invoice', 'like', $request->cari.'%')
						 ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [date('Y-m-d')." ".$request->mulai, date('Y-m-d')." ".$request->selesai])
						 ->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap')
						 ->get();
			}

			else
			{
				return DB::table('dt_kategori_tikets')
						 ->join('dt_transaksi_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
						 ->join('dt_penggunas', 'dt_penggunas.id_pengguna', '=', 'dt_transaksi_tikets.id_penjual')
						 ->where('dt_transaksi_tikets.id_tiket', $request->tiket)
						 ->whereBetween('dt_transaksi_tikets.wkt_transaksi', [date('Y-m-d')." ".$request->mulai, date('Y-m-d')." ".$request->selesai])
						 ->select('dt_kategori_tikets.kategori_tiket', 'dt_transaksi_tikets.*', 'dt_penggunas.nama_lengkap')
						 ->get();
			}
		}
	}*/

	public function tiketvoidproses(Request $request)
	{
		$tmpData = rtrim($request->data, '-');
		$getData = explode('-', $tmpData);

		for($i=0; $i < count($getData); $i++) 
		{
			$data = DB::table('dt_transaksi_tikets')
					  ->where('id_transaksi', $getData[$i])
					  ->get();

			foreach($data as $key) 
			{
				DB::table('dt_transaksi_tiket_voids')->insert(
					[
						'id_transaksi'=>$key->id_transaksi,
						'id_tiket'=>$key->id_tiket,
						'id_penjual'=>$key->id_penjual,
						'harga'=>$key->harga,
						'diskon'=>$key->diskon,
						'komisi_guide'=>$key->komisi_guide,
						'wkt_transaksi'=>$key->wkt_transaksi,
						'status_transaksi'=>$key->status_transaksi,
						'id_scanner'=>$key->id_scanner,
						'status_scan'=>$key->status_scan,
						'wkt_scan'=>$key->wkt_scan,
						'id_guide'=>$key->id_guide,
						'status_pembayaran_komisi'=>$key->status_pembayaran_komisi,
						'invoice'=>$key->invoice,
						'jenis_pembayaran'=>$key->jenis_pembayaran,
						'no_kartu'=>$key->no_kartu,
						'expire_mm'=>$key->expire_mm,
						'expire_yy'=>$key->expire_yy,
						'no_security'=>$key->no_security
					]
				);
			}

			DB::table('dt_transaksi_tikets')->where('id_transaksi', $getData[$i])->delete();
		}
	}

	public function presentation(Request $request)
	{
		$request->session()->flash('page', 2);
		return view('administrators.laporantransaksitiketgrafik', ['kategoritikets'=>KategoriTiket::where('status_tiket', 1)->get()]);
	}

	public function grafik(Request $request)
	{
		$jenisTiket = $request->tiket;
		$tahun = $request->tahun;
		$months = array(1=>"Januari",
						2=>"Pebruari",
						3=>"Maret",
						4=>"April",
						5=>"Mei",
						6=>"Juni",
						7=>"Juli",
						8=>"Agustus",
						9=>"September",
						10=>"Oktober",
						11=>"Nopember",
						12=>"Desember");

		$result['result'] = array();

		if($jenisTiket == "0")
		{
			$data = DB::table('dt_kategori_tikets')
					  ->select('id_tiket', 'kategori_tiket')
					  ->where('dt_kategori_tikets.status_tiket', 1)
					  ->whereIn('id_tiket', json_decode($jenisTiket))
					  ->get();

			foreach($data as $key)
			{
				$r = array();

				$r['jenis_tiket'] = $key->kategori_tiket;
				$r['detail'] = array();

				for($i = 0; $i < count($months); $i++) 
				{
					$rm = array();

					$rm['month'] = $i + 1;

					$dataTransaksi = DB::table('dt_transaksi_tikets')
									   ->leftJoin('dt_kategori_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
									   ->where(DB::raw('month(dt_transaksi_tikets.wkt_transaksi)'), ($i + 1))
									   ->where(DB::raw('year(dt_transaksi_tikets.wkt_transaksi)'), $tahun)
									   ->where(DB::raw('dt_transaksi_tikets.id_tiket'), $key->id_tiket)
									   ->where(DB::raw('dt_kategori_tikets.status_tiket'), 1)
									   ->select(DB::raw('count(dt_transaksi_tikets.id_tiket) as total'))->first();

					$rm['jumlah'] = $dataTransaksi->total;

					array_push($r['detail'], $rm);
				}

				array_push($result['result'], $r);
			}
		}

		else
		{
			$data = DB::table('dt_kategori_tikets')
					  ->select('id_tiket', 'kategori_tiket')
					  ->where('dt_kategori_tikets.status_tiket', 1)
					  ->whereIn('id_tiket', json_decode($jenisTiket))
					  ->get();

			foreach($data as $key)
			{
				$r = array();

				$r['jenis_tiket']=$key->kategori_tiket;
				$r['detail']=array();

				for($i = 0; $i < count($months); $i++) 
				{
					$rm = array();

					$rm['month'] = $i + 1;

					$dataTransaksi = DB::table('dt_transaksi_tikets')
									   ->leftJoin('dt_kategori_tikets', 'dt_transaksi_tikets.id_tiket', '=', 'dt_kategori_tikets.id_tiket')
									   ->where(DB::raw('month(dt_transaksi_tikets.wkt_transaksi)'), ($i + 1))
									   ->where(DB::raw('year(dt_transaksi_tikets.wkt_transaksi)'), $tahun)
									   ->where(DB::raw('dt_transaksi_tikets.id_tiket'), $key->id_tiket)
									   ->where(DB::raw('dt_kategori_tikets.status_tiket'), 1)
									   ->select(DB::raw('count(dt_transaksi_tikets.id_tiket) as total'))->first();

					$rm['jumlah'] = $dataTransaksi->total;

					array_push($r['detail'], $rm);
				}

				array_push($result['result'], $r);
			}
		}

		return json_encode($result);
	}
}
