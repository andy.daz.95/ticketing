<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PenggunaKategori;
use App\Pengguna;
use App\Http\Requests;
use App\Repositories\LogRepository;
use App\Repositories\TicketingRepository;
use DB;
use Hash;

class PenggunaController extends Controller
{
	public function __construct()
	{
	}

	public function index(Request $request)
	{
		$request->session()->flash('page', 0);
		$penggunas = DB::table('dt_pengguna_kategoris')
					   ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
					   ->where('dt_penggunas.status_pengguna', '<>', 2)
					   ->select('dt_pengguna_kategoris.*', 'dt_penggunas.*')
					   ->paginate(25);

		return view('administrators.pengguna',
		[
			'penggunas'=>$penggunas,
			'penggunakategoris'=>PenggunaKategori::paginate(25)
		]);
	}

	public function store(Request $request)
	{
		$check = Pengguna::where('nama_pengguna', $request->username)->where('status_pengguna', '<>', '2')
						 ->exists();
		if($check)
		{
			return '0';
		}

		else
		{
			$pengguna = new Pengguna;
			$pengguna->id_pengguna = TicketingRepository::id();
			$pengguna->nama_lengkap = $request->nama;
			$pengguna->nama_pengguna = $request->username;
			$pengguna->pass_enkrip = TicketingRepository::hash($request->password);
			$pengguna->wkt_daftar = TicketingRepository::date();
			$pengguna->status_pengguna = $request->status;
			$pengguna->id_pengguna_kategori = $request->kategori;
			$pengguna->save();

			$log = new LogRepository;
			$log->pengguna = session('login');
			$log->aktivitas = 'Menambah Data Pengguna.';
			$log->store();
		}
	}

	public function show($id)
	{
		$pengguna = DB::table('dt_pengguna_kategoris')
					  ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
					  ->where('dt_penggunas.id_pengguna', $id)
					  ->select('dt_pengguna_kategoris.*', 'dt_penggunas.*')
					  ->get();

		return $pengguna;
	}

	public function find($id)
	{
		$penggunas = DB::table('dt_pengguna_kategoris')
					   ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
					   ->where('dt_penggunas.status_pengguna', '<>', 2)
					   ->where('dt_penggunas.nama_lengkap', 'like', "$id%")
					   ->select('dt_pengguna_kategoris.*', 'dt_penggunas.*')
					   ->paginate(25);

		return view('administrators.pengguna',
		[
			'penggunas'=>$penggunas,
			'penggunakategoris'=>PenggunaKategori::paginate(25)
		]);
	}

	public function search($id)
	{
		if($id == 'all')
		{
			return DB::table('dt_pengguna_kategoris')
					 ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
					 ->where('dt_penggunas.status_pengguna', '<>', 2)
					 ->select('dt_pengguna_kategoris.*', 'dt_penggunas.*')
					 ->get();
		}

		else
		{
			return DB::table('dt_pengguna_kategoris')
					 ->join('dt_penggunas', 'dt_pengguna_kategoris.id_pengguna_kategori', '=', 'dt_penggunas.id_pengguna_kategori')
					 ->where('dt_penggunas.status_pengguna', '<>', 2)
					 ->where('dt_penggunas.nama_lengkap', 'like', "%$id%")
					 ->select('dt_pengguna_kategoris.*', 'dt_penggunas.*')
					 ->get();
		}
	}

	public function update(Request $request, $id)
	{
		$check = Pengguna::where('nama_pengguna', $request->username)
						->where('status_pengguna', '<>', '2')
						->where('id_pengguna', '<>', $id)
						 ->exists();
		if($check)
		{
			return '0';
		}

		else
		{
			Pengguna::where('id_pengguna', $id)->update
			([
				'nama_lengkap'=>$request->nama,
				'nama_pengguna'=>$request->username,
				'pass_enkrip'=>$request->password != "" ? TicketingRepository::hash($request->password) : Pengguna::where('id_pengguna', $id)->pluck('pass_enkrip')[0],
				'status_pengguna'=>$request->status,
				'id_pengguna_kategori'=>$request->kategori
			]);
	
			$log = new LogRepository;
			$log->pengguna = session('login');
			$log->aktivitas = 'Mengubah Data Pengguna.';
			$log->store();
		}
	}

	public function reset(Request $request, $id)
	{
		Pengguna::where('id_pengguna', $id)->update
		([
			'pass_enkrip'=>TicketingRepository::hash('123456')
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Mereset Password.';
		$log->store();
	}

	public function profile(Request $request, $id)
	{
		Pengguna::where('id_pengguna', $id)->update
		([
			'nama_lengkap'=>$request->nama,
			'pass_enkrip'=>$request->password != "" ? TicketingRepository::hash($request->password) : Pengguna::where('id_pengguna', $id)->pluck('pass_enkrip')[0]
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Mengubah Profile.';
		$log->store();
	}

	public function destroy($id)
	{
		Pengguna::where('id_pengguna', $id)->update
		([
			'status_pengguna'=>2
		]);

		$log = new LogRepository;
		$log->pengguna = session('login');
		$log->aktivitas = 'Menghapus Data Pengguna.';
		$log->store();
	}
}
