<?php

namespace App\Http\Middleware;

use Closure;

class TicketMiddleware
{
    public function handle($request, Closure $next)
    {
        if(in_array($request->session()->get('role'), [1, 3]))
        {
            return $next($request);
        }

        return redirect('/');
    }
}
