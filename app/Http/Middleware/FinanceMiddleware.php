<?php

namespace App\Http\Middleware;

use Closure;

class FinanceMiddleware
{
    public function handle($request, Closure $next)
    {
        if(in_array($request->session()->get('role'), [1, 6]))
        {
            return $next($request);
        }

        return redirect('/');
    }
}
