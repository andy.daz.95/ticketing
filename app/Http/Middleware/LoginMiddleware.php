<?php

namespace App\Http\Middleware;

use Closure;

class LoginMiddleware
{
    public function handle($request, Closure $next)
    {
        if($request->session()->has('login'))
        {
            return $next($request);
        }

        return redirect('/');
    }
}
