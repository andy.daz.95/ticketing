<?php

namespace App\Http\Middleware;

use Closure;

class GateMiddleware
{
    public function handle($request, Closure $next)
    {
        if(in_array($request->session()->get('role'), [1, 4]))
        {
            return $next($request);
        }

        return redirect('/');
    }
}
