<?php

namespace App\Http\Middleware;

use Closure;

class AdministratorMiddleware
{
    public function handle($request, Closure $next)
    {
        if(in_array($request->session()->get('role'), [1]))
        {
            return $next($request);
        }

        return redirect('/');
    }
}
