<?php

namespace App\Http\Middleware;

use Closure;

class KomisiMiddleware
{
    public function handle($request, Closure $next)
    {
        if(in_array($request->session()->get('role'), [1, 5]))
        {
            return $next($request);
        }

        return redirect('/');
    }
}
