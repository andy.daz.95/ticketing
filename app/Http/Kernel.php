<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'administrator' => \App\Http\Middleware\AdministratorMiddleware::class,
        'manager' => \App\Http\Middleware\ManagerMiddleware::class,
        'ticket' => \App\Http\Middleware\TicketMiddleware::class,
        'gate' => \App\Http\Middleware\GateMiddleware::class,
        'komisi' => \App\Http\Middleware\KomisiMiddleware::class,
        'finance' => \App\Http\Middleware\FinanceMiddleware::class,
        'login' => \App\Http\Middleware\LoginMiddleware::class,
    ];
}
