<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenggunaKategori extends Model
{
    protected $table = 'dt_pengguna_kategoris';
    public $timestamps = false;
}
