<?php

namespace App\Repositories;

use App\KategoriTiket;

class KategoriTiketRepository
{
	public static function show($id)
	{
		return KategoriTiket::where('id_tiket', $id)->select('kategori_tiket', 'harga', 'diskon', 'Barang')->first();
	}
}
