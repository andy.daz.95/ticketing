<?php

namespace App\Repositories;

use App\TransaksiTiket;

class TicketingRepository
{
	public static function id()
	{
		sleep(1);
		
		$random = substr(rand(000000000, 999999999), 0, 2);
		$micro = round(microtime(true));
		$id = $random.$micro;
		
		if(strlen($id) < 12)
		{
			$id = $id.substr($random, - (12 - strlen($id)));
		}
		
		return $id;
	}

	public static function unique()
	{
		$id = uniqid();

		return $id;
	}

	public static function check_duplicate($ids){
		return TransaksiTiket::whereIn('id_tiket', $ids)->count() > 0;
	} 

	public static function accurate($client)
	{
		$id = $client.TicketingRepository::id();
		$check = TransaksiTiket::where('id_tiket', $id)->exists();

		if($check)
		{
			accurate();
		}

		else
		{
			return $id;
		}
	}

	public static function date()
	{
		return date('Y-m-d H:i:s', strtotime('+ 8 hours'));
	}

	public static function hash($text)
	{
		return sha1($text);
	}
	
	public static function explodes($date, $time)
	{
		$partDate = explode("-", $date);
		$partTime = explode(":", $time);
		
		return $partDate[2]."-".$partDate[1]."-".$partDate[0]." ".$partTime[0].":".$partTime[1].":00";
	}

	public static function rp($data)
	{
		
		$panjangData=strlen(ceil($data));
		$hasilBagi=substr($panjangData/3,0,1);
		$sisaBagi=$panjangData%3;
		$h="";
		$aw2=0;
		$ambil=3;
		
		if($sisaBagi!=0)
		{
			$h.=substr($data,0,$sisaBagi);
			$pjgDataBaru=strlen($data)-$sisaBagi;
			
			for($i=1;$i<=$pjgDataBaru/3;$i++)
			{
				$h.=".";
				$h.=substr($data,$sisaBagi,$ambil);
				$sisaBagi+=3;
			}
			
		}else
		{
			for($a=1;$a<=$hasilBagi;$a++)
			{
				$h=$h.substr($data,$aw2,$ambil);
				if($a<$hasilBagi)
				{
					$h=$h.".";
				}
				$aw2+=3;
			}
		}
		return $h;
	}
}
