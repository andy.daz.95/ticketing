<?php

namespace App\Repositories;

use App\Log;
use App\Repositories\TicketingRepository;

class LogRepository
{
	public $pengguna;
	public $aktivitas;

	public function store()
	{
		$log = new Log;
		$log->id_pengguna = $this->pengguna;
		$log->aktivitas = $this->aktivitas;
		$log->wkt_log = TicketingRepository::date();
		$log->save();
	}
}
