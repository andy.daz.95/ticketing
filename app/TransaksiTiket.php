<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiTiket extends Model
{
    protected $table = 'dt_transaksi_tikets';
    public $timestamps = false;
    protected $fillable = [
    	'id_transaksi',
		'id_tiket',
		'id_penjual',
		'harga',
		'diskon',
		'komisi_guide',
		'wkt_transaksi',
		'status_transaksi',
		'id_guide',
		'status_scan',
		'status_pembayaran_komisi',
		'invoice',
		'jenis_pembayaran',
		'no_kartu',
		'expire_mm', 
		'expire_yy', 
		'no_security'
    ];
}
