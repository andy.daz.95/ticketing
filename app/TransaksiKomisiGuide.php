<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiKomisiGuide extends Model
{
    protected $table = 'dt_transaksi_komisi_guides';
    public $timestamps = false;
}
