<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriTiket extends Model
{
    protected $table = 'dt_kategori_tikets';
    public $timestamps = false;
}
