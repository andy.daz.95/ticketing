<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PenggunaKategorisTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('dt_pengguna_kategoris')->insert([
            ['pengguna_kategori' => 'Administrator'],
            ['pengguna_kategori' => 'Manager'],
            ['pengguna_kategori' => 'Ticket'],
            ['pengguna_kategori' => 'Gate'],
            ['pengguna_kategori' => 'Komisi'],
            ['pengguna_kategori' => 'Finance']
        ]);
    }
}
