<?php

use App\Repositories\TicketingRepository;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PenggunasTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('dt_penggunas')->insert([
            [
             'id_pengguna' => '000000001',
             'nama_lengkap' => 'Testing Administrator',
             'nama_pengguna' => 'TestingAdministrator',
             'pass_enkrip' => TicketingRepository::hash('123456'),
             'status_pengguna' => 1,
             'id_pengguna_kategori' => 1
            ],
            [
             'id_pengguna' => '000000002',
             'nama_lengkap' => 'Testing Manager',
             'nama_pengguna' => 'TestingManager',
             'pass_enkrip' => TicketingRepository::hash('123456'),
             'status_pengguna' => 1,
             'id_pengguna_kategori' => 2
            ],
            [
             'id_pengguna' => '000000003',
             'nama_lengkap' => 'Testing Tiket',
             'nama_pengguna' => 'TestingTiket',
             'pass_enkrip' => TicketingRepository::hash('123456'),
             'status_pengguna' => 1,
             'id_pengguna_kategori' => 3
            ],
            [
             'id_pengguna' => '000000004',
             'nama_lengkap' => 'Testing Gate',
             'nama_pengguna' => 'TestingGate',
             'pass_enkrip' => TicketingRepository::hash('123456'),
             'status_pengguna' => 1,
             'id_pengguna_kategori' => 4
            ],
            [
             'id_pengguna' => '000000005',
             'nama_lengkap' => 'Testing Komisi',
             'nama_pengguna' => 'TestingKomisi',
             'pass_enkrip' => TicketingRepository::hash('123456'),
             'status_pengguna' => 1,
             'id_pengguna_kategori' => 5
            ],
            [
             'id_pengguna' => '000000006',
             'nama_lengkap' => 'Testing Finance',
             'nama_pengguna' => 'TestingFinance',
             'pass_enkrip' => TicketingRepository::hash('123456'),
             'status_pengguna' => 1,
             'id_pengguna_kategori' => 6
            ]
        ]);
    }
}
