<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class GuidesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('dt_guides')->insert([
            ['id_guide' => '000000000', 'nama' => 'Testing Guide', 'status_guide' => 1]
        ]);
    }
}
