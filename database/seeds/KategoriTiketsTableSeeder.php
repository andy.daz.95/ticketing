<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class KategoriTiketsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('dt_kategori_tikets')->insert([
            ['kategori_tiket' => 'Free', 'harga' => 0, 'diskon' => 0, 'komisi_guide' => 0, 'status_tiket' => 1],
            ['kategori_tiket' => 'Anak', 'harga' => 5000, 'diskon' => 50, 'komisi_guide' => 10, 'status_tiket' => 1],
            ['kategori_tiket' => 'Remaja', 'harga' => 5000, 'diskon' => 50, 'komisi_guide' => 10, 'status_tiket' => 1],
            ['kategori_tiket' => 'Dewasa', 'harga' => 5000, 'diskon' => 50, 'komisi_guide' => 10, 'status_tiket' => 1],
            ['kategori_tiket' => 'Tua', 'harga' => 5000, 'diskon' => 50, 'komisi_guide' => 10, 'status_tiket' => 1]
        ]);
    }
}
