<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(PenggunaKategorisTableSeeder::class);
        $this->call(KategoriTiketsTableSeeder::class);
        $this->call(PenggunasTableSeeder::class);
        $this->call(GuidesTableSeeder::class);
    }
}
