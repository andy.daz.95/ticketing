<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtTravelsTable extends Migration
{
    public function up()
    {
        Schema::create('dt_travels', function (Blueprint $table) {
            $table->integer('id_travel');
            $table->string('nama_travel', 100);
            $table->string('alamat_travel', 100);
            $table->string('no_rek', 30);
            $table->string('bank', 20);
            $table->string('atas_nama', 100);
            $table->float('travel_komisi');
            $table->float('guide_komisi');
            $table->char('status_travel', 1); /* 0: Tidak Aktif 'Hide'; 1: Aktif 'Show' */

            $table->primary('id_travel');
        });
    }

    public function down()
    {
        Schema::drop('dt_travels');
    }
}
