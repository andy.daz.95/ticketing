<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtPenggunasTable extends Migration
{
    public function up()
    {
        Schema::create('dt_penggunas', function (Blueprint $table) {
            $table->char('id_pengguna', 12);
            $table->string('nama_lengkap', 100);
            $table->string('nama_pengguna', 100);
            $table->char('pass_enkrip', 40);
            $table->dateTime('wkt_daftar');
            $table->char('status_pengguna', 1); /* 0: Tidak Aktif 'Show'; 1: Aktif 'Show'; 2: Hapus 'Hide' */
            $table->integer('id_pengguna_kategori');

            $table->primary('id_pengguna');
        });
    }

    public function down()
    {
        Schema::drop('dt_penggunas');
    }
}
