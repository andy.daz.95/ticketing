<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtLogsTable extends Migration
{
    public function up()
    {
        Schema::create('dt_logs', function (Blueprint $table) {
            $table->increments('id_log');
            $table->char('id_pengguna', 12);
            $table->string('aktivitas', 200);
            $table->dateTime('wkt_log');
        });
    }

    public function down()
    {
        Schema::drop('dt_logs');
    }
}
