<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtGuidesTable extends Migration
{
    public function up()
    {
        Schema::create('dt_guides', function (Blueprint $table) {
            $table->char('id_guide', 12);
            $table->char('id_referensi', 15);
            $table->char('no_identitas', 20);
            $table->string('nama', 100);
            $table->char('jenis_kelamin', 1);
            $table->string('tempat_lahir', 20);
            $table->date('tgl_lahir');
            $table->string('alamat', 100);
            $table->string('no_hp', 15);
            $table->string('email', 50);
            $table->char('jenis_guide', 1); /* 0: Freelancer; 1: Travel */
            $table->integer('id_travel');
            $table->char('jenis_transaksi', 1); /* 0: Cash; 1: Bank Transfer */
            $table->string('no_rek', 30);
            $table->string('bank', 20);
            $table->char('ktp', 1);
            $table->char('sim_a', 1);
            $table->char('lisensi', 1);
            $table->char('passport', 1);
            $table->char('sim_b', 1);
            $table->char('sim_c', 1);
            $table->string('nama_ibu_kandung', 100);
            $table->char('status_guide', 1); /* 0: Tidak Aktif 'Show'; 1: Aktif 'Show'; 2: Hapus 'Hide' */
            $table->string('foto', 200000);
            $table->text('sidik_jari');
            $table->text('scan_identitas');

            $table->primary('id_guide');
        });
    }

    public function down()
    {
        Schema::drop('dt_guides');
    }
}
