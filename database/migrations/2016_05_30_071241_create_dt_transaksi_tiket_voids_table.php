<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtTransaksiTiketVoidsTable extends Migration
{
    public function up()
    {
        Schema::create('dt_transaksi_tiket_voids', function (Blueprint $table) {
            $table->char('id_transaksi', 13);
            $table->integer('id_tiket');
            $table->char('id_penjual', 12);
            $table->integer('harga');
            $table->float('diskon');
            $table->float('komisi_guide');
            $table->float('travel_komisi_dari_travel');
            $table->float('guide_komisi_dari_travel');
            $table->dateTime('wkt_transaksi');
            $table->char('status_transaksi', 1); /* 0: Free; 1: Bayar */
            $table->char('id_scanner', 12);
            $table->char('status_scan', 1);
            $table->dateTime('wkt_scan');
            $table->char('id_guide', 12);
            $table->char('status_pembayaran_komisi', 1); /* 0: Belum Bayar; 1: Sudah Bayar */
            $table->char('invoice', 10);
            $table->char('jenis_pembayaran', 1); /* 0: Cash; 1: Debet */
            $table->string('no_kartu', 30);
            $table->char('expire_mm', 2);
            $table->char('expire_yy', 2);
            $table->string('no_security', 15);

            $table->primary('id_transaksi');
        });
    }

    public function down()
    {
        Schema::drop('dt_transaksi_tiket_voids');
    }
}
