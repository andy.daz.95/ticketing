<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtKategoriTiketsTable extends Migration
{
    public function up()
    {
        Schema::create('dt_kategori_tikets', function (Blueprint $table) {
            $table->increments('id_tiket');
            $table->string('kategori_tiket', 100);
            $table->integer('harga');
            $table->float('diskon');
            $table->float('komisi_guide');
            $table->char('status_tiket', 1); /* 0: Tidak Aktif 'Hide'; 1: Aktif 'Show' */
        });
    }

    public function down()
    {
        Schema::drop('dt_kategori_tikets');
    }
}
