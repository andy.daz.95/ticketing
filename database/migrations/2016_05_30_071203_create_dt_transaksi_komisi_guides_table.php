<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtTransaksiKomisiGuidesTable extends Migration
{
    public function up()
    {
        Schema::create('dt_transaksi_komisi_guides', function (Blueprint $table) {
            $table->char('id_transaksi_komisi', 12);
            $table->char('id_guide', 12);
            $table->char('id_pengguna', 12);
            $table->bigInteger('nilai_transaksi');
            $table->dateTime('wkt_transaksi');
            $table->char('status_transaksi', 1); /* 0: Cash; 1: Bank Transfer */

            $table->primary('id_transaksi_komisi');
        });
    }

    public function down()
    {
        Schema::drop('dt_transaksi_komisi_guides');
    }
}
