<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtPenggunaKategorisTable extends Migration
{
    public function up()
    {
        Schema::create('dt_pengguna_kategoris', function (Blueprint $table) {
            $table->increments('id_pengguna_kategori');
            $table->string('pengguna_kategori', 15);
        });
    }

    public function down()
    {
        Schema::drop('dt_pengguna_kategoris');
    }
}
