<?php
ini_set('max_execution_time', 600);
header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");

/**
* class jpg, gif, png =========> BMP
*
* { Description :- 
*	class that resize and convert jpg, gif or png to bmp
* }
* for more info contact with me (mahabub1212@yahoo.com)
* you can modify or use or redistribute this class.
*/
class ToBmp{
	
	// new image width
	var  $new_width;
	
	// new image height
	var $new_height;
	
	// image resources
	var $image_resource;
	
	function image_info($source_image){
		$img_info = getimagesize($source_image);
		
		switch ($img_info['mime']){
			case "image/jpeg": { $this->image_resource = imagecreatefromjpeg ($source_image);   break; }
			case "image/gif":  { $this->image_resource = imagecreatefromgif  ($source_image);   break; }
			case "image/png":  { $this->image_resource = imagecreatefrompng  ($source_image);   break; }
			default: {die("unsupported image time");}
		}
	}
	
	public function imagebmp($file_path = ''){
		
		if(!$this->image_resource) die("cant not convert. image resource is null");
		$picture_width  = imagesx($this->image_resource);
		$picture_height = imagesy($this->image_resource);
		
		
		if(!imageistruecolor($this->image_resource)){
			$tmp_img_reource = imagecreatetruecolor($picture_width,$picture_height);
			imagecopy($tmp_img_reource,$this->image_resource, 0, 0, 0, 0, $picture_width, $picture_height);
			imagedestroy($this->image_resource);
			$this->image_resource = $tmp_img_reource;
			
		}
		
		if((int) $this->new_width >0 && (int) $this->new_height > 0){
			
			$image_resized = imagecreatetruecolor($this->new_width, $this->new_height); 
			imagecopyresampled($image_resized,$this->image_resource,0,0,0,0,$this->new_width,$this->new_height,$picture_width,$picture_height);
			imagedestroy($this->image_resource);
			$this->image_resource =  $image_resized;
		
		}
		
		$result = '';
		
		$biBPLine =  ((int) $this->new_width >0 &&(int)$this->new_height > 0) ? $this->new_width * 3 : $picture_width * 3;
		$biStride = ($biBPLine + 3) & ~3;
		$biSizeImage =  ((int) $this->new_width >0 &&(int)$this->new_height > 0) ? $biStride * $this->new_height : $biStride * $picture_height;
		$bfOffBits = 54;
		$bfSize = $bfOffBits + $biSizeImage;
		
		$result .= substr('BM', 0, 2);
		$result .= pack ('VvvV', $bfSize, 0, 0, $bfOffBits);
		$result .= ((int) $this->new_width >0 &&(int)$this->new_height > 0) ? pack ('VVVvvVVVVVV', 40, $this->new_width, $this->new_height, 1, 24, 0, $biSizeImage, 0, 0, 0, 0) : pack ('VVVvvVVVVVV', 40, $picture_width, $picture_height, 1, 24, 0, $biSizeImage, 0, 0, 0, 0);
		
		$numpad = $biStride - $biBPLine;
		
		$h = ((int) $this->new_width >0 &&(int)$this->new_height > 0) ? $this->new_height : $picture_height;
		$w = ((int) $this->new_width >0 &&(int)$this->new_height > 0) ? $this->new_width  : $picture_width;
		

		for ($y = $h - 1; $y >= 0; --$y) {
			for ($x = 0; $x < $w; ++$x) {
				$col = imagecolorat ($this->image_resource, $x, $y);
				$result .= substr(pack ('V', $col), 0, 3);
			}
			for ($i = 0; $i < $numpad; ++$i) {
				$result .= pack ('C', 0);
			}
        }
		
		
      if($file_path == ''){
      	
      	header("Content-type: image/bmp");
      	echo $result;
      } else {
      	
      	$fp = fopen($file_path,"wb");
      	fwrite($fp,$result);
      	fclose($fp);
      	//=============
      }
      return ;  
	}
}


function createQRCode($dataCode,$dir)
{
	require_once('phpqrcode/qrlib.php');
	$tempDir = $dir;

	$codeContents = $dataCode;
	
	$fileName = $dataCode.'.png';
	
	$pngAbsoluteFilePath = $tempDir.$fileName;
	$urlRelativeFilePath = $tempDir.$fileName;
	
	//QRcode::png($codeContents, $pngAbsoluteFilePath);
	QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_H,20); 
	
	return $pngAbsoluteFilePath;
}

function splitData($dataCode)
{
	$data1=substr($dataCode, 0,3)." ";
	$data2=substr($dataCode, 3,3)." ";
	$data3=substr($dataCode, 6,3);
	return $data1.$data2.$data3;
}

function rp($data)
{
	
	$panjangData=strlen(ceil($data));
	$hasilBagi=substr($panjangData/3,0,1);
	$sisaBagi=$panjangData%3;
	$h="";
	$aw2=0;
	$ambil=3;
	
	if($sisaBagi!=0)
	{
		$h.=substr($data,0,$sisaBagi);
		$pjgDataBaru=strlen($data)-$sisaBagi;
		
		for($i=1;$i<=$pjgDataBaru/3;$i++)
		{
			$h.=".";
			$h.=substr($data,$sisaBagi,$ambil);
			$sisaBagi+=3;
		}
		
	}else
	{
		for($a=1;$a<=$hasilBagi;$a++)
		{
			$h=$h.substr($data,$aw2,$ambil);
			if($a<$hasilBagi)
			{
				$h=$h.".";
			}
			$aw2+=3;
		}
	}
	return $h;
}
// USAGE //

if(isset($_GET['bmp']))
{
	$dataArray=$_GET['bmp'];
	//$dataParse='["117431640;{\"kategori_tiket\":\"Anak\",\"harga\":5000,\"diskon\":50}"]';
	//$dataParse='["117431640;10000000;1;{\"kategori_tiket\":\"Anak Anak internasional negara \",\"harga\":8000000,\"diskon\":0}","51788331;20000;2;{\"kategori_tiket\":\"Remaja\",\"harga\":5000,\"diskon\":10}","316619873;20000;3;{\"kategori_tiket\":\"Remaja\",\"harga\":5000,\"diskon\":10}"]';
	//$dataParse='["117431640;{\"kategori_tiket\":\"Anak\",\"harga\":5000,\"diskon\":50}"]';
	//$dataParse='["117431640;totalBayar;pengguna;{\"kategori_tiket\":\"Anak\",\"harga\":5000,\"diskon\":50};invoice"]';
	
	$namaPrinter="EPSON TM-T82 ReceiptSA4";

	/*$tmpData=explode('["', $dataParse);
	$dataParse=$tmpData[1];
	$tmpData=explode('"]', $dataParse);
	$dataParse=$tmpData[0];
	$dataArray=explode('","', $dataParse);*/
	
	$var_magin_left = 20;
	//print receipt
	
	$p = printer_open($namaPrinter);
	printer_set_option($p, PRINTER_MODE, "RAW");
	printer_set_option($p, PRINTER_ORIENTATION, "LANDSCAPE");

	printer_start_doc($p);
	printer_start_page($p);

	$font = printer_create_font("Arial", 28, 22, PRINTER_FW_BOLD, false, false, false, 0);
	printer_select_font($p, $font);
	printer_draw_text($p, ".: I AM BALI :.",160,0);

	$font = printer_create_font("Arial", 20, 16, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_select_font($p, $font);
	printer_draw_text($p, "Interactive Art Museum",115,30);
	printer_draw_text($p, "MONUMENT BAJRA SANDHI",65,50);

	$font = printer_create_font("Arial", 20, 16, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_select_font($p, $font);
	printer_draw_text($p, "www.iamindonesia.co.id",105,70);
	
	$idKasir="";
	$bayarBiaya="0";
	$invoice="";
	
	$tmpKategoriTiket="";
	$tmpHargaTiket=0;
	$tmpQty=1;
	$totalHarga=0;
	for ($i=0; $i < 1; $i++) 
	{ 
		$dataLine=explode(';', $dataArray[$i]);
		$idKasir=$dataLine[2];
		$bayarBiaya=$dataLine[1];
		$invoice=$dataLine[4];
		
		$parsed_json = str_replace('}', '', str_replace('{', '',str_replace('"', '', str_replace('\\', '', json_decode(json_encode($dataLine[3]))))));
		$getDataLine=explode(',', $parsed_json);
		$getKategori=explode(':', $getDataLine[0]);
		$tmpKategoriTiket=$getKategori[1];
		
		$getHarga=explode(':', $getDataLine[1]);
		$hargaTiket=$getHarga[1];
		
		$getDiskon=explode(':', $getDataLine[2]);
		$diskonTiket=$getDiskon[1];
		
		$tmpHargaTiket=($hargaTiket-(($hargaTiket*$diskonTiket)/100));
		$totalHarga=$tmpHargaTiket;
	}
	
	$font = printer_create_font("Arial", 18, 14, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_draw_text($p, "=================================================================", $var_magin_left, 100);
	printer_select_font($p, $font);
	printer_draw_text($p, "Invoice : ", $var_magin_left, 120);
	printer_draw_text($p, $invoice, $var_magin_left + 130, 120);
	
	printer_draw_text($p, "Csh : ", $var_magin_left, 170);
	printer_draw_text($p, $idKasir, $var_magin_left + 80, 170);

	printer_draw_text($p, date('Y-m-d H:i:s'), $var_magin_left + 258, 170);
	printer_draw_text($p, "=================================================================", $var_magin_left, 190);

	printer_draw_text($p, "Transaksi", $var_magin_left, 210);
	printer_draw_text($p, "Qty", $var_magin_left + 280, 210);
	printer_draw_text($p, "Price", $var_magin_left + 360, 210);

	$row = 240;
	for ($i=1; $i <count($dataArray) ; $i++) 
	{ 
		$dataLine=explode(';', $dataArray[$i]);
		$idTiket=$dataLine[0];
		//echo $idTiket." ";
		
		$parsed_json = str_replace('}', '', str_replace('{', '',str_replace('"', '', str_replace('\\', '', json_decode(json_encode($dataLine[3]))))));
		
		$getDataLine=explode(',', $parsed_json);
		$getKategori=explode(':', $getDataLine[0]);
		$kategoriTiket=$getKategori[1];
		
		$getHarga=explode(':', $getDataLine[1]);
		$hargaTiket=$getHarga[1];
		
		$getDiskon=explode(':', $getDataLine[2]);
		$diskonTiket=$getDiskon[1];
		
		if($kategoriTiket!="Free")
		{
			if($tmpKategoriTiket!=$kategoriTiket)
			{
				if(strlen($tmpKategoriTiket)>16)
				{
					printer_draw_text($p, substr($tmpKategoriTiket, 0,16), $var_magin_left, $row);
					printer_draw_text($p, substr($tmpKategoriTiket, 16,strlen($tmpKategoriTiket)-16), $var_magin_left, $row+20);
					printer_draw_text($p, $tmpQty, $var_magin_left + 280, $row);
					//printer_draw_text($p, rp($tmpHargaTiket), $var_magin_left + 360, $row);
					$row += 50;
				}
				else 
				{
					printer_draw_text($p, $tmpKategoriTiket, $var_magin_left, $row);
					printer_draw_text($p, $tmpQty, $var_magin_left + 280, $row);
					//printer_draw_text($p, rp($tmpHargaTiket), $var_magin_left + 360, $row);
					$row += 30;	
				}
				
				$tmpQty=0;
				$tmpHargaTiket=0;
				$tmpKategoriTiket=$kategoriTiket;
			}
			
			$tmpHargaTiket+=($hargaTiket-(($hargaTiket*$diskonTiket)/100));
			$tmpQty++;
			
			//hargaTotal
			$totalHarga+=($hargaTiket-(($hargaTiket*$diskonTiket)/100));
		}
	}

	if(strlen($tmpKategoriTiket)>16)
	{
		printer_draw_text($p, substr($tmpKategoriTiket, 0,16), $var_magin_left, $row);
		printer_draw_text($p, substr($tmpKategoriTiket, 16,strlen($tmpKategoriTiket)-16), $var_magin_left, $row+20);
		printer_draw_text($p, $tmpQty, $var_magin_left + 280, $row);
		//printer_draw_text($p, rp($tmpHargaTiket), $var_magin_left + 360, $row);
		$row += 50;
	}
	else 
	{
		printer_draw_text($p, $tmpKategoriTiket, $var_magin_left, $row);
		printer_draw_text($p, $tmpQty, $var_magin_left + 280, $row);
		//printer_draw_text($p, rp($tmpHargaTiket), $var_magin_left + 360, $row);
		$row += 30;	
	}
	$row += 30;
	
	$row += 10;
	printer_draw_line($p, $var_magin_left, $row, 600, $row);
	$row += 10;
	printer_draw_text($p, "Total", $var_magin_left, $row);
	printer_draw_text($p, "Rp ".rp($totalHarga), $var_magin_left + 310, $row);
	$row += 20;
	printer_draw_text($p, "Bayar", $var_magin_left, $row);
	printer_draw_text($p, "Rp ".rp($bayarBiaya), $var_magin_left + 310, $row);
	$row += 20;
	printer_draw_text($p, "Kembali", $var_magin_left, $row);
	printer_draw_text($p, "Rp ".rp(($bayarBiaya-$totalHarga)), $var_magin_left + 310, $row);
	$row += 20;
	printer_draw_text($p, "=================================================================", $var_magin_left, $row);

	$font = printer_create_font("Arial", 20, 16, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_select_font($p, $font);
	$row += 40;
	printer_draw_text($p, "Thanks for coming", 170, $row);
	$row += 20;
	printer_draw_text($p, "Contact Support : +62361 4749 838", 20, $row);

	$font = printer_create_font("Arial", 18, 14, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_select_font($p, $font);
	$row += 20;
	printer_draw_text($p, "Jl. Puputan Niti Mandala", 140, $row);
	$row += 20;
	printer_draw_text($p, "Renon - Balis", 210, $row);
	$row += 70;
	printer_draw_text($p, ".", 70, $row);

	printer_delete_font($font);
	printer_end_page($p);
	printer_end_doc($p);
	printer_close ($p);
	
	//reciept2
	$p = printer_open($namaPrinter);
	printer_set_option($p, PRINTER_MODE, "RAW");
	printer_set_option($p, PRINTER_ORIENTATION, "LANDSCAPE");

	printer_start_doc($p);
	printer_start_page($p);

	$font = printer_create_font("Arial", 28, 22, PRINTER_FW_BOLD, false, false, false, 0);
	printer_select_font($p, $font);
	printer_draw_text($p, ".: I AM BALI :.",160,0);

	$font = printer_create_font("Arial", 20, 16, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_select_font($p, $font);
	printer_draw_text($p, "Interactive Art Museum",115,30);
	printer_draw_text($p, "MONUMENT BAJRA SANDHI",65,50);

	$font = printer_create_font("Arial", 20, 16, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_select_font($p, $font);
	printer_draw_text($p, "www.iamindonesia.co.id",105,70);
	
	$idKasir="";
	$bayarBiaya="0";
	$invoice="";
	
	$tmpKategoriTiket="";
	$tmpHargaTiket=0;
	$tmpQty=1;
	$totalHarga=0;
	for ($i=0; $i < 1; $i++) 
	{ 
		$dataLine=explode(';', $dataArray[$i]);
		$idKasir=$dataLine[2];
		$bayarBiaya=$dataLine[1];
		$invoice=$dataLine[4];
		
		$parsed_json = str_replace('}', '', str_replace('{', '',str_replace('"', '', str_replace('\\', '', json_decode(json_encode($dataLine[3]))))));
		$getDataLine=explode(',', $parsed_json);
		$getKategori=explode(':', $getDataLine[0]);
		$tmpKategoriTiket=$getKategori[1];
		
		$getHarga=explode(':', $getDataLine[1]);
		$hargaTiket=$getHarga[1];
		
		$getDiskon=explode(':', $getDataLine[2]);
		$diskonTiket=$getDiskon[1];
		
		$tmpHargaTiket=($hargaTiket-(($hargaTiket*$diskonTiket)/100));
		$totalHarga=$tmpHargaTiket;
	}
	
	$font = printer_create_font("Arial", 18, 14, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_draw_text($p, "=================================================================", $var_magin_left, 100);
	printer_select_font($p, $font);
	printer_draw_text($p, "Invoice : ", $var_magin_left, 120);
	printer_draw_text($p, $invoice, $var_magin_left + 130, 120);
	
	printer_draw_text($p, "Csh : ", $var_magin_left, 170);
	printer_draw_text($p, $idKasir, $var_magin_left + 80, 170);

	printer_draw_text($p, date('Y-m-d H:i:s'), $var_magin_left + 258, 170);
	printer_draw_text($p, "=================================================================", $var_magin_left, 190);

	printer_draw_text($p, "Transaksi", $var_magin_left, 210);
	printer_draw_text($p, "Qty", $var_magin_left + 280, 210);
	printer_draw_text($p, "Price", $var_magin_left + 360, 210);

	$row = 240;
	for ($i=1; $i <count($dataArray) ; $i++) 
	{ 
		$dataLine=explode(';', $dataArray[$i]);
		$idTiket=$dataLine[0];
		//echo $idTiket." ";
		
		$parsed_json = str_replace('}', '', str_replace('{', '',str_replace('"', '', str_replace('\\', '', json_decode(json_encode($dataLine[3]))))));
		
		$getDataLine=explode(',', $parsed_json);
		$getKategori=explode(':', $getDataLine[0]);
		$kategoriTiket=$getKategori[1];
		
		$getHarga=explode(':', $getDataLine[1]);
		$hargaTiket=$getHarga[1];
		
		$getDiskon=explode(':', $getDataLine[2]);
		$diskonTiket=$getDiskon[1];
		
		if($kategoriTiket!="Free")
		{
			if($tmpKategoriTiket!=$kategoriTiket)
			{
				if(strlen($tmpKategoriTiket)>16)
				{
					printer_draw_text($p, substr($tmpKategoriTiket, 0,16), $var_magin_left, $row);
					printer_draw_text($p, substr($tmpKategoriTiket, 16,strlen($tmpKategoriTiket)-16), $var_magin_left, $row+20);
					printer_draw_text($p, $tmpQty, $var_magin_left + 280, $row);
					//printer_draw_text($p, rp($tmpHargaTiket), $var_magin_left + 360, $row);
					$row += 50;
				}
				else 
				{
					printer_draw_text($p, $tmpKategoriTiket, $var_magin_left, $row);
					printer_draw_text($p, $tmpQty, $var_magin_left + 280, $row);
					//printer_draw_text($p, rp($tmpHargaTiket), $var_magin_left + 360, $row);
					$row += 30;	
				}
				
				$tmpQty=0;
				$tmpHargaTiket=0;
				$tmpKategoriTiket=$kategoriTiket;
			}
			
			$tmpHargaTiket+=($hargaTiket-(($hargaTiket*$diskonTiket)/100));
			$tmpQty++;
			
			//hargaTotal
			$totalHarga+=($hargaTiket-(($hargaTiket*$diskonTiket)/100));
		}
	}

	if(strlen($tmpKategoriTiket)>16)
	{
		printer_draw_text($p, substr($tmpKategoriTiket, 0,16), $var_magin_left, $row);
		printer_draw_text($p, substr($tmpKategoriTiket, 16,strlen($tmpKategoriTiket)-16), $var_magin_left, $row+20);
		printer_draw_text($p, $tmpQty, $var_magin_left + 280, $row);
		//printer_draw_text($p, rp($tmpHargaTiket), $var_magin_left + 360, $row);
		$row += 50;
	}
	else 
	{
		printer_draw_text($p, $tmpKategoriTiket, $var_magin_left, $row);
		printer_draw_text($p, $tmpQty, $var_magin_left + 280, $row);
		//printer_draw_text($p, rp($tmpHargaTiket), $var_magin_left + 360, $row);
		$row += 30;	
	}
	$row += 30;
	
	$row += 10;
	printer_draw_line($p, $var_magin_left, $row, 600, $row);
	$row += 10;
	printer_draw_text($p, "Total", $var_magin_left, $row);
	printer_draw_text($p, "Rp ".rp($totalHarga), $var_magin_left + 310, $row);
	$row += 20;
	printer_draw_text($p, "Bayar", $var_magin_left, $row);
	printer_draw_text($p, "Rp ".rp($bayarBiaya), $var_magin_left + 310, $row);
	$row += 20;
	printer_draw_text($p, "Kembali", $var_magin_left, $row);
	printer_draw_text($p, "Rp ".rp(($bayarBiaya-$totalHarga)), $var_magin_left + 310, $row);
	$row += 20;
	printer_draw_text($p, "=================================================================", $var_magin_left, $row);

	$font = printer_create_font("Arial", 20, 16, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_select_font($p, $font);
	$row += 40;
	printer_draw_text($p, "Thanks for coming", 170, $row);
	$row += 20;
	printer_draw_text($p, "Contact Support : +62361 4749 838", 20, $row);

	$font = printer_create_font("Arial", 18, 14, PRINTER_FW_NORMAL, false, false, false, 0);
	printer_select_font($p, $font);
	$row += 20;
	printer_draw_text($p, "Jl. Puputan Niti Mandala", 140, $row);
	$row += 20;
	printer_draw_text($p, "Renon - Balis", 210, $row);
	$row += 70;
	printer_draw_text($p, ".", 70, $row);

	printer_delete_font($font);
	printer_end_page($p);
	printer_end_doc($p);
	printer_close ($p);
	
	//print ticket
	for ($i=0; $i <count($dataArray) ; $i++) 
	{ 
		$dataLine=explode(';', $dataArray[$i]);
		$idTiket=$dataLine[0];
		//echo $idTiket." ";
		
		$parsed_json = str_replace('}', '', str_replace('{', '',str_replace('"', '', str_replace('\\', '', json_decode(json_encode($dataLine[3]))))));
		
		$getDataLine=explode(',', $parsed_json);
		$getKategori=explode(':', $getDataLine[0]);
		$kategori=$getKategori[1];
		
		$getHarga=explode(':', $getDataLine[1]);
		$harga=$getHarga[1];
	 	$hargaTemp=$getHarga[1];
		
		$getDiskon=explode(':', $getDataLine[2]);
		$diskon=$getDiskon[1];
		
		
		///start
		$tiket=$kategori;
		$harga="Rp ".rp($harga-(($harga*$diskon)/100)).",-";
		$dataCode=$idTiket;
		$dir='qrcode/';
		
		// intialize the class
		$ToBMP = new ToBmp();
		// convert png to bmp
		$ToBMP->image_info(createQRCode($dataCode,$dir));
		$ToBMP->new_width  = 580;
		$ToBMP->new_height = 580;
		$fileNew=$dir.$dataCode.".bmp";
		$ToBMP->imagebmp($fileNew);
		
		//print
		$var_magin_left = 20;
		
		$handle = printer_open($namaPrinter);
		printer_set_option($handle, PRINTER_MODE, "raw");
		printer_set_option($handle,PRINTER_ORIENTATION, PRINTER_ORIENTATION_LANDSCAPE); 
		printer_start_doc($handle);
		printer_start_page($handle);
		
		printer_draw_bmp($handle, 'logo.bmp', $var_magin_left, 0, 240,80);
		printer_draw_bmp($handle, 'bajra.bmp', 255, 0, 330,170);
		
		$font = printer_create_font("Arial", 100, 50, PRINTER_FW_BOLD, false, false, false, 0);
		printer_select_font($handle, $font);
		printer_draw_text($handle, "BALI",$var_magin_left,80);
		
		$row=190;
		printer_draw_line($handle, $var_magin_left, $row, 600, $row);
		
		$row+=5;
		$font = printer_create_font("Arial", 70, 30, PRINTER_FW_BOLD, false, false, false, 0);
		printer_select_font($handle, $font);
		if($hargaTemp=="0")
		{
			printer_draw_text($handle, "FREE",$var_magin_left,$row);
		}
		else
		{
			if(strlen($tiket)>16)
			{
				if(strlen($tiket)>33)
				{
					$font = printer_create_font("Arial", 33, 15, PRINTER_FW_BOLD, false, false, false, 0);
					printer_select_font($handle, $font);
					printer_draw_text($handle, substr($tiket, 0,33),$var_magin_left,$row);
					printer_draw_text($handle, substr($tiket, 33,strlen($tiket)-33),$var_magin_left,$row+35);
				}
				else
				{		
					$font = printer_create_font("Arial", 40, 16, PRINTER_FW_BOLD, false, false, false, 0);
					printer_select_font($handle, $font);
					printer_draw_text($handle, $tiket,$var_magin_left,$row+10);
				}
			}
			else
			{
				$font = printer_create_font("Arial", 70, 30, PRINTER_FW_BOLD, false, false, false, 0);
				printer_select_font($handle, $font);
				printer_draw_text($handle, $tiket,$var_magin_left,$row);
			}
		}
		
		$row+=75;
		printer_draw_line($handle, $var_magin_left, $row, 600, $row);
		
		$row+=10;
		$font = printer_create_font("Arial", 50, 20, PRINTER_FW_BOLD, false, false, false, 0);
		printer_select_font($handle, $font);
		if($hargaTemp=="0")
		{
			printer_draw_text($handle, "-",300,$row+40);
		}
		else
		{
			printer_draw_text($handle, $harga,300,$row+40);
		}
		
		$font = printer_create_font("Arial", 35, 15, PRINTER_FW_BOLD, false, false, false, 0);
		printer_select_font($handle, $font);
		
		$date=date('d-m-Y');
		$time=date('H:i');
		
		printer_draw_text($handle, "Date : ",300,$row+120);
		printer_draw_text($handle, "Time : ",300,$row+160);
		printer_draw_text($handle, "Code : ",300,$row+210);
		
		$font = printer_create_font("Arial", 30, 13, PRINTER_FW_NORMAL, false, false, false, 0);
		printer_select_font($handle, $font);
		printer_draw_text($handle, $date,400,$row+124);
		printer_draw_text($handle, $time,400,$row+165);
		
		$font = printer_create_font("Arial", 26, 11, PRINTER_FW_NORMAL, false, false, false, 0);
		printer_select_font($handle, $font);
		printer_draw_text($handle, splitData($idTiket),400,$row+217);
		
		printer_draw_bmp($handle, $fileNew, 0, $row, 300, 300);
		
		$row+=280;
		printer_draw_line($handle, $var_magin_left, $row, 600, $row);
		
		$row+=30;
		$font = printer_create_font("Arial", 28, 13, PRINTER_FW_NORMAL, false, false, false, 0);
		printer_select_font($handle, $font);
		printer_draw_text($handle, "Location : Monument Bajra Sandhi",$var_magin_left,$row);
		$row+=30;
	 	printer_draw_text($handle, "Email : info@iamindonesia.co.id",$var_magin_left,$row);
		$row+=30;
	 	printer_draw_text($handle, "Telp : +62361 4749 838",$var_magin_left,$row);
		$row+=30;
		printer_draw_text($handle, "www.iamindonesia.co.id",$var_magin_left,$row);
		$row+=50;
		printer_draw_text($handle, ".",$var_magin_left,$row);
		
		printer_end_page($handle);
		printer_end_doc($handle);
		printer_close ($handle);
		
		unlink($dir.$dataCode.".png");
		unlink($fileNew);
	}
	
	//$dataParse='["117431640;{\"kategori_tiket\":\"Anak\",\"harga\":0,\"diskon\":50}","51788331;{\"kategori_tiket\":\"Remaja\",\"harga\":5000,\"diskon\":50}","316619873;{\"kategori_tiket\":\"Remaja\",\"harga\":5000,\"diskon\":50}"]';
	//$dataParse='["117431640;{\"kategori_tiket\":\"Anak\",\"harga\":5000,\"diskon\":50}"]';
	//$dataParse='["117431640;totalBayar;pengguna;{\"kategori_tiket\":\"Anak\",\"harga\":5000,\"diskon\":50}"]';
	
	/*$tmpData=explode('["', $dataParse);
	$dataParse=$tmpData[1];
	$tmpData=explode('"]', $dataParse);
	$dataParse=$tmpData[0];
	$dataArray=explode('","', $dataParse);
	
	$tmpData="";
	for ($i=0; $i < count($dataArray); $i++) 
	{ 
		$dataLine=explode(';', $dataArray[$i]);
		$idTiket=$dataLine[0];
		//echo $idTiket." ";
		
		$parsed_json = str_replace('}', '', str_replace('{', '',str_replace('"', '', str_replace('\\', '', json_decode(json_encode($dataLine[1]))))));
		
		$getDataLine=explode(',', $parsed_json);
		$getKategori=explode(':', $getDataLine[0]);
		$kategori=$getKategori[1];
		
		$getHarga=explode(':', $getDataLine[1]);
		$harga=$getHarga[1];
		$hargaTemp=$getHarga[1];
		
		$getDiskon=explode(':', $getDataLine[2]);
		$diskon=$getDiskon[1];
		
		
		///start
		$tiket=$kategori;
		$harga="Rp ".rp($harga-(($harga*$diskon)/100)).",-";
		$dataCode=$idTiket;
		$dir='qrcode/';
		
		// intialize the class
		$ToBMP = new ToBmp();
		// convert png to bmp
		$ToBMP->image_info(createQRCode($dataCode,$dir));
		$ToBMP->new_width  = 580;
		$ToBMP->new_height = 580;
		$fileNew=$dir.$dataCode.".bmp";
		$ToBMP->imagebmp($fileNew);
		
		
		//print
		$var_magin_left = 20;
		
		$handle = printer_open("EPSON TM-T82 ReceiptSA4");
		printer_set_option($handle, PRINTER_MODE, "raw");
		printer_set_option($handle,PRINTER_ORIENTATION, PRINTER_ORIENTATION_LANDSCAPE); 
		printer_start_doc($handle);
		printer_start_page($handle);
		
		printer_draw_bmp($handle, 'logo.bmp', $var_magin_left, 0, 240,80);
		printer_draw_bmp($handle, 'bajra.bmp', 255, 0, 330,170);
		
		$font = printer_create_font("Arial", 100, 50, PRINTER_FW_BOLD, false, false, false, 0);
		printer_select_font($handle, $font);
		printer_draw_text($handle, "BALI",$var_magin_left,80);
		
		$row=190;
		printer_draw_line($handle, $var_magin_left, $row, 600, $row);
		
		$row+=5;
		$font = printer_create_font("Arial", 70, 30, PRINTER_FW_BOLD, false, false, false, 0);
		printer_select_font($handle, $font);
		if($hargaTemp=="0")
		{
			printer_draw_text($handle, "FREE",$var_magin_left,$row);
		}
		else
		{
			printer_draw_text($handle, $tiket,$var_magin_left,$row);
		}
		
		$row+=75;
		printer_draw_line($handle, $var_magin_left, $row, 600, $row);
		
		$row+=10;
		$font = printer_create_font("Arial", 50, 20, PRINTER_FW_BOLD, false, false, false, 0);
		printer_select_font($handle, $font);
		if($hargaTemp=="0")
		{
			printer_draw_text($handle, "-",300,$row+40);
		}
		else
		{
			printer_draw_text($handle, $harga,300,$row+40);
		}
		
		$font = printer_create_font("Arial", 35, 15, PRINTER_FW_BOLD, false, false, false, 0);
		printer_select_font($handle, $font);
		
		$date=date('d-m-Y');
		$time=date('H:s');
		
		printer_draw_text($handle, "Date : ",300,$row+120);
		printer_draw_text($handle, "Time : ",300,$row+160);
		printer_draw_text($handle, "Code : ",300,$row+210);
		
		$font = printer_create_font("Arial", 30, 13, PRINTER_FW_NORMAL, false, false, false, 0);
		printer_select_font($handle, $font);
		printer_draw_text($handle, $date,400,$row+124);
		printer_draw_text($handle, $time,400,$row+165);
		
		$font = printer_create_font("Arial", 26, 11, PRINTER_FW_NORMAL, false, false, false, 0);
		printer_select_font($handle, $font);
		printer_draw_text($handle, splitData($idTiket),400,$row+217);
		
		printer_draw_bmp($handle, $fileNew, 0, $row, 300, 300);
		
		$row+=280;
		printer_draw_line($handle, $var_magin_left, $row, 600, $row);
		
		$row+=30;
		$font = printer_create_font("Arial", 28, 13, PRINTER_FW_NORMAL, false, false, false, 0);
		printer_select_font($handle, $font);
		printer_draw_text($handle, "Location : Monument Bajra Sandhi",$var_magin_left,$row);
		$row+=30;
	 	printer_draw_text($handle, "Email : info@iamindonesia.co.id",$var_magin_left,$row);
		$row+=30;
	 	printer_draw_text($handle, "Telp : +62361 4749 838",$var_magin_left,$row);
		$row+=30;
		printer_draw_text($handle, "www.iamindonesia.co.id",$var_magin_left,$row);
		$row+=50;
		printer_draw_text($handle, ".",$var_magin_left,$row);
		
		printer_end_page($handle);
		printer_end_doc($handle);
		printer_close ($handle);
	}*/
}	
?>