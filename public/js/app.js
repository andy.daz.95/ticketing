$(document).ready(function(){
	$.ajaxSetup({
		headers:{
			'X-CSRF-TOKEN':$('meta[name="csrf_token"]').attr('content')
		}
	});

	toastr.options = {
		"progressBar":true,
		"positionClass":"toast-bottom-right",
		"timeOut":"3000"
	};
});