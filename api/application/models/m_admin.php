<?php
	class M_admin extends CI_Model 
	{
		function __construct() 
		{
			parent::__construct();
		}

		//PENGGUNA/////////////////////////////////////////////////////////////////////
		function cekPengguna($username,$password){
			$this->db->select('id_pengguna');
			$this->db->where('nama_pengguna',$username);
			$this->db->where('pass_enkrip',$password);
			$this->db->where('status_pengguna','1');
			$this->db->where(" id_pengguna_kategori IN('4','1') ");
			$query=$this->db->get('dt_penggunas');
			return $query->result();
		}

		//TRANSAKSI TICKET////////////////////////////////////////////////////////////
		function cekHasilScan($hasil) {
			$this->db->select('id_tiket,wkt_transaksi');
			$this->db->where('id_transaksi',$hasil);
			$this->db->where('status_scan','0');
			$query=$this->db->get('dt_transaksi_tikets');
			return $query->result();
		}
		function updateDataTransaksi($myid,$hasil,$uStatus,$uDateScan){
			$data=array('id_scanner'=>$myid, 'status_scan'=>$uStatus, 'wkt_scan'=>$uDateScan);
			$this->db->where('id_transaksi',$hasil);
			return $this->db->update('dt_transaksi_tikets',$data);
		}
		function showTransaksi($myid,$year,$month,$date){
			$this->db->select('id_transaksi,wkt_scan');
			$this->db->where('YEAR(wkt_scan)',$year);
			$this->db->where('MONTH(wkt_scan)',$month);
			$this->db->where('DAY(wkt_scan)',$date);
			$this->db->where('id_scanner',$myid);
			$this->db->where('status_scan','1');
			$this->db->order_by('wkt_scan','DESC');
			$query=$this->db->get('dt_transaksi_tikets');
			return $query->result();
		}

		//LOG////////////////////////////////////////////////////////////////////////////
		function insertLog($id_log,$id_pengguna,$aktifitas,$wkt_log){
			$data=array('id_log'=>$id_log, 'id_pengguna'=>$id_pengguna, 'aktivitas'=>$aktifitas, 'wkt_log'=>$wkt_log);
			$this->db->set($data);
            $this->db->insert('dt_logs');
		}
	}