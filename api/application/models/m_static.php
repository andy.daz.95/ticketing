<?php
	class M_static extends CI_Model 
	{
		function __construct() 
		{
			parent::__construct();
		}

		function xss($data)
		{
			$cek=stripcslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES)));
			return $cek;
		}

		function display_html($data)
		{
			$cek=htmlspecialchars_decode($data);
			return $cek;
		}

		function eng_display_month($data)
        {
            $months=array("Jan" => "January", "Feb" => "February", "Mar" => "March", "Apr" => "April", "May" => "May", "Jun" => "Juny", "Jul" => "July",
            "Aug" => "August", "Sep" => "September", "Oct" => "October", "Nov" => "November", "Dec" => "December");
            return $months[$data];
        }

		function time_eng_withoutday($date)
        {
            $timestamp = strtotime($date);
            $day = date('D', $timestamp);
            $month = date('M', $timestamp);
            
            $getWaktu=
			$this->m_static->eng_display_month($month).' '.
            substr($date, 8,2).', '.
            substr($date, 0,4);
            
            return $getWaktu;
        }

        function date_to_timestamp($date)
		{
			$result="";
			if($date!="")
			{
				$get = new DateTime($date);
				$result = $get->getTimestamp();				
			}
			else 
			{
				$get = new DateTime();
				$result = $get->getTimestamp();
			}
			return $result;
		}

		function waktu_sekarang()
		{
			return date('Y-m-d H:i:s', strtotime('+ 6 hours'));
		}
		function waktu_sekarang_history()
        {
            return date('Y-m-d', strtotime('+ 6 hours'));
        }

        function insertLog($id_pengguna,$aktifitas){
        	$id_log=$this->date_to_timestamp("");
        	$wkt_log=$this->m_static->waktu_sekarang();

        	$this->m_admin->insertLog($id_log,$id_pengguna,$aktifitas,$wkt_log);
        }
	}
