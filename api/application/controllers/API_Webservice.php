<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API_Webservice extends CI_Controller 
{	
	function __construct() 
	{
		parent::__construct();
		$this->load->model('m_static');
        $this->load->model('m_admin','',TRUE);
        $this->load->library('Ajax');
		header('Access-Control-Allow-Origin: *');
	}

	function index()
	{
		//echo $this->m_static->waktu_sekarang();
	}

	function serviceAuthLogin(){
		$data['status']='0';
		$data['notif']="";
		$data['getID']="";

		if(isset($_POST['key'])){
			$private=$this->m_static->xss($_POST['key']);
			$username=$this->m_static->xss($_POST['username']);
			$password=$this->m_static->xss($_POST['password']);
			$sha1=sha1($password);

			if($private==$this->config->item('private_key')){
				
				$cekPengguna=$this->m_admin->cekPengguna($username,$sha1);
				if(count($cekPengguna)>0){
					$data['status']="200";
					$data['notif']="Success";
					$data['getID']=$cekPengguna[0]->id_pengguna;

					$this->m_static->insertLog($cekPengguna[0]->id_pengguna,"Login");
				} else {
					$data['status']="400";
					$data['notif']="Username and password not match";
				}

			} else {
				$data['status']="400";
				$data['notif']="Your key expired, please login again!";
			}
		} else {
			$data['status']="400";
			$data['notif']="Error, system can't process your data";
		}

		echo json_encode($data);
	}

	function serviceAuthLogout() {
		$myid=$this->m_static->xss($_POST['myid']);
		$this->m_static->insertLog($myid,"Logout");
	}

	function serviceScan()
	{
		$data['status']='0';
		$data['notif']="";

		if(isset($_POST['key'])){
			$private=$this->m_static->xss($_POST['key']);
			$myid=$this->m_static->xss($_POST['myid']);
			$hasil=$this->m_static->xss($_POST['hasil']);
			$type=$this->m_static->xss($_POST['type']);

			if($private==$this->config->item('private_key')){

				$cekHasilScan=$this->m_admin->cekHasilScan($hasil);
				if(count($cekHasilScan)>0){
					$uStatus = '1';
					$uDateScan = $this->m_static->waktu_sekarang();
					$getWaktuTransaksi="";
					foreach ($cekHasilScan as $key) 
					{
						$getWaktuTransaksi=substr($key->wkt_transaksi, 0, 10);
					}

					if($getWaktuTransaksi==substr($uDateScan, 0,10))
					{
						$updateDataTransaksi=$this->m_admin->updateDataTransaksi($myid,$hasil,$uStatus,$uDateScan);
						if($updateDataTransaksi){
							$data['status']="200";
							$data['notif']="Successfully scanned";

							$this->m_static->insertLog($myid,$type);
						} else {
							$data['status']="400";
							$data['notif']="Please try again!";
						}
					} else {
						$data['status']="201";
						$data['notif']="Sorry, Ticket expired. You can use ticket only one day";
					}

				} else {
					$data['status']="201";
					$data['notif']="Sorry, Ticket already been scanned!";
				}

			} else {
				$data['status']="400";
				$data['notif']="Your key expired, please login again!";
			}
		} else {
			$data['status']="400";
			$data['notif']="Error, system can't process your data";
		}

		echo json_encode($data);
	}

	function displayHistory()
	{
		$data['status']="0";
		$data['notif']="";
		$data['result']="";

		if(isset($_POST['key'])){
			$private=$this->m_static->xss($_POST['key']);
			$myid=$this->m_static->xss($_POST['myid']);
			$waktu=$this->m_static->xss($_POST['waktu']);

			if($private==$this->config->item('private_key')){
				if($waktu==""){
                    $wkt_scan=$this->m_static->waktu_sekarang_history();
                } else {
                    $wkt_scan=$waktu;
                }

				$explode = explode('-', $wkt_scan);
				$year=$explode[0];
				$month=$explode[1];
				$date=$explode[2];

				$showTransaksi=$this->m_admin->showTransaksi($myid,$year,$month,$date);
				if(count($showTransaksi)>0){
					$result = [];
					foreach($showTransaksi as $key){
						$trans_waktu = $this->m_static->time_eng_withoutday($key->wkt_scan);
						$result[] = array('trans_id'=>$key->id_transaksi, 'trans_waktu'=>$trans_waktu);
					}
					$data['status'] = "200";
					$data['notif'] = "Success";
					$data['result'] = $result;
				} else {
					$data['status']="201";
					$data['notif']="No Data Available";
				}

			} else {
				$data['status']="400";
				$data['notif']="Your key expired, please login again!";
			}
		} else {
			$data['status']="400";
			$data['notif']="Error, system can't process your data";
		}

		echo json_encode($data);
	}
}
