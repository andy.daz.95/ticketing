<!DOCTYPE html>
<html>
    <head>
        <title>503 Service Unavailable</title>
        <link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
        <link rel="icon" href="{{ URL::asset('favicon.ico') }}">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
                font-family: "Source Sans Pro";
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">503 Service Unavailable</div>
            </div>
        </div>
    </body>
</html>