@extends('layouts.app')

@section('content')
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Selamat Datang</div>
					<div class="box-body">
						<p>Silahkan gunakan aplikasi ini dengan semaksimal mungkin.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection