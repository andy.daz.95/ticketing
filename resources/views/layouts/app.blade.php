<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf_token" content="{!! csrf_token() !!}">
		<title>Ticketing</title>
		<link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/dist/css/skins/skin-blue.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/plugins/timepicker/timepicker.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/plugins/toastr/toastr.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/plugins/daterangepicker/daterangepicker.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/plugins/airdatepicker/datepicker.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
		<link rel="icon" href="{{ URL::asset('favicon.ico') }}">
		<script src="{{ URL::asset('theme/plugins/jquery/jquery.min.js') }}"></script>
		<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini sidebar-collapse" id="app-layout">
		<div class="wrapper">
			@include('layouts.header')
			@include('layouts.sidebar')
			<div class="content-wrapper">
				@yield('content')
				@include('shared.profile')
			</div>
			@include('layouts.footer')
		</div>
		<script src="{{ URL::asset('theme/plugins/bootbox/bootbox.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/filestyle/filestyle.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/timepicker/timepicker.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/form/form.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/submitter/submitter.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/price/price.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/numeric/numeric.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/toastr/toastr.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/moment/moment.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/chart/chart.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/daterangepicker/daterangepicker.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/airdatepicker/datepicker.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/airdatepicker/i18n/datepicker.en.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/timer/jquery.timer.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/ajaxmask/ajaxmask.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/webcam/webcam.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/validate/validate.min.js') }}"></script>
		<script src="{{ URL::asset('theme/plugins/validate/additional.min.js') }}"></script>
		<script src="{{ URL::asset('js/app.js') }}"></script>
		<script src="{{ URL::asset('theme/dist/js/app.min.js') }}"></script>
		<script type="text/javascript">
			var $loading = $("#loader").hide();
			var $overlay = $(".overlay").hide();
			$(document)
			  .ajaxStart(function () {
			    $loading.show();
			    $overlay.show();
			  })
			  .ajaxStop(function () {
			    $loading.hide();
			    $overlay.hide();
			  });
		</script>
	</body>
</html>