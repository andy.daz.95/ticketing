<header class="main-header">
	<a class="logo" href="#">
		<span class="logo-mini"><b>I</b>AM</span>
		<span class="logo-lg"><b>I</b>AM</span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a class="sidebar-toggle" href="#" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span></a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a class="dropdown-toggle" href="#" data-toggle="dropdown">
						<img class="user-image" src="{{ URL::asset('theme/images/user.png') }}" alt="User Image">
						<span class="hidden session pengguna">{{ session('login') }}</span>
						<span class="hidden-xs">{{ session('name') }}</span>
						<span class="hidden-xs caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header">
							<img class="img-circle" src="{{ URL::asset('theme/images/user.png') }}" alt="User Image">
							<p>{{ session('category') }}</p>
						</li>
						<li class="user-footer">
							<div class="pull-left"><a class="btn btn-primary btn-flat" href="#modalProfile" data-toggle="modal">Profile</a></div>
							<div class="pull-right"><a class="btn btn-danger btn-flat" href="{{ url('/logout') }}">Sign Out</a></div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>