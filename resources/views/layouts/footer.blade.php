<footer class="main-footer">
	<div class="pull-right hidden-xs"></div>
	<strong>Copyright &copy; 2016 <a href="http://osthread.com/" target="blank">OS Team Development</a>.</strong> <i>All rights reserved.</i>
</footer>