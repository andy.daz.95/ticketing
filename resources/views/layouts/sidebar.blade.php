<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header"><center>Menu Utama</center></li>
			@if(session('role') == 1)
				<li @if(session('page') == 0) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-database"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/penggunakategori') }}"><i class="fa fa-link"></i> <span>Pengguna Kategori</span></a></li>
						<li><a href="{{ url('/pengguna') }}"><i class="fa fa-link"></i> <span>Pengguna</span></a></li>
						<li><a href="{{ url('/travel') }}"><i class="fa fa-link"></i> <span>Travel</span></a></li>
						<li><a href="{{ url('/guide') }}"><i class="fa fa-link"></i> <span>Guide</span></a></li>
						<li><a href="{{ url('/kategoritiket') }}"><i class="fa fa-link"></i> <span>Kategori Tiket</span></a></li>
						<li><a href="{{ url('/log') }}"><i class="fa fa-link"></i> <span>Log</span></a></li>
					</ul>
				</li>
				<li @if(session('page') == 1) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-shopping-cart"></i> <span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/pendapatanguide') }}"><i class="fa fa-link"></i> <span>Pendapatan Guide</span></a></li>
						<li><a href="{{ url('/finance/transaksikomisiguide') }}"><i class="fa fa-link"></i> <span>Komisi Guide</span></a></li>
						<li><a href="{{ url('/transaksitiket') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket</span></a></li>
						<li><a href="{{ url('/transaksitiketvoid') }}"><i class="fa fa-link"></i> <span>Void Tiket</span></a></li>
					</ul>
				</li>
				<li @if(session('page') == 2) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/laporantransaksitiketgrafik') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket Grafik</span></a></li>
						<li><a href="{{ url('/laporantransaksitiket') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket</span></a></li>
						<li><a href="{{ url('/laporantransaksikomisiguide') }}"><i class="fa fa-link"></i> <span>Komisi Guide</span></a></li>
						<li><a href="{{ url('/laporanlog') }}"><i class="fa fa-link"></i> <span>Log</span></a></li>
					</ul>
				</li>
			@elseif(session('role') == 2)
				<li class="treeview active">
					<a href="#"><i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/laporantransaksitiketgrafik') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket Grafik</span></a></li>
						<li><a href="{{ url('/laporantransaksitiket') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket</span></a></li>
						<li><a href="{{ url('/laporantransaksikomisiguide') }}"><i class="fa fa-link"></i> <span>Komisi Guide</span></a></li>
						<li><a href="{{ url('/laporanlog') }}"><i class="fa fa-link"></i> <span>Log</span></a></li>
					</ul>
				</li>
			@elseif(session('role') == 3)
				<li @if(session('page') == 1) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-shopping-cart"></i> <span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/transaksitiket') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket</span></a></li>
						<li><a href="{{ url('/transaksitiketvoid') }}"><i class="fa fa-link"></i> <span>Void Tiket</span></a></li>
					</ul>
				</li>
				<li @if(session('page') == 2) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/tiket/laporantransaksitiket') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket</span></a></li>
					</ul>
				</li>
			@elseif(session('role') == 5)
				<li @if(session('page') == 0 || session('page') == 1) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-shopping-cart"></i> <span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/pendapatanguide') }}"><i class="fa fa-link"></i> <span>Pendapatan Guide</span></a></li>
						<li><a href="{{ url('/guide') }}"><i class="fa fa-link"></i> <span>Guide</span></a></li>
					</ul>
				</li>
				<li @if(session('page') == 2) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/komisi/laporantransaksikomisiguide') }}"><i class="fa fa-link"></i> <span>Komisi Guide</span></a></li>
					</ul>
				</li>
			@elseif(session('role') == 6)
				<li @if(session('page') == 0) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-database"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/travel') }}"><i class="fa fa-link"></i> <span>Travel</span></a></li>
						<li><a href="{{ url('/finance/guide') }}"><i class="fa fa-link"></i> <span>Guide</span></a></li>
					</ul>
				</li>
				<li @if(session('page') == 1) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-shopping-cart"></i> <span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/finance/pendapatanguide') }}"><i class="fa fa-link"></i> <span>Pendapatan Guide</span></a></li>
						<li><a href="{{ url('/finance/transaksikomisiguide') }}"><i class="fa fa-link"></i> <span>Komisi Guide</span></a></li>
					</ul>
				</li>
				<li @if(session('page') == 2) class = "treeview active" @else class = "treeview" @endif>
					<a href="#"><i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu">
						<li><a href="{{ url('/finance/laporantransaksitiketgrafik') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket Grafik</span></a></li>
						<li><a href="{{ url('/finance/laporantransaksitiket') }}"><i class="fa fa-link"></i> <span>Transaksi Tiket</span></a></li>
						<li><a href="{{ url('/finance/laporantransaksikomisiguide') }}"><i class="fa fa-link"></i> <span>Komisi Guide</span></a></li>
						<li><a href="{{ url('/finance/laporanlog') }}"><i class="fa fa-link"></i> <span>Log</span></a></li>
					</ul>
				</li>
			@endif
		</ul>
	</section>
</aside>