<div class="modal fade" id="modalSearchTravel">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
				<h4 class="modal-title">Data Travel</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="box">
							<div class="box-header with-border">Cari Travel</div>
							<div class="box-body">
								<form class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textSearchTravel">Cari</label>
										<div class="col-sm-10">
											<input class="form-control" id="textSearchTravel" type="text" placeholder="Nama Travel">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="box">
							<div class="box-header with-border">Daftar Informasi</div>
							<div class="box-body">
								<table class="table table-hover">
									<thead>
										<th>ID</th>
										<th>Travel</th>
									</thead>
									<tbody class="rows travel">
										@foreach($travels as $travel)
										<tr>
											<td>{{ $travel->id_travel }}</td>
											<td>{{ $travel->nama_travel }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								{{ $travels->links() }}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<small><i>Silahkan pilih travel dengan menekan baris pada travel bersangkutan.</i></small>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		/* Choose Search Travel */
		$("tbody.rows.travel").on("click", "tr", function(e){
			var id = $(this).find("td").eq(0).text();
			var travel = $(this).find("td").eq(1).text();

			$("#textIDTravel").val(id);
			$("#textTravel").val(travel);

			$("#modalSearchTravel").modal("hide");
		});

		/* Search Travel */
		$("#textSearchTravel").on("keypress", function(e){
			if(e.which == 13){
				var id = $(this).val();

				if($(this).val() == ""){
					id = "all";
				}

				$.ajax({
					type:"GET",
					url:"/ticketing/public/search/travel/" + id,
					dataType:"json",
					success:function(response){
						var row = "";

						$("tbody.rows.travel").empty();

						$.each(response, function(index, value){
							row = row + "<tr>" +
										"<td>" + value.id_travel + "</td>" +
										"<td>" + value.nama_travel + "</td>" +
										"</tr>";
						});

						$("tbody.rows.travel").append(row);
					}
				});

				return false;
			}
		});
	});
</script>