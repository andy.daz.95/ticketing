<div class="modal fade" id="modalSearchPengguna">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
				<h4 class="modal-title">Data Pengguna</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="box">
							<div class="box-header with-border">Cari Pengguna</div>
							<div class="box-body">
								<form class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textSearchPengguna">Cari</label>
										<div class="col-sm-10">
											<input class="form-control" id="textSearchPengguna" type="text" placeholder="Nama Pengguna">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="box">
							<div class="box-header with-border">Daftar Informasi</div>
							<div class="box-body">
								<table class="table table-hover">
									<thead>
										<th>ID</th>
										<th>Pengguna</th>
										<th>Kategori</th>
									</thead>
									<tbody class="rows pengguna">
										@foreach($penggunas as $pengguna)
										<tr>
											<td>{{ $pengguna->id_pengguna }}</td>
											<td>{{ $pengguna->nama_lengkap }}</td>
											<td>{{ $pengguna->pengguna_kategori }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								{{ $penggunas->links() }}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<small><i>Silahkan pilih pengguna dengan menekan baris pada pengguna bersangkutan.</i></small>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		/* Choose Search Pengguna */
		$("tbody.rows.pengguna").on("click", "tr", function(e){
			var id = $(this).find("td").eq(0).text();
			var pengguna = $(this).find("td").eq(1).text();

			$("#textIDPengguna").val(id);
			$("#textPengguna").val(pengguna);

			$("#modalSearchPengguna").modal("hide");
		});

		/* Search Pengguna */
		$("#textSearchPengguna").on("keypress", function(e){
			if(e.which == 13){
				var id = $(this).val();

				if($(this).val() == ""){
					id = "all";
				}

				$.ajax({
					type:"GET",
					url:"/ticketing/public/search/pengguna/" + id,
					dataType:"json",
					success:function(response){
						var row = "";

						$("tbody.rows.pengguna").empty();

						$.each(response, function(index, value){
							row = row + "<tr>" +
										"<td>" + value.id_pengguna + "</td>" +
										"<td>" + value.nama_lengkap + "</td>" +
										"<td>" + value.pengguna_kategori + "</td>" +
										"</tr>";
						});

						$("tbody.rows.pengguna").append(row);
					}
				});

				return false;
			}
		});
	});
</script>