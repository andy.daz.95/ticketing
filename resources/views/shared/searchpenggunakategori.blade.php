<div class="modal fade" id="modalSearchPenggunaKategori">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
				<h4 class="modal-title">Data Pengguna Kategori</h4>
			</div>
			<div class="modal-body">
				<table class="table table-striped">
					<thead>
						<th>ID</th>
						<th>Kategori</th>
						<th>&nbsp;</th>
					</thead>
					<tbody>
						@foreach($penggunakategoris as $penggunakategori)
						<tr>
							<td>{{ $penggunakategori->id_pengguna_kategori }}</td>
							<td>{{ $penggunakategori->pengguna_kategori }}</td>
							<td><button class="btn btn-flat btn-success btn-xs get penggunakategori" type="button" data-id="{{ $penggunakategori->id_pengguna_kategori }}" data-category="{{ $penggunakategori->pengguna_kategori }}"><i class="fa fa-check"></i></button></td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $penggunakategoris->links() }}
			</div>
			<div class="modal-footer">
				<small><i>Silahkan pilih pengguna kategori dengan menekan tombol cek.</i></small>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("button.get.penggunakategori").on("click", function(){
			$("#textIDKategori").val($(this).data("id"));
			$("#textKategori").val($(this).data("category"));

			$("#modalSearchPenggunaKategori").modal("hide");
		});
	});
</script>