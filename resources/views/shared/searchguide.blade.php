<div class="modal fade" id="modalSearchGuide">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
				<h4 class="modal-title">Data Guide</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="box">
							<div class="box-header with-border">Cari Guide</div>
							<div class="box-body">
								<form class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="textSearchGuide">Cari</label>
										<div class="col-sm-10">
											<input class="form-control" id="textSearchGuide" type="text" placeholder="Nama Guide">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="box">
							<div class="box-header with-border">Daftar Informasi</div>
							<div class="box-body no-padding">
								<div class="table-responsive pre-scrollable">
									<table class="table table-bordered table-hover">
										<thead>
											<th>ID</th>
											<th>Guide</th>
											<th>Referensi</th>
										</thead>
										<tbody class="rows guide">
											@foreach($guides as $guide)
											<tr>
												<td>{{ $guide->id_guide }}</td>
												<td>{{ $guide->nama }}</td>
												<td>{{ $guide->id_referensi }}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<small><i>Silahkan pilih guide dengan menekan baris pada guide bersangkutan.</i></small>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var timeoutId;

	$(document).ready(function(){
		/* Choose Search Guide */
		$("tbody.rows.guide").on("click", "tr", function(e){
			var id = $(this).find("td").eq(0).text();
			var guide = $(this).find("td").eq(1).text();

			$("#selectJumlah").removeAttr('disabled');
			$('#plc-box-free').fadeIn();
			$("#textIDGuide").val(id);
			$("#textGuide").val(guide);

			$("#modalSearchGuide").modal("hide");
			$("#buttonSearch").removeAttr("disabled");
		});

		/* Search Guide */
		$("#textSearchGuide").bind("keyup", function(e){
			if (timeoutId){
            	clearTimeout(timeoutId);
        	}

        	var id = $(this).val();

			if($(this).val() == ""){
				id = "all";
			}

			timeoutId = setTimeout(function(){ 

				$.ajax({
					type:"GET",
					url:"/ticketing/public/search/guide/" + id,
					dataType:"json",
					success:function(response){
						var row = "";

						$("tbody.rows.guide").empty();

						$.each(response, function(index, value){
							row = row + "<tr>" +
										"<td>" + value.id_guide + "</td>" +
										"<td>" + value.nama + "</td>" +
										"<td>" + value.id_referensi + "</td>" +
										"</tr>";
						});

						$("tbody.rows.guide").append(row);
					}
				});

				return false; 
			}, 500);	
		});
	});
</script>