<div class="modal fade" id="modalProfile">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
				<h4 class="modal-title">Profile</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="formProfile">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="textProfileNamaLengkap">Nama</label>
						<div class="col-sm-10">
							<input class="form-control" id="textProfileNamaLengkap" type="text" placeholder="Nama Lengkap" disabled>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="textProfilePasswordBaru">Password</label>
						<div class="col-sm-10">
							<input class="form-control" id="textProfilePasswordBaru" type="password" placeholder="Password Baru">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="textProfileKonfirmasiPassword">Konfirmasi</label>
						<div class="col-sm-10">
							<input class="form-control" id="textProfileKonfirmasiPassword" type="password" placeholder="Konfirmasi Password">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Keluar</button>
				<button class="btn btn-flat btn-primary save profile" id="buttonSaveProfile" type="button" data-value="store" disabled>Simpan</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		/* Modal Profile When Closing */
		$("#modalProfile").on("hidden.bs.modal", function(){
			$("#formProfile").trigger('reset');
			$("#buttonSaveProfile").data("value", "store");
		});

		/* Show Detail Profile */
		$("#modalProfile").on("shown.bs.modal", function(){
			var id = $("span.session.pengguna").text();

			$.ajax({
				type:"GET",
				url:"/ticketing/public/profile/" + id,
				dataType:"json",
				success:function(response){
					console.log(response);
					$("#modalProfile").modal();

					$.each(response, function(index, value){
						$("#textProfileNamaLengkap").val(value.nama_lengkap);
					});

					$("#buttonSaveProfile").data("value", "update");
				}
			});
		});

		/* Update Transaksi Tiket */
		$("button.save.profile").on("click", function(){
			var id = $("span.session.pengguna").text();
			var password = $("#textProfilePasswordBaru").val();
			var konfirmasi = $("#textProfileKonfirmasiPassword").val();

			if(password == konfirmasi)
			{
				$.ajax({
					type:"PUT",
					url:"/ticketing/public/pengguna/profile/" + id,
					data:{
						nama:$("#textProfileNamaLengkap").val(),
						password:$("#textProfilePasswordBaru").val()
					},
					success:function(response){
						toastr.success("Sukses Menjalankan Perintah");
						setTimeout(function(){ window.location.href = location.href; }, 3000);
					}
				});
			}

			else
			{
				toastr.info("Informasi Sesuaikan Konfirmasi Password");
			}
		});

		$("#textProfileKonfirmasiPassword").on("keyup", function(){
			var password = $("#textProfilePasswordBaru").val();

			if(password == $(this).val() && password.length > 0){
				$("button.save.profile").prop("disabled", false);
			}

			else{
				$("button.save.profile").prop("disabled", true);
			}
		});
	});
</script>