@extends('layouts.app')

@section('content')
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<form class="form-horizontal" action="{{ url('laporanlog/excel') }}" method="post">
						{{ csrf_field() }}
						<div class="box-header with-border">Laporan Harian Log</div>
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTanggalMulai">Tanggal Mulai</label>
								<div class="col-sm-4">
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control" id="textTanggalMulai" name="mulai" type="text" placeholder="Tanggal Mulai">
									</div>
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textWaktuMulai" name="waktuMulai" type="text" placeholder="Waktu Awal">
									</div>
								</div>
								<label class="col-sm-2 control-label" for="textTanggalAkhir">Tanggal Akhir</label>
								<div class="col-sm-4">
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control" id="textTanggalAkhir" name="selesai" type="text" placeholder="Tanggal Akhir">
									</div>
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textWaktuAkhir" name="waktuAkhir" type="text" placeholder="Waktu Akhir">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDPengguna">Pengguna</label>
								<div class="col-sm-4 hide">
									<input class="form-control" id="textIDPengguna" name="pengguna" type="text" placeholder="ID Pengguna" readonly>
								</div>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textPengguna" type="text" placeholder="Pengguna" disabled>
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalSearchPengguna"><i class="fa fa-plus"></i></button>
											<button class="btn btn-danger reset laporanlog" type="button"><i class="fa fa-close"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer text-right">
							<button class="btn btn-flat btn-primary search laporanlog" id="buttonSearch" type="button" data-value="store"><i class="fa fa-search"></i> Cari</button>
							<button class="btn btn-flat btn-success export laporanlog" id="buttonExport" type="submit" data-value="store"><i class="fa fa-download"></i> Ekspor</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Laporan Harian Log</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th>ID</th>
									<th>Pengguna</th>
									<th>Aktivitas</th>
									<th>Waktu</th>
								</thead>
								<tbody class="rows laporanlog">
									<tr><td colspan="5"><small><i>Data tidak ditemukan.</i></small></td></tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer">
						<small><i>Anda juga dapat mengekpor hasil pencarian dalam format excel.</i></small>
					</div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				var now = new Date(); 
                var year  = now.getFullYear();
                var month  = now.getMonth() + 1 + ""; if(month.length == 1)	month = "0" + month;
                var day  = now.getDate() + ""; if(day.length == 1) day = "0" + day;

                $("#textTanggalMulai").val('01-' + month + '-' + year);
                $("#textTanggalAkhir").val(day + '-' + month + '-' + year);
				
				/* Show Date Picker */
				$("#textTanggalMulai, #textTanggalAkhir").datepicker({
					language: 'en',
					dateFormat: 'dd-mm-yyyy',
					maxDate: new Date(),
					setDate: '21-07-2016'
				});

				$("#textWaktuMulai, #textWaktuAkhir").timepicker({
					showMeridian:false
				});

				$("#textWaktuMulai").val('00:00');
				$("#textWaktuAkhir").val('23:59');

				$("button.reset.laporanlog").on("click", function(){
					$("#textIDPengguna").val("");
					$("#textPengguna").val("");
				});

				$("#buttonSearch").on("click", function(){
					var tmp = $("#textTanggalMulai").val().split('-');
					var wkt = $("#textWaktuMulai").val();

					if(wkt.length == 4)
					{
						wkt = '0' + wkt;
					}

					var waktuMulai = tmp[2] + '-' + tmp[1] + '-' + tmp[0] + ' ' + wkt + ':00';
					
					var tmp = $("#textTanggalAkhir").val().split('-');
					var wkt = $("#textWaktuAkhir").val();

					if(wkt.length == 4)
					{
						wkt = '0' + wkt;
					}

					var waktuAkhir = tmp[2] + '-' + tmp[1] + '-' + tmp[0] + ' ' + $("#textWaktuAkhir").val() + ':00';

					$.ajax({
						type:"POST",
						url:"/ticketing/public/laporanlog",
						data:{
							mulai:waktuMulai,
							selesai:waktuAkhir,
							pengguna:$("#textIDPengguna").val()
						},
						success:function(response){
							var row = "";

							if(response.length == 0){
								row = "<tr><td colspan='5'><small><i>Data tidak ditemukan.</i></small></td></tr>";
							}

							else{
								var no = 1;

								$.each(response, function(i, value){
									row = row + "<tr>" +
												"<td>" + no++ + "</td>" +
												"<td>" + value.id_log + "</td>" +
												"<td>" + value.nama_lengkap + "</td>" +
												"<td>" + value.aktivitas + "</td>" +
												"<td>" + value.wkt_log + "</td>" +
												"</tr>";
								});
							}

							$("tbody.rows.laporanlog").html(row);
						}
					});
				});
			});
		</script>
	</section>
	@include('shared.searchpengguna')
@endsection