@extends('layouts.app')

@section('content')
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<form class="form-horizontal" action="{{ url('laporantransaksikomisiguide/excel') }}" method="post">
						{{ csrf_field() }}
						<div class="box-header with-border">Laporan Harian Transaksi Komisi Guide</div>
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTanggalMulai">Tanggal Mulai</label>
								<div class="col-sm-4">
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control" id="textTanggalMulai" name="mulai" type="text" placeholder="Tanggal Mulai">
									</div>
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textWaktuMulai" name="waktuMulai" type="text" placeholder="Waktu Awal">
									</div>
								</div>
								<label class="col-sm-2 control-label" for="textTanggalAkhir">Tanggal Akhir</label>
								<div class="col-sm-4">
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control" id="textTanggalAkhir" name="selesai" type="text" placeholder="Tanggal Akhir">
									</div>
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textWaktuAkhir" name="waktuAkhir" type="text" placeholder="Waktu Akhir">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDGuide">Guide</label>
								<div class="col-sm-4 hide">
									<input class="form-control" id="textIDGuide" name="guide" type="text" placeholder="ID Guide" readonly>
								</div>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textGuide" type="text" placeholder="Guide" disabled>
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalSearchGuide"><i class="fa fa-plus"></i></button>
											<button class="btn btn-danger reset laporantransaksikomisiguide" type="button"><i class="fa fa-close"></i></button>
										</span>
									</div>
								</div>
								<label class="col-sm-2 control-label" for="selectPetugasKomisi">Petugas Komisi</label>
								<div class="col-sm-4">
									<select class="form-control" id="selectPetugasKomisi" name="komisi">
										<option value="">Semua Petugas Komisi</option>
										@foreach($komisis as $komisi)
											<option value="{{ $komisi->id_pengguna }}">{{ $komisi->nama_pengguna }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="box-footer text-right">
							<button class="btn btn-flat btn-primary search laporantransaksikomisiguide" id="buttonSearch" type="button" data-value="store"><i class="fa fa-search"></i> Cari</button>
							<button class="btn btn-flat btn-success export laporantransaksikomisiguide" id="buttonExport" type="submit" data-value="store"><i class="fa fa-download"></i> Ekspor</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Laporan Harian Transaksi Komisi Guide</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th class="hide">ID Guide</th>
									<th>ID</th>
									<th>Guide</th>
									<th>Referensi</th>
									<th>Travel</th>
									<th>Nilai</th>
									<th>Waktu</th>
									<th>Jenis</th>
									<th>Detail</th>
								</thead>
								<tfoot>
									<tr>
										<td colspan="5"><b>Total</b></td>
										<td class="price">0</td>
										<td colspan="3"></td>
									</tr>
								</tfoot>
								<tbody class="rows laporantransaksikomisiguide">
									<tr><td colspan="9"><small><i>Data tidak ditemukan.</i></small></td></tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer">
						<small><i>Anda juga dapat mengekpor hasil pencarian dalam format excel.</i></small>
					</div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modalDetailPendapatanGuide">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
						<h4 class="modal-title">Data Pendapatan Guide</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="box">
									<div class="box-header with-border">Data Guide</div>
									<div class="box-body">
										<form class="form-horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="textKodeGuide">Kode Guide</label>
												<div class="col-sm-4">
													<input class="form-control" id="textKodeGuide" type="text" placeholder="Kode Guide" disabled>
												</div>
												<label class="col-sm-2 control-label" for="textNamaGuide">Nama Guide</label>
												<div class="col-sm-4">
													<input class="form-control" id="textNamaGuide" type="text" placeholder="Nama Guide" disabled>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="textNamaTravel">Travel</label>
												<div class="col-sm-4">
													<input class="form-control" id="textNamaTravel" type="text" placeholder="Travel" disabled>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="box">
									<div class="box-header with-border">Daftar Informasi</div>
									<div class="box-body">
										<table class="table table-hover">
											<thead>
												<th>No.</th>
												<th>ID Transaksi</th>
												<th>Kategori Tiket</th>
												<th>Pendapatan</th>
												<th>Komisi Guide</th>
												<th>Komisi Travel</th>
												<th>Waktu Transaksi</th>
											</thead>
											<tbody class="rows detailpendapatanguide"></tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<small><i>Informasi detail dari pendapatan guide yang sudah dibayar.</i></small>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("td.price").autoNumeric("init", {aSign:'Rp '});
				$("td.discount").autoNumeric("init", {aSign:' %', pSign:'s'});

				var now = new Date();
				var year = now.getFullYear();
				var month = now.getMonth() + 1 + ""; if(month.length == 1) month = "0" + month;
				var day = now.getDate() + ""; if(day.length == 1) day = "0" + day;

				var waktuMulai = "";
				var waktuAkhir = "";

				$("#textTanggalMulai").val('01-' + month + '-' + year);
				$("#textTanggalAkhir").val(day + '-' + month + '-' + year);
				
				/* Show Date Picker */
				$("#textTanggalMulai, #textTanggalAkhir").datepicker({
					language: 'en',
					dateFormat: 'dd-mm-yyyy',
					maxDate: new Date(),
					setDate: '21-07-2016'
				});
				
				$("#textWaktuMulai, #textWaktuAkhir").timepicker({
					showMeridian:false
				});

				$("#textWaktuMulai").val('00:00');
				$("#textWaktuAkhir").val('23:59');

				$("button.reset.laporantransaksikomisiguide").on("click", function(){
					$("#textIDGuide").val("");
					$("#textGuide").val("");
				});

				$("#buttonSearch").on("click", function(){
					var tmp = $("#textTanggalMulai").val().split('-');
					var wkt = $("#textWaktuMulai").val();

					if(wkt.length == 4)
					{
						wkt = '0' + wkt;
					}

					waktuMulai = tmp[2] + '-' + tmp[1] + '-' + tmp[0] + ' ' + wkt + ':00';

					tmp = $("#textTanggalAkhir").val().split('-');
					wkt = $("#textWaktuAkhir").val();

					if(wkt.length == 4)
					{
						wkt = '0' + wkt;
					}

					waktuAkhir = tmp[2] + '-' + tmp[1] + '-' + tmp[0] + ' ' + $("#textWaktuAkhir").val() + ':00';

					$.ajax({
						type:"POST",
						url:"/ticketing/public/laporantransaksikomisiguide",
						data:{
							mulai:waktuMulai,
							selesai:waktuAkhir,
							guide:$("#textIDGuide").val(),
							komisi:$("#textPetugasKomisi").val()
						},
						success:function(response){
							var row = "";
							var total = 0;

							if(response.length == 0){
								row = "<tr><td colspan='9'><small><i>Data tidak ditemukan.</i></small></td></tr>";
							}

							else{
								var no = 1;

								$.each(response, function(i, value){
									total = total + parseInt(value.nilai_transaksi, 10);

									row = row + "<tr>" +
												"<td>" + no++ + "</td>" +
												"<td class='hide'>" + value.id_guide + "</td>" +
												"<td>" + value.id_transaksi_komisi + "</td>" +
												"<td>" + value.nama + "</td>" +
												"<td>" + (value.id_referensi == "" ? '-' : value.id_referensi) + "</td>" +
												"<td>" + (value.nama_travel == null ? '-' : value.nama_travel) + "</td>" +
												"<td class='price'>" + value.nilai_transaksi + "</td>" +
												"<td>" + value.wkt_transaksi + "</td>" +
												"<td>" + (value.status_transaksi == 0 ? 'Cash' : 'Bank Transfer') + "</td>" +
												"<td><i class='fa fa-eye'></i></td>" +
												"</tr>";
								});
							}

							$("tbody.rows.laporantransaksikomisiguide").html(row);
							$("td.price").autoNumeric("init", {aSign:'Rp '});
							$("tfoot").find("td").eq(1).autoNumeric("set", total);
						}
					});
				});

				/* Choose Detail Guide */
				$("tbody.rows.laporantransaksikomisiguide").on("click", "tr", function(e){
					var id = $(this).closest("tr").find("td").eq(1).text();
					var group = $(this).closest("tr").find("td").eq(2).text();
					var guide = $(this).closest("tr").find("td").eq(3).text();
					var travel = $(this).closest("tr").find("td").eq(5).text();

					$.ajax({
						type:"POST",
						url:"/ticketing/public/laporantransaksikomisiguide/komisi/" + id,
						data:{
							mulai:waktuMulai,
							selesai:waktuAkhir,
							guide:id,
							group:group,
							komisi:$("#textPetugasKomisi").val()
						},
						success:function(response){
							var row = "";
							var pendapatan = 0;
							var komisiGuide = 0;
							var komisiTravel = 0;
							var totalPendapatan = 0;
							var totalKomisiGuide = 0;
							var totalKomisiTravel = 0;

							$("#modalDetailPendapatanGuide").modal();

							$("#textKodeGuide").val(id);
							$("#textNamaGuide").val(guide);
							$("#textNamaTravel").val(travel);

							$("tbody.rows.detailpendapatanguide").empty();

							if(response.length == 0){
								row = "<tr><td colspan='7'><small><i>Data tidak ditemukan.</i></small></td></tr>";
							}

							else{
								$.each(response, function(index, value){
									if(value.nama_travel != null)
									{
										komisiTravel = ((value.komisi_guide * value.harga / 100) - ((value.komisi_guide * value.harga / 100) * (value.guide_komisi == null ? 0 : value.guide_komisi) / 100));
									}

									pendapatan = (value.komisi_guide * value.harga / 100);
									komisiGuide = ((value.komisi_guide * value.harga / 100) - ((value.komisi_guide * value.harga / 100) * (value.travel_komisi == null ? 0 : value.travel_komisi) / 100));

									totalPendapatan = totalPendapatan + parseInt(pendapatan, 10);
									totalKomisiGuide = totalKomisiGuide + parseInt(komisiGuide, 10);
									totalKomisiTravel = totalKomisiTravel + parseInt(komisiTravel, 10);

									var no = 1;

									row = row + "<tr>" +
												"<td>" + no++ + "</td>" +
												"<td>" + value.id_transaksi + "</td>" +
												"<td>" + value.kategori_tiket + "</td>" +
												"<td class='price'>" + pendapatan + "</td>" +
												"<td class='price'>" + komisiGuide + "</td>" +
												"<td class='price'>" + komisiTravel + "</td>" +
												"<td>" + value.wkt_transaksi + "</td>" +
												"</tr>";
								});
							}

							row = row + "<tr>"+
										"<td></td>" +
										"<td></td>" +
										"<td></td>" +
										"<td class='price text-bold'>" + totalPendapatan + "</td>" +
										"<td class='price text-bold'>" + totalKomisiGuide + "</td>" +
										"<td class='price text-bold'>" + totalKomisiTravel + "</td>" +
										"<td></td>" +
										"</tr>";

							$("tbody.rows.detailpendapatanguide").append(row);
							$("td.price").autoNumeric("init", {aSign:'Rp '});
						}
					});
				});
			});
		</script>
	</section>
	@include('shared.searchguide')
@endsection