@extends('layouts.app')

@section('content')
	<section class="content-header">
		<h1>Pendapatan Guide</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<form class="form-horizontal" action="{{ url('pendapatanguide/excel') }}" method="post">
						{{ csrf_field() }}
						<div class="box-header with-border">Temukan Pendapatan Guide</div>
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTanggalMulai">Tanggal Mulai</label>
								<div class="col-sm-4">
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control" id="textTanggalMulai" name="mulai" type="text" placeholder="Tanggal Mulai">
									</div>
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textWaktuMulai" name="waktuMulai" type="text" placeholder="Waktu Awal">
									</div>
								</div>
								<label class="col-sm-2 control-label" for="textTanggalAkhir">Tanggal Akhir</label>
								<div class="col-sm-4">
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control" id="textTanggalAkhir" name="selesai" type="text" placeholder="Tanggal Akhir">
									</div>
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textWaktuAkhir" name="waktuAkhir" type="text" placeholder="Waktu Akhir">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="selectStatus">Status</label>
								<div class="col-sm-4">
									<select class="form-control" id="selectStatus" name="status">
										<option value="">Semua Status</option>
										<option value="0">Belum Bayar</option>
										<option value="1">Sudah Bayar</option>
									</select>
								</div>
							</div>
						</div>
						<div class="box-footer text-right">
							<button class="btn btn-flat btn-primary search pendapatanguide" id="buttonSearch" type="button" data-value="store"><i class="fa fa-search"></i> Cari</button>
							<button class="btn btn-flat btn-success export pendapatanguide" id="buttonExport" type="submit" data-value="store"><i class="fa fa-download"></i> Ekspor</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Daftar Informasi</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th class="hide">ID</th>
									<th>Guide</th>
									<th>Referensi</th>
									<th>Travel</th>
									<th>Pendapatan</th>
									<th>Komisi Guide</th>
									<th>Komisi Travel</th>
									<th>Status</th>
									<th>&nbsp;</th>
								</thead>
								<tbody class="rows pendapatanguide">
									@if(count($guides) == 0)
									<tr>
										<td colspan="9"><small><i>Data tidak ditemukan.</i></small></td>
									</tr>
									@else
									@foreach($guides as $key=>$guide)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td class="hide">{{ $guide->id_guide }}</td>
										<td>{{ $guide->nama }}</td>
										<td>{{ $guide->id_referensi == "" ? '-' : $guide->id_referensi }}</td>
										<td>{{ $guide->nama_travel == null ? '-' : $guide->nama_travel }}</td>
										<td class="price">{{ $guide->pendapatan }}</td>
										<td class="price">{{ ($guide->pendapatan - ($guide->pendapatan * $guide->travel_komisi / 100)) }}</td>
										<td class="price">
										@if($guide->nama_travel == null)
										0
										@else
										{{ ($guide->pendapatan - ($guide->pendapatan * $guide->guide_komisi / 100)) }}
										@endif
										</td>
										<td>{{ $guide->status_pembayaran_komisi == 0 ? 'Belum Bayar' : 'Sudah Bayar' }}</td>
										<td></td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
							{{ $guides->links() }}
						</div>
					</div>
					<div class="box-footer"><small><i>Data pendapatan guide tidak dapat diubah.</i></small></div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				var now = new Date();
				var year = now.getFullYear();
				var month = now.getMonth() + 1 + ""; if(month.length == 1) month = "0" + month;
				var day = now.getDate() + ""; if(day.length == 1) day = "0" + day;

				$("#textTanggalMulai").val('01-' + month + '-' + year);
				$("#textTanggalAkhir").val(day + '-' + month + '-' + year);
				$("td.price").autoNumeric("init", {aSign:'Rp '});

				/* Show Date Picker */
				$("#textTanggalMulai, #textTanggalAkhir").datepicker({
					language: 'en',
					dateFormat: 'dd-mm-yyyy',
					maxDate: new Date(),
					setDate: '21-07-2016'
				});

				$("#textWaktuMulai, #textWaktuAkhir").timepicker({
					showMeridian:false
				});

				$("#textWaktuMulai").val('00:00');
				$("#textWaktuAkhir").val('23:59');
			});

			$("#buttonSearch").on("click", function(){
				if($("#selectStatus").val() != ""){
					$("ul.pagination").prop("class", "hide");

					var tmp = $("#textTanggalMulai").val().split('-');
					var wkt = $("#textWaktuMulai").val();

					if(wkt.length == 4)
					{
						wkt = '0' + wkt;
					}

					var waktuMulai = tmp[2] + '-' + tmp[1] + '-' + tmp[0] +' ' + wkt + ':00';

					var tmp = $("#textTanggalAkhir").val().split('-');
					var wkt = $("#textWaktuAkhir").val();

					if(wkt.length == 4)
					{
						wkt = '0' + wkt;
					}

					var waktuAkhir = tmp[2] + '-' + tmp[1] + '-' + tmp[0] + ' ' + $("#textWaktuAkhir").val() + ':00';

					$.ajax({
						type:"POST",
						url:"/ticketing/public/pendapatanguide",
						data:{
							mulai:waktuMulai,
							selesai:waktuAkhir,
							status:$("#selectStatus").val()
						},
						success:function(response){
							var row = "";

							if(response.length == 0){
								row = "<tr><td colspan='9'><small><i>Data tidak ditemukan.</i></small></td></tr>";
							}

							else
							{
								var komisi = 0;
								var no = 1;

								$.each(response, function(i, value){
									if(value.nama_travel != null)
									{
										komisi = (value.pendapatan - (value.pendapatan * (value.guide_komisi == null ? 0 : value.guide_komisi) / 100));
									}

									if(value.pendapatan != "0")
									{
									row = row + "<tr>" +
												"<td>" + no++ + "</td>" +
												"<td class='hide'>" + value.id_guide + "</td>" +
												"<td>" + value.nama + "</td>" +
												"<td>" + (value.id_referensi == "" ? '-' : value.id_referensi) + "</td>" +
												"<td>" + (value.nama_travel == null ? '-' : value.nama_travel) + "</td>" +
												"<td class='price'>" + value.pendapatan + "</td>" +
												"<td class='price'>" + (value.pendapatan - (value.pendapatan * (value.travel_komisi == null ? 0 : value.travel_komisi) / 100)) + "</td>" +
												"<td class='price'>" + komisi + "</td>" +
												"<td>" + (value.status_pembayaran_komisi == 0 ? 'Belum Bayar' : 'Sudah Bayar') + "</td>" +
												"<td></td>" +
												"</tr>";
									}
								});
							}

							$("tbody.rows.pendapatanguide").html(row);
							$("td.price").autoNumeric("init", {aSign:'Rp '});
						}
					});
				}

				else
				{
					window.location.href = location.href;
				}
			});
		</script>
	</section>
@endsection