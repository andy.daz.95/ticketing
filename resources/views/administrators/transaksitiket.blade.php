@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ URL::asset('theme/plugins/calculator/jquery.calculator.css') }}">
<script src="{{ URL::asset('theme/plugins/calculator/jquery.plugin.js') }}"></script>
<script src="{{ URL::asset('theme/plugins/calculator/jquery.calculator.js') }}"></script>

	<section class="content-header">
		<h1>Transaksi Tiket</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class='col-sm-8'>
				<div class="box">
					<div class="box-header with-border">Guide</div>
					<div class="box-body">
						<form class="form-horizontal guide">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDGuide">Guide</label>
								<div class="col-sm-4">
									<input class="form-control input-lg" id="textIDGuide" type="text" placeholder="ID Referensi">
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<input class="form-control input-lg" id="textGuide" type="text" placeholder="Guide" disabled>
										<span class="input-group-btn">
											<button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#modalSearchGuide"><i class="fa fa-plus"></i></button>
											<button class="btn btn-danger btn-lg reset transaksitiket" type="button"><i class="fa fa-close"></i></button>
										</span>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="box-footer">
						<small class="text-danger"><i>Masukkan id referensi atau kosongkan jika tanpa guide.</i></small>
						<div id="plc-free" class="pull-right"></div>
					</div>
				</div>
				<div class="box" id="plc-box-free" style="display: none;">
					<div class="box-header with-border">Jumlah Tiket Free</div>
					<div class="box-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textJumlah">Jumlah</label>
								<div class="col-sm-10">
									<select class="form-control" id="selectJumlah" disabled>
										<option value="0">0</option>
										<option value="1">1</option>
										<option value="2">2</option>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>

				<input type="hidden" value="" id="setTrigger" />
				<input type="hidden" value="<?php echo count($kategoritikets);?>" id="jmlKategori" />
				<div class="box">
					<div class="box-header with-border">Kategori Tiket</div>
					<div class="box-body table-responsive">
						<table class="table table-striped">
							<thead>
								<th>Kategori</th>
								<th>Harga</th>
								<th>Diskon</th>
								<th>Jumlah</th>
								<th>Total</th>
								<th></th>
								<th class="hide">ID</th>
								<th class="hide">Komisi</th>
							</thead>
							<tfoot>
								<tr>
									<td colspan="3">Total</td>
									<td class="text-center" id="jmlBeli">0</td>
									<td class="price" id="jmlBeliTotal">0</td>
									<td></td>
									<td class="hide" colspan="2"></td>
								</tr>
							</tfoot>
							<tbody class="rows transaksitiket">
								<?php
									$no=0;
								?>
								@foreach($kategoritikets as $kategoritiket)
								<tr>
									<td>{{ $kategoritiket->kategori_tiket }}</td>
									<td class="price" id="harga<?php echo $no;?>">{{ $kategoritiket->harga }}</td>
									<td class="discount" id="diskon<?php echo $no;?>">{{ $kategoritiket->diskon }}</td>
									<td width="150px" id="jumlah<?php echo $no;?>">
										<div class="form-group" style="margin-top: 15px;">
											<div class="input-group">
										    	<span style="background: #00a65a; border: 1px solid #00a65a;" class="input-group-addon plus transaksitiket"><i style="color: #FFFFFF;" class="fa fa-plus"></i></span>
										    	<!--<span style="background: #FFFFFF; border: 1px solid #c7c7c7;" class="input-group-addon count countTicket" id="countJml<?php echo $no;?>">0</span>-->
										    	<input readonly="readonly" class="form-control count countTicket txtCountTicket" style="text-align: center;" type="text" value="0" id="countJml<?php echo $no;?>">
										    	<span style="background: #dd4b39; border: 1px solid #dd4b39;" class="input-group-addon minus transaksitiket"><i style="color: #FFFFFF;" class="fa fa-minus"></i></span>
											</div>
										</div>
										
									</td>
									<td class="price" id="total<?php echo $no;?>">0</td>
									<td>
										<button style="margin-right: 10px;" class="btn btn-flat btn-danger btn-sm clear transaksitiket" type="button" data-id="{{ $kategoritiket->id_tiket }}"><i class="fa fa-close"></i></button>
									</td>
									<td class="hide">{{ $kategoritiket->id_tiket }}</td>
									<td class="hide">{{ $kategoritiket->komisi_guide }}</td>
								</tr>
								<?php
									$no++;
								?>
								@endforeach
							</tbody>
						</table>
						{{ $kategoritikets->links() }}
					</div>
					<div class="box-footer"></div>
				</div>
			</div>
			<div class='col-sm-4'>
				<div class="box">
					<div class="box-header with-border">Transaksi Tiket</div>
					<div class="box-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTotal">Total</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input class="form-control input-lg" id="textTotal" type="text" placeholder="Total" disabled>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textBayar">Bayar</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input style="background: #FFFFFF;" class="form-control input-lg" id="textBayar" type="text" placeholder="Bayar" readonly="readonly">
									</div>
									<small class="text-primary"><i>Formulir bayar wajib diisi.</i></small>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textKembali">Kembali</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input class="form-control input-lg" id="textKembali" type="text" placeholder="Kembali" disabled>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="radioJenis">Status</label>
								<div class="col-sm-10">
									<label class="radio-inline"><input name="jenis" type="radio" value="cash" checked> Cash</label>
									<label class="radio-inline"><input name="jenis" type="radio" value="debet"> Credit or Debit Card</label>
								</div>
							</div>
						</form>
					</div>
					<div class="box-footer">
						<button class="btn btn-flat btn-primary btn-lg save transaksitiket" id="buttonSave" type="button" data-value="store" disabled><i class="fa fa-save"></i> Pay</button>
						<img style="margin:0 15px;" src="{{ URL::asset('images/loading.gif') }}" id="loader">
						<button class="btn btn-flat btn-warning btn-lg pull-right" data-toggle="modal" data-target="#myModalPrintAgain" type="button"><i class="fa fa-print"></i> Print</button>
					</div>
				</div>
				<div class="box box-success collapsed-box">
					<div class="box-header with-border">
						<h3 class="box-title">Number Pad</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
				        <div class="col-sm-12 phone">
				            <div class="row1">
				                <div style="width: 350px;margin: auto;">
				                    <div class="num-pad">
					                    <div class="span4">
					                        <div class="num" data='7'>
					                            <div class="txt">
					                                7 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="span4">
					                        <div class="num" data='8'>
					                            <div class="txt">
					                                8 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="span4">
					                        <div class="num" data='9'>
					                            <div class="txt">
					                                9 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>							                    
					                    <div class="span4">
					                        <div class="num" data='4'>
					                            <div class="txt">
					                                4 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div><div class="span4">
					                        <div class="num" data='5'>
					                            <div class="txt">
					                                5 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="span4">
					                        <div class="num"  data='6'>
					                            <div class="txt">
					                                6 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>						                    
					                    <div class="span4">
					                        <div class="num" data='1'>
					                            <div class="txt">
					                                1 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="span4">
					                        <div class="num" data='2'>
					                            <div class="txt">
					                                2 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="span4">
					                        <div class="num" data='3'>
					                            <div class="txt">
					                                3 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>	
					                    <div class="span4">
					                        <div class="num" data='0'>
					                            <div class="txt">
					                                0 <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>
				                    	<div class="span4">
					                        <div class="num" id="del" data='del'>
					                            <div class="txt">
					                                DEL <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="span4">
					                        <div class="num" id="c" data='c'>
					                            <div class="txt">
					                                C <span class="small"></span>
					                            </div>
					                        </div>
					                    </div>
				                    </div>
				                </div>
				            </div>
				        </div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Print Again-->
		<div id="myModalPrintAgain" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="text">Jumlah Bayar</label>
							<input type="text" class="form-control input-lg" id="bayarPrintAgain">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Tutup</button>
						<button type="button" class="btn btn-success btn-flat" id="buttonPrintAgain"><i class="fa fa-print"></i> Print</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Debet -->
		<div class="modal fade" id="modalDebet">
			<div class="modal-dialog">
				<div class="modal-content">
					<form class="form-horizontal" id="formDebet">
						<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
							<h4 class="modal-title">Jenis Pembayaran</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTotalAbsolute"><b class="text-danger">*</b> Total</label>
								<div class="col-sm-10">
									<input class="form-control" id="textTotalAbsolute" type="text" placeholder="Total" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textNoKartu"><b class="text-danger">*</b> No. Kartu</label>
								<div class="col-sm-10">
									<input class="form-control" id="textNoKartu" name="kartu" type="text" placeholder="No. Kartu">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textExpirationMM"><b class="text-danger">*</b> Exp MM</label>
								<div class="col-sm-4">
									<input class="form-control" id="textExpirationMM" name="mm" type="text" placeholder="Expired MM">
								</div>
								<label class="col-sm-2 control-label" for="textExpirationYY"><b class="text-danger">*</b> Exp YY</label>
								<div class="col-sm-4">
									<input class="form-control" id="textExpirationYY" name="yy" type="text" placeholder="Expired YY">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textSecurity"><b class="text-danger">*</b> Security</label>
								<div class="col-sm-4">
									<input class="form-control" id="textSecurity" name="security" type="text" placeholder="Security Kode">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Keluar</button>
							<button class="btn btn-flat btn-primary save debet" id="buttonSaveDebet" type="submit" data-value="store" disabled>Cash</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			  	$('#textBayar').attr('readonly');
			  	$('.txtCountTicket').attr('readonly');
			}

			else
			{
				$('#textBayar').removeAttr('readonly');
				$('.txtCountTicket').removeAttr('readonly');
			}

			<?php
				$no = 0;
				foreach ($kategoritikets as $key)
				{
				}
			?>

			/* Global Variabel */
			var i = 0;
			var j = 0;
			var bmp = "";
			var all = [];
			var triggerBox="";
			
			function set_total()
			{
				var jml = parseInt($('#jmlKategori').val());
				var totalHarga = 0;
				var jmlBeli = 0;
				var jmlBeliTotal = 0;

				for (var a = 0; a < jml; a++) 
				{
					jmlBeli+=parseInt($('#countJml' + a).text());
	    			jmlBeliTotal+=parseInt($('#total' + a).autoNumeric('get'), 10);
				}

				$('#jmlBeli').text(jmlBeli);
				$('#jmlBeliTotal').autoNumeric('set', jmlBeliTotal);

				i = jmlBeli;
				j = jmlBeliTotal;

				$('#textTotal').autoNumeric('set', jmlBeliTotal);
			}

			function clearGuide()
			{
				$('#textGuide').val("");
				$('#textIDGuide').val("");
			}

			$(document).ready(function(){
				$("#bayarPrintAgain").autoNumeric("init");
				$('#buttonPrintAgain').click(function(){
					var invoice = "";

					$.ajax({
						type:"POST",
						url:"/ticketing/public/transaksitiket/lastinvoice",
						async:false,
						data:{
							referensi:''
						},
						success:function(response){
							id = response;
							invoice=response[0].invoice;
						}
					});

					if(invoice != "")
					{
						var result = [];
						$.ajax({
							type:"POST",
							url:"/ticketing/public/transaksitiket/datalastinvoice",
							async:false,
							data:{
								referensi:invoice
							},
							success:function(response){
								tmpBmp = response;
								for (var i = 0; i < response.length; i++) 
								{
								    result.push(response[i].id_transaksi+';'+parseInt($('#bayarPrintAgain').autoNumeric("get"), 10)+';'+$('#referensiStaf').val()+';{\\kategori_tiket\\:\\"'+response[i].kategori_tiket+'\\",\\"harga\\":'+response[i].harga+',\\"diskon\\":'+response[i].diskon+'};'+response[i].invoice);
								}
							}
						});

						$.ajax({
							type:"GET",
							url:"http://<?php echo $_SERVER['REMOTE_ADDR'];?>/ticketing/public/print/ToBmp.php",
							dataType: 'jsonp',
							data:{
								bmp:result
							},
							success:function(response){
								console.log(result);
								alert("");
							}
						});
						toastr.success("Sukses Menjalankan Perintah");
						setTimeout(function(){ window.location.href = location.href; }, 3000);
					}
				});

				$("#modalDebet").on("hidden.bs.modal", function(){
					$("input[value='cash']").prop("checked", true);
				});

				$('.clear-btn').click(function(){
					var data_id = $(this).attr('data-id');

					var jmlBeli = parseInt($('#jumlah' + data_id).text());
					var hargaBeli = parseInt($('#total' + data_id).text());

					var totalJml = parseInt($('#jmlBeli').text());
					var totalHarga = parseInt($('#jmlBeliTotal').text());
					
					$('#jumlah' + data_id).html('0');
	    			$('#total' + data_id).html('0');

	    			$('#jmlBeli').html((totalJml - jmlBeli));
					$('#jmlBeliTotal').html((totalHarga - hargaBeli));
					$("#textTotal").autoNumeric("set", (totalHarga - hargaBeli));
				});

				$("#textTotal").autoNumeric("init");
				$("#textBayar").autoNumeric("init");
				$("#textKembali").autoNumeric("init");
				$("input.count").autoNumeric("init", {vMin:0, vMax:1000});
				$("td.price").autoNumeric("init", {aSign:'Rp '});
				$("td.discount").autoNumeric("init", {aSign:' %', pSign:'s'});

				$('.countTicket').click(function(){
					$('.countTicket').css('background', '#FFFFFF');
					$("#textBayar").css('background', '#FFFFFF');
					$(this).css('background', '#cbffe7');

					triggerBox = $(this).attr('id');
				});

				$("#textBayar").on("focus", function(){
					$(this).select();
					$('.countTicket').css('background', '#FFFFFF');
					$(this).css('background', '#cbffe7');

					triggerBox = "textBayar";
				});

				$("input[name='jenis']").on("click", function(){
					var total = parseInt($("#textTotal").autoNumeric("get"), 10);
					var bayar = parseInt($("#textBayar").autoNumeric("get"), 10);

					if($(this).val() == "debet"){
						$("#modalDebet").modal();
						$("#buttonSave").prop("disabled", true);
						$("#textTotalAbsolute").val($("#textTotal").val());

						if(bayar >= total){
							$("#buttonSaveDebet").prop("disabled", false);
						}
					}

					else{
						$("#modalDebet").modal("hide");

						if(bayar >= total){
							$("#buttonSave").prop("disabled", false);
						}
					}
				});

				$('.num').click(function(){
					if(triggerBox != "")
					{
						if(triggerBox == "textBayar")
						{
							if($(this).attr('data') == "del")
							{
								var jmlAwal = $('#' + triggerBox).autoNumeric('get');

								if(jmlAwal.length > 1)
								{								
									$('#' + triggerBox).autoNumeric("set", jmlAwal.substring(0, jmlAwal.length - 1));
								}

								else
								{
									$('#' + triggerBox).autoNumeric("set", "0");
									$('#textKembali').autoNumeric("set", "0");
								}
							}

							else if($(this).attr('data') == "c")
							{
								$('#' + triggerBox).autoNumeric("set", "0");
								$('#textKembali').autoNumeric("set", "0");
							}

							else
							{
								var jmlAwal = $('#' + triggerBox).autoNumeric('get');

								if(jmlAwal == "0" || jmlAwal == "")
								{
									jmlAwal = "";
								}

								else
								{
									jmlAwal = jmlAwal + "";
								}

								var valNumpad = parseInt((jmlAwal + $(this).attr('data')));
								$('#' + triggerBox).autoNumeric("set", valNumpad);
							}

							var total = parseInt($("#textTotal").autoNumeric("get"), 10);
							var bayar = parseInt($("#textBayar").autoNumeric("get"), 10);
		
							if(bayar >= total)
							{
								$("#textKembali").autoNumeric("set", (bayar - total));
								$("#buttonSave").prop('disabled', false);
								$("#buttonSaveDebet").prop('disabled', false);
							}
		
							else
							{
								$("#buttonSave").prop('disabled', true);
								$("#buttonSaveDebet").prop('disabled', true);
							}
						}

						else
						{
							if($(this).attr('data') == "del")
							{
								var jmlAwal = $('#' + triggerBox).val();

								if(jmlAwal.length > 1)
								{
									jmlAwal = parseInt(jmlAwal.slice(0, -1));
								}

								else
								{
									jmlAwal = parseInt("0");
								}
								
								var valNumpad = parseInt(jmlAwal);

								$('#' + triggerBox).val(valNumpad);

								var jml = parseFloat(valNumpad);
				    			var diskon = parseFloat($('#diskon' + triggerBox.slice(-1)).text());
				    			var harga = parseFloat($('#harga' + triggerBox.slice(-1)).autoNumeric("get"), 10);
								
								var getDiskon = (harga * diskon) / 100;
								var total = (harga - getDiskon) * jml;

				    			$('#total' + triggerBox.slice(-1)).autoNumeric("set", total);
				    			set_total();
							}

							else if($(this).attr('data') == "c")
							{
								var valNumpad = parseInt("0");

								$('#' + triggerBox).val(valNumpad);

								var jml = parseFloat(valNumpad);
				    			var diskon = parseFloat($('#diskon' + triggerBox.slice(-1)).text());
				    			var harga = parseFloat($('#harga' + triggerBox.slice(-1)).autoNumeric("get"), 10);

								var getDiskon = (harga * diskon) / 100;
								var total = (harga - getDiskon) * jml;

				    			$('#total' + triggerBox.slice(-1)).autoNumeric("set", total);
				    			set_total();
							}

							else
							{
								if(triggerBox != "")
								{
									var jmlAwal = parseInt($('#' + triggerBox).val());

									if(jmlAwal == 0)
									{
										jmlAwal = "";
									}

									else
									{
										jmlAwal = jmlAwal + "";
									}

									var valNumpad = parseInt((jmlAwal + $(this).attr('data')));

									if(parseInt(valNumpad) <= 999)
									{
										$('#' + triggerBox).val(valNumpad);

										var jml = parseFloat(valNumpad);
						    			var diskon = parseFloat($('#diskon' + triggerBox.slice(-1)).text());
						    			var harga = parseFloat($('#harga' + triggerBox.slice(-1)).autoNumeric("get"), 10);

										var getDiskon = (harga * diskon) / 100;
										var total = (harga - getDiskon) * jml;
										
						    			$('#total' + triggerBox.slice(-1)).autoNumeric("set", total);
						    			set_total();
									}
								}
							}
						}
					}
				});

				$("#formDebet").validate({
					rules: {
						kartu:{
							required:true,
							pattern:/^([0-9]){1,30}$/
						},
						mm:{
							required:true,
							pattern:/^([0-9]){1,2}$/
						},
						yy:{
							required:true,
							pattern:/^([0-9]){1,2}$/
						},
						security:{
							required:true,
							pattern:/^([0-9]){1,15}$/
						}
					},
					messages: {
						kartu:{
							required:"Silahkan masukkan no. kartu",
							pattern:"Silahkan masukkan angka"
						},
						mm:{
							required:"Silahkan masukkan bulan",
							pattern:"Silahkan masukkan angka"
						},
						yy:{
							required:"Silahkan masukkan tahun",
							pattern:"Silahkan masukkan angka"
						},
						security:{
							required:"Silahkan masukkan security",
							pattern:"Silahkan masukkan angka"
						},
					},
					submitHandler: function(form) {
						var totalAwal = $("#jmlBeli").text();
						
						if(totalAwal > 50)
						{
							bootbox.alert({
								size:"small",
								title:"Konfirmasi",
								message:"Jumlah Pembelian Lebih Dari 50"
							});
						}
						
						else
						{
							var id = $("#textIDGuide").val();
							var guide = $("#textGuide").val();

							$("#buttonSaveDebet").prop('disabled', true);

							if(id != "" && guide == ""){
								$.ajax({
									type:"POST",
									url:"/ticketing/public/guide/store",
									async:false,
									data:{
										referensi:$("#textIDGuide").val()
									},
									success:function(response){
										id = response;
									}
								});
							}

							if($("#textTotal").autoNumeric("get") >= 0)
							{
								$("tbody.rows.transaksitiket tr").each(function(index){
									var jumlah = $(this).closest("tr").find("td input").val();

									for(var i = 0; i < parseInt(jumlah, 10); i++){
										all.push([
											$(this).find("td").eq(6).text(), /* Tiket */
											$("span.session.pengguna").text(), /* Penjual */
											$(this).find("td").eq(1).autoNumeric("get"), /* Harga */
											$(this).find("td").eq(2).autoNumeric("get"), /* Diskon */
											$(this).find("td").eq(7).text(), /* Komisi */
											1, /* Status */
											id, /* Guide */
											$(this).find("td").eq(0).text(), /* Kategori */
											$(this).closest("tr").find("td input").val(), /* Jumlah */
											$(this).find("td").eq(4).autoNumeric("get"), /* Total */
											0, /* Scan */
											$("#textBayar").autoNumeric("get"), /* Bayar */
											1, /* Jenis Bayar */
											$("#textNoKartu").val(), /* No Kartu */
											$("#textExpirationMM").val(), /* Expire MM */
											$("#textExpirationYY").val(), /* Expire YY */
											$("#textSecurity").val(), /* Security */
											'<?php echo $_SERVER['REMOTE_ADDR'];?>' /* IP */
										]);
									}
								});

								if(guide != ""){
									for(var i = 0; i < parseInt($("#selectJumlah").val(), 10); i++){
										all.push([
											1, /* Tiket */
											$("span.session.pengguna").text(), /* Penjual */
											0, /* Harga */
											0, /* Diskon */
											0, /* Komisi */
											0, /* Status */
											$("#textIDGuide").val(), /* Guide */
											"Free", /* Kategori */
											0, /* Jumlah */
											0, /* Total */
											0, /* Scan */
											0, /* Bayar */
											1, /* Jenis Bayar */
											$("#textNoKartu").val(), /* No Kartu */
											$("#textExpirationMM").val(), /* Expire MM */
											$("#textExpirationYY").val(), /* Expire YY */
											$("#textSecurity").val(), /* Security */
											'<?php echo $_SERVER['REMOTE_ADDR'];?>' /* IP */
										]);
									}
								}
		
								$.ajax({
									type:"POST",
									url:"/ticketing/public/transaksitiket/store",
									async:false,
									data:{
										all:all
									},
									success:function(response){
										console.log(response);
										bmp = response;
									}
								});
								
								
								$.ajax({
									type:"GET",
									url:"http://<?php echo $_SERVER['REMOTE_ADDR'];?>/ticketing/public/print/ToBmp.php",
									dataType: 'jsonp',
									data:{
										bmp:bmp
									},
									success:function(response){
										console.log(bmp);
									}
								});
		
								toastr.success("Sukses Menjalankan Perintah");
								setTimeout(function(){ window.location.href = location.href; }, 3000);
							}
						}
					},
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block',
					errorPlacement: function(error, element) {
						if(element.parent('.input-group').length) {
							error.insertAfter(element.parent());
						}

						else{
							error.insertAfter(element);
						}
					}
				});
				
				/* Plus Transaksi Tiket */
				$(".plus.transaksitiket").on("click", function(){
					var harga = $(this).closest("tr").find("td").eq(1);
					var diskon = $(this).closest("tr").find("td").eq(2);
					var jumlah = $(this).closest("tr").find("td input");
					var total = $(this).closest("tr").find("td").eq(4);
					var absolutjumlah = $(this).closest("table").find("tfoot tr td").eq(1);
					var absoluttotal = $(this).closest("table").find("tfoot tr td").eq(2);
					
					jumlah.val(parseInt(jumlah.val(), 10) + 1);
					
					var jumlahTiket = parseInt(jumlah.val(), 10);

					total.autoNumeric("set", (parseInt(harga.autoNumeric("get"), 10) - (parseInt(harga.autoNumeric("get"), 10) * parseFloat(diskon.autoNumeric("get"), 10) / 100)) * jumlahTiket);

					i = i + 1;
					j = j + (parseInt(harga.autoNumeric("get"), 10) - (parseInt(harga.autoNumeric("get"), 10) * parseFloat(diskon.autoNumeric("get"), 10) / 100));

					absolutjumlah.text(i);
					absoluttotal.autoNumeric("set", j);

					$("#textTotal").autoNumeric("set", j);

					if($("#textTotal").autoNumeric("get") == 0)
					{
						$("#buttonSave").prop('disabled', true);
					}

					$("#textKembali").val(0);
					$("#textBayar").val(0);
				});

				/* Minus Transaksi Tiket */
				$(".minus.transaksitiket").on("click", function(){
					var harga = $(this).closest("tr").find("td").eq(1);
					var diskon = $(this).closest("tr").find("td").eq(2);
					var jumlah = $(this).closest("tr").find("td input");
					var total = $(this).closest("tr").find("td").eq(4);
					var absolutjumlah = $(this).closest("table").find("tfoot tr td").eq(1);
					var absoluttotal = $(this).closest("table").find("tfoot tr td").eq(2);
					
					var jumlahTiket = parseInt(jumlah.val(), 10);
					
					if(jumlahTiket > 0)
					{
						jumlah.val(jumlahTiket - 1);
						total.autoNumeric("set", (parseInt(harga.autoNumeric("get"), 10) - (parseInt(harga.autoNumeric("get"), 10) * parseFloat(diskon.autoNumeric("get"), 10) / 100)) * parseInt(jumlah.val(),10));

						i = i - 1;
						j = j - (parseInt(harga.autoNumeric("get"), 10) - (parseInt(harga.autoNumeric("get"), 10) * parseFloat(diskon.autoNumeric("get"), 10) / 100));
					}

					absolutjumlah.text(i);
					absoluttotal.autoNumeric("set", j);

					$("#textTotal").autoNumeric("set", j);

					if($("#textTotal").autoNumeric("get") == 0)
					{
						$("#buttonSave").prop('disabled', true);
					}

					$("#textKembali").val(0);
					$("#textBayar").val(0);
				});
				
				$(".clear.transaksitiket").on("click", function(){
					var harga = $(this).closest("tr").find("td").eq(1);
					var diskon = $(this).closest("tr").find("td").eq(2);
					var jumlah = $(this).closest("tr").find("td input");
					var total = $(this).closest("tr").find("td").eq(4);
					var absolutjumlah = $(this).closest("table").find("tfoot tr td").eq(1);
					var absoluttotal = $(this).closest("table").find("tfoot tr td").eq(2);

					var nilaiUnit = parseInt(total.autoNumeric("get"), 10);
					var jmlTiket = parseInt(jumlah.val());

					var jumlahTotal = parseInt(absolutjumlah.text());
					var nilaiTotal = parseInt(absoluttotal.autoNumeric("get"), 10);

					jumlah.val(parseInt(0));
					total.autoNumeric("set", 0);

					absolutjumlah.text((jumlahTotal - jmlTiket));

					i = (jumlahTotal - jmlTiket);

					absoluttotal.autoNumeric("set", (nilaiTotal - nilaiUnit));

					j = (nilaiTotal - nilaiUnit);

					$("#textTotal").autoNumeric("set", j);

					if($("#textTotal").autoNumeric("get") == 0)
					{
						$("#buttonSave").prop('disabled', true);
					}

					$("#textKembali").val(0);
					$("#textBayar").val(0);
				});

				/* Reset Transaksi Tiket */
				$("button.reset.transaksitiket").on("click", function(){
					$("form.guide").trigger('reset');
					$('#plc-box-free').fadeOut();
					$("#selectJumlah").val(0);
					$("#selectJumlah").prop('disabled', true);
					$('#plc-free').html("");
				});

				/* Kalkulasi Pembayaran */
				$("#textBayar").on("keyup", function(e){
					var total = parseInt($("#textTotal").autoNumeric("get"), 10);
					var bayar = parseInt($("#textBayar").autoNumeric("get"), 10);

					if(bayar >= total)
					{
						$("#textKembali").autoNumeric("set", (bayar - total));
						$("#buttonSave").prop('disabled', false);
						$("#buttonSaveDebet").prop('disabled', false);
					}

					else
					{
						$("#buttonSave").prop('disabled', true);
						$("#buttonSaveDebet").prop('disabled', true);
					}
				});

				$("input.count").on("focus", function(){
					$(this).select();
				});

				$("input.count").on("blur", function(){
					if($(this).val() == "")$(this).val(0);
				});

				$("input.count").on("keypress", function(e){
				});

				$("input.count").on("keyup", function(){
					i = 0;
					j = 0;

					var harga = $(this).closest("tr").find("td").eq(1);
					var diskon = $(this).closest("tr").find("td").eq(2);
					var jumlah = $(this).closest("tr").find("td input");
					var total = $(this).closest("tr").find("td").eq(4);
					var absolutjumlah = $(this).closest("table").find("tfoot tr td").eq(1);
					var absoluttotal = $(this).closest("table").find("tfoot tr td").eq(2);

					if(jumlah.val() == "")
					{
						total.autoNumeric("set", 0);
					}

					else
					{
						total.autoNumeric("set", (parseInt(harga.autoNumeric("get"), 10) - (parseInt(harga.autoNumeric("get"), 10) * parseFloat(diskon.autoNumeric("get"), 10) / 100)) * parseInt(jumlah.val(), 10));
					}

					$("tbody.rows.transaksitiket tr td input").each(function(index){
						var subtotal = $(this).closest("tr").find("td").eq(4);

						if($(this).val() == "")i = i + 0;
						else i = i + parseInt($(this).val(), 10);

						j = j + parseInt(subtotal.autoNumeric("get"), 10);

						absolutjumlah.text(i);
						absoluttotal.autoNumeric("set", j);
					});

					$("#textTotal").autoNumeric("set", j);
					
					if($("#textTotal").autoNumeric("get") == 0)
					{
						$("#buttonSave").prop('disabled', true);
					}

					$("#textKembali").val(0);
					$("#textBayar").val(0);
				});

				/* Store Transaksi Tiket */
				$("button.save.transaksitiket").on("click", function(){
					var totalAwal = $("#jmlBeli").text();
					
					if(totalAwal > 50)
					{
						bootbox.alert({
							size:"small",
							title:"Konfirmasi",
							message:"Jumlah Pembelian Lebih Dari 50"
						});
					}
					
					else
					{
						var id = $("#textIDGuide").val();
						var guide = $("#textGuide").val();

						if(id != "" && guide == ""){
							$.ajax({
								type:"POST",
								url:"/ticketing/public/guide/store",
								async:false,
								data:{
									referensi:$("#textIDGuide").val()
								},
								success:function(response){
									id = response;
								}
							});
						}

						if($("#textTotal").autoNumeric("get") >= 0)
						{
							$(this).prop('disabled',true);
							
							$("tbody.rows.transaksitiket tr").each(function(index){
								var jumlah = $(this).closest("tr").find("td input").val();

								for(var i = 0; i < parseInt(jumlah, 10); i++){
									all.push([
										$(this).find("td").eq(6).text(), /* Tiket */
										$("span.session.pengguna").text(), /* Penjual */
										$(this).find("td").eq(1).autoNumeric("get"), /* Harga */
										$(this).find("td").eq(2).autoNumeric("get"), /* Diskon */
										$(this).find("td").eq(7).text(), /* Komisi */
										1, /* Status */
										id, /* Guide */
										$(this).find("td").eq(0).text(), /* Kategori */
										$(this).closest("tr").find("td input").val(), /* Jumlah */
										$(this).find("td").eq(4).autoNumeric("get"), /* Total */
										0, /* Scan */
										$("#textBayar").autoNumeric("get"), /* Bayar */
										0, /* Jenis Bayar */
										'', /* No Kartu */
										'', /* Expire MM */
										'', /* Expire YY */
										'', /* Security */
										'<?php echo $_SERVER['REMOTE_ADDR'];?>' /* IP */
									]);
								}
							});
							
							if(guide != ""){
								for(var i = 0; i < parseInt($("#selectJumlah").val(), 10); i++){
									all.push([
										1, /* Tiket */
										$("span.session.pengguna").text(), /* Penjual */
										0, /* Harga */
										0, /* Diskon */
										0, /* Komisi */
										0, /* Status */
										$("#textIDGuide").val(), /* Guide */
										"Free", /* Kategori */
										0, /* Jumlah */
										0, /* Total */
										0, /* Scan */
										0, /* Bayar */
										0, /* Jenis Bayar */
										$("#textNoKartu").val(), /* No Kartu */
										$("#textExpirationMM").val(), /* Expire MM */
										$("#textExpirationYY").val(), /* Expire YY */
										$("#textSecurity").val(), /* Security */
										'<?php echo $_SERVER['REMOTE_ADDR'];?>' /* IP */
									]);
								}
							}

							$.ajax({
								type:"POST",
								url:"/ticketing/public/transaksitiket/store",
								async:false,
								data:{
									all:all
								},
								success:function(response){
									console.log(response);
									console.log("button.save.transaksitiket");
									bmp = response;
								}
							});
							
							$.ajax({
								type:"GET",
								url:"http://<?php echo $_SERVER['REMOTE_ADDR'];?>/ticketing/public/print/ToBmp.php",
								dataType: 'jsonp',
								data:{
									bmp:bmp
								},
								success:function(response){
									console.log(bmp);
								}
							});

							toastr.success("Sukses Menjalankan Perintah");
							setTimeout(function(){ window.location.href = location.href; }, 3000);
						}
					}
				});
			});
		</script>
	</section>
	@include('shared.searchguide')
@endsection