@extends('layouts.app')

@section('content')
	<section class="content-header">
		<h1>Kategori Tiket</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Daftar Informasi</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th class="hide">ID</th>
									<th>Kategori</th>
									<th>Harga</th>
									<th>Diskon</th>
									<th>Komisi</th>
									<th>Status</th>
									<th>&nbsp;</th>
								</thead>
								<tbody>
									@if(count($kategoritikets) == 0)
									<tr>
										<td colspan="7"><small><i>Data tidak ditemukan.</i></small></td>
									</tr>
									@else
									@foreach($kategoritikets as $key=>$kategoritiket)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td class="hide">{{ $kategoritiket->id_tiket }}</td>
										<td>{{ $kategoritiket->kategori_tiket }}</td>
										<td class="price">{{ $kategoritiket->harga }}</td>
										<td>{{ $kategoritiket->diskon }}%</td>
										<td>{{ $kategoritiket->komisi_guide }}%</td>
										<td>
											@if($kategoritiket->status_tiket == 1)
											<label class="label label-success"><i class="fa fa-check"></i> Aktif</label>
											@else
											<label class="label label-danger"><i class="fa fa-times"></i> Nonaktif</label>
											@endif
										</td>
										<td>
											<button class="btn btn-flat btn-success btn-xs open kategoritiket" type="button" data-id="{{ $kategoritiket->id_tiket }}"><i class="fa fa-pencil"></i></button>
											<button class="btn btn-flat btn-danger btn-xs delete kategoritiket" type="button" data-id="{{ $kategoritiket->id_tiket }}"><i class="fa fa-trash"></i></button>
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
							{{ $kategoritikets->links() }}
						</div>
					</div>
					<div class="box-footer">
						<button class="btn btn-flat btn-primary" type="button" data-toggle="modal" data-target="#modalKategoriTiket" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Buat Baru</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Kategori Tiket -->
		<div class="modal fade" id="modalKategoriTiket">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form class="form-horizontal" id="formKategoriTiket">
						<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
							<h4 class="modal-title">Data Kategori Tiket</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDTiket"><b class="text-danger">*</b> ID</label>
								<div class="col-sm-10">
									<input class="form-control" id="textIDTiket" type="text" placeholder="ID" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textKategori"><b class="text-danger">*</b> Kategori</label>
								<div class="col-sm-10">
									<input class="form-control" id="textKategori" name="kategori" type="text" placeholder="Kategori">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textHarga"><b class="text-danger">*</b> Harga</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input class="form-control" id="textHarga" name="harga" type="text" placeholder="Harga">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textDiskon"><b class="text-danger">*</b> Diskon</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input class="form-control" id="textDiskon" name="diskon" type="text" placeholder="Diskon">
										<span class="input-group-addon">%</span>
									</div>
									<small class="text-primary"><i>Tanda desimal dengan tanda baca titik.</i></small>
								</div>
								<div class="col-sm-2">
									<button class="btn btn-flat btn-primary btn-block" id="buttonKonversiDiskon" type="button">Konversi</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textKomisi"><b class="text-danger">*</b> Komisi</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input class="form-control" id="textKomisi" name="komisi" type="text" placeholder="Komisi">
										<span class="input-group-addon">%</span>
									</div>
									<small class="text-primary"><i>Tanda desimal dengan tanda baca titik.</i></small>
								</div>
								<div class="col-sm-2">
									<button class="btn btn-flat btn-primary btn-block" id="buttonKonversiKomisi" type="button">Konversi</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="selectStatusTiket"><b class="text-danger">*</b> Status Tiket</label>
								<div class="col-sm-10">
									<select class="form-control" id="selectStatusTiket" name="status">
										<option value="0">Off</option>
										<option value="1" selected>On</option>
									</select>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Keluar</button>
							<button class="btn btn-flat btn-primary save kategoritiket" id="buttonSave" type="submit" data-value="store">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Modal Konversi Persen -->
		<div class="modal fade" id="modalKonversiPersenDiskon">
			<div class="modal-dialog">
				<div class="modal-content">
					<form class="form-horizontal" id="formKonversiPersenDiskon">
						<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
							<h4 class="modal-title">Konversi Persen</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textHargaPersenDiskon">Harga</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input class="form-control" id="textHargaPersenDiskon" name="hargapersendiskon" type="text" placeholder="Harga">
										<span class="input-group-btn">
											<button class="btn btn-flat btn-primary" id="buttonKonversiHargaPersenDiskon" type="button">Konversi Persen</button>
										</span>
									</div>
									<small class="text-primary"><i>Harga yang anda tulis akan dikonversi menjadi diskon dari harga tiket.</i></small>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modalKonversiPersenKomisi">
			<div class="modal-dialog">
				<div class="modal-content">
					<form class="form-horizontal" id="formKonversiPersenKomisi">
						<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
							<h4 class="modal-title">Konversi Persen</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textHargaPersenKomisi">Harga</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input class="form-control" id="textHargaPersenKomisi" name="hargapersenkomisi" type="text" placeholder="Harga">
										<span class="input-group-btn">
											<button class="btn btn-flat btn-primary" id="buttonKonversiHargaPersenKomisi" type="button">Konversi Persen</button>
										</span>
									</div>
									<small class="text-primary"><i>Harga yang anda tulis akan dikonversi menjadi diskon dari komisi guide.</i></small>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				/* Global Variabel */
				$("#textHarga, #textHargaPersenDiskon, #textHargaPersenKomisi").autoNumeric("init", {vMin:0});
				$("#textDiskon").autoNumeric("init", {vMin:'0.00', vMax:'100.00'});
				$("#textKomisi").autoNumeric("init", {vMin:'0.00', vMax:'100.00'});
				$("td.price").autoNumeric("init", {aSign:'Rp '});

				/* Modal Kategori Tiket When Closing */
				$("#modalKategoriTiket").on("hidden.bs.modal", function(){
					$("form").trigger('reset');
					$("#buttonSave").data("value", "store");
				});

				$("#modalKonversiPersenDiskon").on("hidden.bs.modal", function(){
					$("#formKonversiPersenDiskon").trigger('reset');
				});

				$("#modalKonversiPersenKomisi").on("hidden.bs.modal", function(){
					$("#formKonversiPersenKomisi").trigger('reset');
				});

				$("#buttonKonversiDiskon").on("click", function(){
					$("#modalKonversiPersenDiskon").modal();
				});

				$("#buttonKonversiKomisi").on("click", function(){
					$("#modalKonversiPersenKomisi").modal();
				});

				$("#buttonKonversiHargaPersenDiskon").on("click", function(){
					if($("#textHarga").val() == ""){
						bootbox.alert({
							size:"small",
							title:"Konfirmasi",
							message:"Harga Tidak Boleh Kosong"
						});
					}

					else{
						var harga = $("#textHarga").autoNumeric("get");
						var diskon = $("#textHargaPersenDiskon").autoNumeric("get");

						if(parseFloat(harga) >= parseFloat(diskon)){
							var hasil = parseFloat(diskon) / parseFloat(harga) * 100;

							$("#textDiskon").autoNumeric("set", hasil);
							$("#modalKonversiPersenDiskon").modal("hide");
						}

						else{
							bootbox.alert({
								size:"small",
								title:"Konfirmasi",
								message:"Jumlah Diskon Lebih Dari Harga"
							});
						}
					}
				});

				$("#buttonKonversiHargaPersenKomisi").on("click", function(){
					if($("#textHarga").val() == ""){
						bootbox.alert({
							size:"small",
							title:"Konfirmasi",
							message:"Harga Tidak Boleh Kosong"
						});
					}

					else{
						var harga = $("#textHarga").autoNumeric("get");
						var komisi = $("#textHargaPersenKomisi").autoNumeric("get");

						if(parseFloat(harga) >= parseFloat(komisi)){
							var hasil = parseFloat(komisi) / parseFloat(harga) * 100;

							$("#textKomisi").autoNumeric("set", hasil);
							$("#modalKonversiPersenKomisi").modal("hide");
						}

						else{
							bootbox.alert({
								size:"small",
								title:"Konfirmasi",
								message:"Jumlah Komisi Lebih Dari Harga"
							});
						}
					}
				});

				/* Show Detail Kategori Tiket */
				$("button.open.kategoritiket").on("click", function(){
					var id = $(this).data("id");

					$.ajax({
						type:"GET",
						url:"/ticketing/public/kategoritiket/" + id,
						dataType:"json",
						success:function(response){
							console.log(response);
							$("#modalKategoriTiket").modal();
							$("#textIDTiket").val(response.id_tiket);
							$("#textKategori").val(response.kategori_tiket);
							$("#textHarga").autoNumeric("set", response.harga);
							$("#textDiskon").autoNumeric("set", response.diskon);
							$("#textKomisi").autoNumeric("set", response.komisi_guide);
							$("#selectStatusTiket").val(response.status_tiket);
							$("#buttonSave").data("value", "update");
						}
					});
				});

				$("#formKategoriTiket").validate({
					rules: {
						kategori:{
							required:true,
							pattern:/^[a-zA-Z0-9\ ]{1,100}$/
						},
						harga:"required",
						diskon:"required",
						komisi:"required",
						status:"required"
					},
					messages: {
						kategori:{
							required:"Silahkan masukkan kategori tiket",
							pattern:"Silahkan masukkan huruf alphabet atau angka numerik"
						},
						harga:"Silahkan masukkan harga",
						diskon:"Silahkan masukkan diskon",
						komisi:"Silahkan masukkan komisi",
						status:"Silahkan masukkan status"
					},
					submitHandler: function(form) {
						var method = "";
						var url = "";
						var data = {
							kategori:$("#textKategori").val(),
							harga:$("#textHarga").autoNumeric("get"),
							diskon:$("#textDiskon").autoNumeric("get"),
							komisi:$("#textKomisi").autoNumeric("get"),
							status:$("#selectStatusTiket").val()
						};

						if($("button.save.kategoritiket").data("value") == "store"){
							method = "POST";
							url = "/ticketing/public/kategoritiket/store";
						}

						if($("button.save.kategoritiket").data("value") == "update"){
							var id = $("#textIDTiket").val();

							method = "PUT";
							url = "/ticketing/public/kategoritiket/update/" + id;
						}

						$.ajax({
							type:method,
							url:url,
							data:data,
							success:function(response){
								toastr.success("Sukses Menjalankan Perintah");
								setTimeout(function(){ window.location.href = location.href; }, 3000);
							}
						});
					},
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block',
					errorPlacement: function(error, element) {
						if(element.parent('.input-group').length) {
							error.insertAfter(element.parent());
						}

						else{
							error.insertAfter(element);
						}
					}
				});

				/* Store Or Update Kategori Tiket */
				$("button.save.kategoritiket").on("click", function(){
				});

				/* Delete Kategori Tiket */
				$("button.delete.kategoritiket").on("click", function(){
					var id = $(this).data("id");

					bootbox.confirm({
						size:"small",
						title:"Konfirmasi",
						message:"Hapus Kategori Tiket?",
						callback:function(result){
							if(result){
								$.ajax({
									type:"DELETE",
									url:"/ticketing/public/kategoritiket/delete/" + id,
									success:function(response){
										toastr.success("Sukses Menjalankan Perintah");
										setTimeout(function(){ window.location.href = location.href; }, 3000);
									}
								});
							}
						}
					});
				});
			});
		</script>
	</section>
@endsection