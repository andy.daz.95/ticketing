@extends('layouts.app')

@section('content')
	<section class="content-header">
		<h1>Transaksi Komisi Guide</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class='col-sm-7'>
				<div class="box hide">
					<form class="form-horizontal" action="{{ url('transaksikomisiguide/search/guide') }}" method="get">
						<div class="box-header with-border">Guide</div>
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDGuide">Guide</label>
								<div class="col-sm-4">
									<input class="form-control" id="textIDGuide" name="id" type="text" value="{{ isset($travels) ? $travels->id_guide : '' }}" placeholder="ID Guide" readonly>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<input class="form-control" id="textGuide" type="text" value="{{ isset($travels) ? $travels->nama : '' }}" placeholder="Guide" disabled>
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalSearchGuide"><i class="fa fa-plus"></i></button>
											<button class="btn btn-danger reset transaksikomisiguide" type="button"><i class="fa fa-close"></i></button>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-flat btn-primary search transaksikomisiguide" id="buttonSearch" type="submit" data-value="store"><i class="fa fa-search"></i> Cari</button>
						</div>
					</form>
				</div>
				<div class="box">
					<div class="box-header with-border">Transaksi Tiket</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<th>No.</th>
									<th>ID</th>
									<th>Kategori</th>
									<th>Komisi</th>
									<th>Waktu</th>
									<th><input class="cek-all" type="checkbox" checked></th>
								</thead>
								<tbody class="rows transaksikomisiguide">
									@if(count($transaksitikets) == 0)
									<tr>
										<td colspan="6"><small><i>Data tidak ditemukan.</i></small></td>
									</tr>
									@else
									<?php $no = 1; ?>
									@foreach($transaksitikets as $transaksitiket)
									<tr class="item">
										<td>{{ $no++ }}</td>
										<td>{{ $transaksitiket->id_transaksi }}</td>
										<td>{{ $transaksitiket->kategori_tiket }}</td>
										<td class="price">{{ ($transaksitiket->harga * $transaksitiket->komisi_guide / 100) }}</td>
										<td>{{ $transaksitiket->wkt_transaksi }}</td>
										<td><input class="cek transaksikomisiguide data-komisi-guide" type="checkbox" checked="true"></td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer">
						<small><i>Pembayaran guide harus secara cash.</i></small>
					</div>
				</div>
			</div>
			<div class='col-sm-5'>
				<div class="box">
					<div class="box-header with-border">Transaksi Komisi Guide</div>
					<div class="box-body">
						<form class="form-horizontal">
							<div class="form-group hide">
								<label class="col-sm-2 control-label" for="textJenis">Jenis</label>
								<div class="col-sm-10">
									<input class="form-control" id="textJenis" type="text" value="{{ isset($travels) ? $travels->jenis_transaksi : '' }}" placeholder="Jenis" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textJumlah">Jumlah</label>
								<div class="col-sm-10">
									<input class="form-control" id="textJumlah" type="text" placeholder="Jumlah" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textKomisi">Komisi</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input class="form-control" id="textKomisi" type="text" placeholder="Komisi" disabled>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textPersen">Persentase</label>
								<div class="col-sm-10">
									<div class="input-group">
										<input class="form-control" id="textPersen" type="text" value="{{ empty($travels->guide_komisi) ? 0 : $travels->guide_komisi }}" placeholder="Persen" disabled>
										<span class="input-group-addon">%</span>
									</div>
									<small class="text-primary"><i>Persentase yang diterima dari pihak travel.</i></small>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTravel">Travel</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input class="form-control" id="textTravel" type="text" placeholder="Travel" disabled>
									</div>
									<small class="text-primary"><i>Komisi untuk travel.</i></small>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTotal">Total</label>
								<div class="col-sm-10">
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input class="form-control" id="textTotal" type="text" placeholder="Total" disabled>
									</div>
									<small class="text-primary"><i>Komisi untuk guide.</i></small>
								</div>
							</div>
						</form>
					</div>
					<div class="box-footer">
						<button class="btn btn-flat btn-primary save transaksikomisiguide" id="buttonSave" type="button" data-value="store"><i class="fa fa-save"></i> Simpan</button>
						<img style="margin:0 15px;" src="{{ URL::asset('images/loading.gif') }}" id="loader">
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				/* Global Variabel */
				$("#textPersen").autoNumeric("init");
				$("#textKomisi").autoNumeric("init");
				$("#textTotal").autoNumeric("init");
				$("#textTravel").autoNumeric("init");
				$("td.price").autoNumeric("init", {aSign:'Rp '});

				var i = 0;
				var j = 0;
				var persen = $("#textPersen").autoNumeric("get");

				$("tbody.rows.transaksikomisiguide tr.item").each(function(index){
					var id = $(this).find("td").eq(1).text();
					var komisi = $(this).closest("tr").find("td").eq(3);
					var cek = $(this).find("td input")[0];

					if(cek.checked){
						i = i + 1;
						j = j + parseInt(komisi.autoNumeric("get"), 10);
					}
				});

				if(parseFloat(persen, 10) > 0){
					$("#textJumlah").val(i);
					$("#textKomisi").autoNumeric("set", j);
					$("#textTotal").autoNumeric("set", j * parseFloat(persen, 10) / 100);
					$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
				}

				else{
					$("#textJumlah").val(i);
					$("#textKomisi").autoNumeric("set", j);
					$("#textTotal").autoNumeric("set", j);
					$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
				}

				/* Cek Transaksi Komisi Guide */
				$("input.cek.transaksikomisiguide").on("change", function(){
					var komisi = $(this).closest("tr").find("td").eq(3);

					if(this.checked){
						i = i + 1;
						j = j + parseInt(komisi.autoNumeric("get"), 10);

						$("#textJumlah").val(i);
						$("#textKomisi").autoNumeric("set", j);
						$("#textTotal").autoNumeric("set", j);
						$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
					}

					else{
						i = i - 1;
						j = j - parseInt(komisi.autoNumeric("get"), 10);

						$("#textJumlah").val(i);
						$("#textKomisi").autoNumeric("set", j);
						$("#textTotal").autoNumeric("set", j);
						$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
					}

					if(parseFloat(persen, 10) > 0){
						$("#textJumlah").val(i);
						$("#textKomisi").autoNumeric("set", j);
						$("#textTotal").autoNumeric("set", j * parseFloat(persen, 10) / 100);
						$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
					}

					else{
						$("#textJumlah").val(i);
						$("#textKomisi").autoNumeric("set", j);
						$("#textTotal").autoNumeric("set", j);
						$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
					}
				});

				/* Reset Transaksi Komisi Guide */
				$("button.reset.transaksikomisiguide").on("click", function(){
					$("form").trigger('reset');

					$("#textIDGuide").val("");
					$("#textGuide").val("");
					$("#textPersen").val("");

					$("tbody.rows.transaksikomisiguide").html("");

					$("#buttonSearch").attr("disabled", "disabled");
				});

				/* Store Transaksi Komisi Guide */
				$("button.save.transaksikomisiguide").on("click", function(){
					if($("#textJenis").val() == 0){
						$("#buttonSave").prop('disabled', true);

						var total = $("#textTotal").autoNumeric("get");

						if(parseInt(total, 10) > 0){
							$.ajax({
								type:"POST",
								url:"/ticketing/public/transaksikomisiguide/store",
								async:false,
								data:{
									guide:$("#textIDGuide").val(),
									pengguna:$("span.session.pengguna").text(),
									nilai:total,
									status:0
								},
								success:function(response){
									toastr.success("Sukses Menjalankan Perintah");
									setTimeout(function(){ window.location.href = location.href; }, 3000);
								}
							});

							$("tbody.rows.transaksikomisiguide tr").each(function(index){
								var id = $(this).find("td").eq(1).text();
								var cek = $(this).find("td input")[0];

								if(cek.checked){
									$.ajax({
										type:"PUT",
										url:"/ticketing/public/komisi/transaksitiket/update/" + id,
										async:false,
										data:{
											pembayaran:'1'
										}
									});
								}
							});
						}

						else{
							toastr.info("Informasi Sesuaikan Jumlah Bayar");
						}
					}

					else{
							toastr.info("Pembayaran Guide Harus Cash");
					}
				});

				$(".cek-all").on("change", function(){
					if($(this).is(':checked')){
						i = 0;
						j = 0;

						$('input.data-komisi-guide').each(function () {
		                    this.checked = true;
		                });

		                $("tbody.rows.transaksikomisiguide tr").each(function(index){
							var id = $(this).find("td").eq(1).text();
							var komisi = $(this).closest("tr").find("td").eq(3);
							var cek = $(this).find("td input")[0];

							if(cek.checked){
								i = i + 1;
								j = j + parseInt(komisi.autoNumeric("get"), 10);
							}
						});

						if(parseFloat(persen, 10) > 0){
							$("#textJumlah").val(i);
							$("#textKomisi").autoNumeric("set", j);
							$("#textTotal").autoNumeric("set", j * parseFloat(persen, 10) / 100);
							$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
						}

						else{
							$("#textJumlah").val(i);
							$("#textKomisi").autoNumeric("set", j);
							$("#textTotal").autoNumeric("set", j);
							$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
						}
					}
					else{
						$('input.data-komisi-guide').each(function () {
		                    this.checked = false;
		                });

		                $("tbody.rows.transaksikomisiguide tr").each(function(index){
							var id = $(this).find("td").eq(1).text();
							var komisi = $(this).closest("tr").find("td").eq(3);
							var cek = $(this).find("td input")[0];

							if(!cek.checked){
								i = 0;
								j = 0;
							}
						});

						if(parseFloat(persen, 10) > 0){
							$("#textJumlah").val(i);
							$("#textKomisi").autoNumeric("set", j);
							$("#textTotal").autoNumeric("set", j * parseFloat(persen, 10) / 100);
							$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
						}

						else{
							$("#textJumlah").val(i);
							$("#textKomisi").autoNumeric("set", j);
							$("#textTotal").autoNumeric("set", j);
							$("#textTravel").autoNumeric("set", j - parseInt($("#textTotal").autoNumeric("get"), 10));
						}
					}
				});

				$("form").on("submit", function(e){
					e.preventDefault();

					var id = $("#textIDGuide").val();
					var url = this.action + '/' + id;

					window.location.href = url;
				});
			});
		</script>
	</section>
	@include('shared.searchguide')
@endsection