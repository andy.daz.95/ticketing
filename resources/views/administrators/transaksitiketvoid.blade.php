@extends('layouts.app')

@section('content')
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<form class="form-horizontal" action="{{ url('laporantransaksitiket/excel') }}" method="post">
						{{ csrf_field() }}
						<div class="box-header with-border">Void Tiket</div>
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTanggalAkhir">Waktu Awal</label>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textTanggalMulai" name="mulai" type="text" placeholder="Tanggal Akhir">
										 <span class="input-group-addon addonTextTanggalMulai" style="cursor:pointer"><i class="glyphicon glyphicon-time"></i></span>
										</span>
									</div>
								</div>
								<label class="col-sm-2 control-label" for="textTanggalAkhir">Waktu Akhir</label>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textTanggalAkhir" name="selesai" type="text" placeholder="Tanggal Akhir">
										 <span class="input-group-addon addonTextTanggalAkhir" style="cursor:pointer"><i class="glyphicon glyphicon-time"></i></span>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="selectTiket">Tiket</label>
								<div class="col-sm-4">
									<select class="form-control" id="selectTiket" name="tiket">
										<option value="">Semua Tiket</option>
										@foreach($kategoritikets as $kategoritiket)
											<option value="{{ $kategoritiket->id_tiket }}">{{ $kategoritiket->kategori_tiket }}</option>
										@endforeach
									</select>
								</div>
								<label class="col-sm-2 control-label" for="textTanggalMulai">Pencarian</label>
								<div class="col-sm-4">
									<input class="form-control" id="textPencarian" name="pencarian" type="text" placeholder="Masukan Nomor Invoice">
								</div>
							</div>
						</div>
						<div class="box-footer text-right">
							<button class="btn btn-flat btn-primary search laporantransaksitiket" id="buttonSearch" type="button" data-value="store"><i class="fa fa-search"></i> Cari</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">
						Transaksi Tiket Hari Ini <small><b><i><?php echo date('d/m/Y'); ?></i></b></small>
						<button class="btn btn-flat btn-danger pull-right" data-toggle="modal" data-target="#myModalVoid"><i class="fa fa-close"></i> Void Tiket</button>
						<button style="margin-right: 15px;" class="btn btn-flat btn-success pull-right btn-print-ticket"><i class="fa fa-print"></i> Print Tiket</button>
					</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th>Invoice</th>
									<th>No. Tiket</th>
									<th>Kategori</th>
									<th>Harga</th>
									<th>Diskon</th>
									<th>Komisi</th>
									<th>Total</th>
									<th>Waktu</th>
									<th><label class="checkbox-inline"><input type="checkbox" id="checkAllVoid"/> <b>Check All</b></label></th>
								</thead>
								<tbody class="rows laporantransaksitiket">
									<tr><td colspan="10"><small><i>Data tidak ditemukan.</i></small></td></tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer">
						<small><i>Anda dapat membatalkan tiket yang telah dibeli.</i></small>
					</div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
		<div id="myModalVoid" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Konfirmasi</h4>
					</div>
					<div class="modal-body">
						<p>Lakukan Void Pada Tiket?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success btn-void">Yakin</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.btn-print-ticket').click(function(){
					var tmpData = "";
					var count = 0;
					var trigger = "";
					var result = [];

					$('input.data-void').each(function(i){
						if(this.checked==true)
						{
							trigger="yes";
							tmpData+='"';
							tmpData+=$('#tiket' + count).text() + ';0;0;{';
							tmpData+='\\"kategori_tiket\\":\\"' + $('#kategori' + count).text() + '\\",';
							tmpData+='\\"harga\\":' + $('#harga' + count).text() + ',';
							tmpData+='\\"diskon\\":' + $('#diskon' + count).text();
							tmpData+='};0';
							tmpData+='",';

							result.push($('#tiket' + count).text() + ';0;0;{\\kategori_tiket\\:\\"' + $('#kategori' + count).text() + '\\",\\"harga\\":' + $('#harga' + count).text() + ',\\"diskon\\":' + $('#diskon' + count).text() + '};0');
						}

						count++;
					});

					var str = tmpData;
					var lastIndex = str.lastIndexOf(",");
					str = str.substring(0, lastIndex);
					tmpData = '[' + str + ']';

					if(trigger != "")
					{
						$.ajax({
							type:"GET",
							url:"http://<?php echo $_SERVER['REMOTE_ADDR'];?>/ticketing/public/print/ToBmpTicket.php",
							dataType: 'jsonp',
							data:{
								bmp:result
							},
							success:function(response){
								console.log(result);
							}
						});
					}
				});

				$('.btn-void').click(function(){
					var dataTmp = "";
					var count = 0;
					$('input.data-void').each(function(i){
						if(this.checked == true)
						{
							dataTmp+=$(this).val() + "-";
						}

						count++;
					});

					if(dataTmp != "")
					{
						$.ajax({
							type:"POST",
							url:"/ticketing/public/transaksitiketvoidproses",
							data:{
								data:dataTmp
							},
							success:function(response){
								toastr.success("Sukses Menjalankan Perintah");
								setTimeout(function(){ window.location.href = location.href; }, 3000);
							}
						});
					}

					else
					{
						alert("Silahkan pilih tiket yang akan di void");
					}
				});

                $("#textTanggalMulai").val(' 00:00');
                $("#textTanggalAkhir").val(" 23:59");

                $(".addonTextTanggalMulai").click(function(){
                    $("#textTanggalMulai").focus();
                });

                $(".addonTextTanggalAkhir").click(function(){
                    $("#textTanggalAkhir").focus();
                });

				/* Show Date Picker */
				$("#textTanggalMulai, #textTanggalAkhir").timepicker({
					showMeridian:false
				});
				
				var globalJmlDataVoid = 0;
				
				$('#checkAllVoid').change(function(){
					if(this.checked)
					{
						$('input.data-void').each(function(){
							this.checked = true;
						});
					}

					else
					{
						$('input.data-void').each(function(){
							this.checked = false;
						});
					}
				});

				$("#buttonSearch").on("click", function(){
					$.ajax({
						type:"POST",
						url:"/ticketing/public/transaksitiketvoid",
						data:{
							mulai:$("#textTanggalMulai").val()+":00",
							selesai:$("#textTanggalAkhir").val()+":00",
							tiket:$("#selectTiket").val(),
							cari:$("#textPencarian").val()
						},
						success:function(response){
							var row = "";
							var count = 0;
							var no = 1;
							$.each(response, function(i, value){
								row = row + "<tr id='line" + count + "'>" +
											"<td>" + no++ + "</td>" +
											"<td id='invoice" + count + "'>" + value.invoice + "</td>" +
											"<td id='tiket" + count + "'>" + value.id_transaksi + "</td>" +
											"<td id='kategori" + count + "'>" + value.kategori_tiket + "</td>" +
											"<td id='harga" + count + "' class='price'>" + value.harga + "</td>" +
											"<td id='diskon" + count + "'>" + value.diskon + "%</td>" +
											"<td>" + value.komisi_guide + "%</td>" +
											"<td class='price'>" + (parseFloat(value.harga) - (parseFloat(value.harga) * parseFloat(value.diskon) / 100)) + "</td>" +
											"<td>" + value.wkt_transaksi + "</td>" +
											"<td><input class='data-void' type='checkbox' id='data-void" + count + "' value='" + value.id_transaksi + "'></td>" +
											"</tr>";
								count++;
							});

							globalJmlDataVoid = count;

							$("tbody.rows.laporantransaksitiket").html(row);
							$("td.price").autoNumeric("init", {aSign:'Rp '});

							if(count == 0)
							{
								$("tbody.rows.laporantransaksitiket").html("<tr><td colspan='10'><small><i>Data tidak ditemukan.</i></small></td></tr>");	
							}
						}
					});
				});
			});
		</script>
	</section>
@endsection