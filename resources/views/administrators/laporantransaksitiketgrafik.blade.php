@extends('layouts.app')

@section('content')
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Grafik Laporan Bulanan Transaksi Tiket</div>
					<div class="box-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="selectTiket">Tiket</label>
								<div class="col-sm-4">
									<div class="checkbox">
										<label><input type="checkbox" id="all" name="tiket" value="0"> Semua Tiket</label>
									</div>
									@foreach($kategoritikets as $kategoritiket)
									<div class="checkbox">
										<label><input type="checkbox" name="tiket" value="{{ $kategoritiket->id_tiket }}"> {{ $kategoritiket->kategori_tiket }}</label>
									</div>
									@endforeach
									<select class="form-control hide" id="selectTiket">
										<option value="0">Semua Tiket</option>
										@foreach($kategoritikets as $kategoritiket)
										<option value="{{ $kategoritiket->id_tiket }}">{{ $kategoritiket->kategori_tiket }}</option>
										@endforeach
									</select>
								</div>
								<label class="col-sm-2 control-label" for="textTanggalMulai">Tahun</label>
								<div class="col-sm-4">
									<select class="form-control" id="tahunSearch">
										<?php
											$start = 2016;
											$finish = date('Y');
											for ($i = $start; $i <= $finish; $i++)
											{
												echo '<option value="'.$i.'">'.$i.'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</form>
					</div>
					<div class="box-footer">
						<button class="btn btn-flat btn-primary search laporantransaksitiket pull-right" id="buttonSearch" type="button" data-value="store"><i class="fa fa-search"></i> Cari</button>
					</div>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Grafik Laporan Bulanan Transaksi Tiket</div>
					<div class="box-body"><canvas id="chartLaporanTransaksiTiket" class="hide"></canvas></div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#all").on("click", function(){
					if($(this).is(":checked")){
						$("input:checkbox").prop("checked", true);
					}

					else{
						$("input:checkbox").prop("checked", false);
					}
				});
				
				/* Show Date Picker */
				$("#textTanggalMulai, #textTanggalAkhir").daterangepicker({
					singleDatePicker:true,
					timePicker:true,
					timePicker24Hour:true,
					locale:{ format:"YYYY-MM-DD HH:mm:ss" }
				});

				/* Show Chart */
				var chart = $("#chartLaporanTransaksiTiket");

				var myLineChart = null;

		        var randomColorFactor = function() {
		            return Math.round(Math.random() * 255);
		        };
		        var randomColor = function(opacity) {
		            return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
		        };

		        var backGroundColor = ["#3c8dbc",
		        					   "#00a65a",
		        					   "#f56954",
		        					   "#001F3F",
		        					   "#605ca8",
		        					   "#D81B60",
		        					   "#00c0ef",
		        					   "#f39c12",
		        					   "#D81B60",
		        					   "#ff9966",
		        					   "#006699",
		        					   "#33ff66",
		        					   "#ff9999",
		        					   "#999966",
		        					   "#cc33cc",
		        					   "#336666",
		        					   "#00cc99",
		        					   "#ccffcc",
		        					   "#6633ff"];

		        var pointBorderColor = ["#3c8dbc",
		        						"#00a65a",
		        						"#f56954",
		        						"#001F3F",
		        						"#605ca8",
		        						"#D81B60",
		        						"#00c0ef",
		        						"#f39c12",
		        						"#D81B60",
		        						"#ff9966",
		        						"#006699",
		        						"#33ff66",
		        						"#ff9999",
		        						"#999966",
		        						"#cc33cc",
		        						"#336666",
		        						"#00cc99",
		        						"#ccffcc",
		        						"#6633ff"];
				
		        $("#buttonSearch").on("click", function(){
		        	var tiket = [];

					$("input:checked").each(function(index){
						tiket.push($(this).val());
					});

			        $.ajax({
			        	type:"POST",
			        	url:"/ticketing/public/laporantransaksitiketgrafik",
			        	data:{
			        		tahun:$("#tahunSearch").val(),
			        		tiket:JSON.stringify(tiket)
			        	},
			        	success:function(response){
			        		var max = 0;
			        		var myData = JSON.parse(response);
			        		var dataset;
			        		var datasets = [];
			        		for(i=0;i<myData.result.length;i++){
			        			dataset = {
				                    	label: myData.result[i].jenis_tiket,
				                    	backgroundColor: backGroundColor[i],
				                    	pointBorderColor: pointBorderColor[i],
				                    	data: [
				                    		myData.result[i].detail[0].jumlah,
				                    		myData.result[i].detail[1].jumlah,
				                    		myData.result[i].detail[2].jumlah,
				                    		myData.result[i].detail[3].jumlah,
				                    		myData.result[i].detail[4].jumlah,
				                    		myData.result[i].detail[5].jumlah,
				                    		myData.result[i].detail[6].jumlah,
				                    		myData.result[i].detail[7].jumlah,
				                    		myData.result[i].detail[8].jumlah,
				                    		myData.result[i].detail[9].jumlah,
				                    		myData.result[i].detail[10].jumlah,
				                    		myData.result[i].detail[11].jumlah],
				                };

				                datasets.push(dataset);
			        		}
			                
			                for(i = 0; i < myData.result.length; i++){
			                	for(j = 0; j < 12; j++){
				                	if(max < myData.result[i].detail[j].jumlah){
				                		max = myData.result[i].detail[j].jumlah;
				                	}
				                }
			                }

			                max = max + 20;

			        		var config = {
					            type: 'bar',
					            data: {
					                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
					                datasets: datasets
					            },
					            options: {
					                responsive: true,
					                title:{
					                    display:false
					                },
					                tooltips: {
					                    mode: 'label',
					                    callbacks: {
					                    }
					                },
					                hover: {
					                    mode: 'dataset'
					                },
					                scales: {
					                    xAxes: [{
					                        display: true,
					                        scaleLabel: {
					                            show: true,
					                            labelString: 'Month'
					                        }
					                    }],
					                    yAxes: [{
					                        display: true,
					                        scaleLabel: {
					                            show: true,
					                            labelString: 'Value'
					                        },
					                        ticks: {
					                            suggestedMin: 0,
					                            suggestedMax: max,
					                        }
					                    }]
					                }
					            }
					        };

					        $("#chartLaporanTransaksiTiket").removeAttr("class");

					        if(myLineChart != null){
								myLineChart.destroy();
							}

							myLineChart = new Chart(chart, config);
			        	}
			        });
		        });
			});
		</script>
	</section>
@endsection