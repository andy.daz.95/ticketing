@extends('layouts.app')

@section('content')
	<section class="content-header">
		<h1>Pengguna Kategori</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Daftar Informasi</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th class="hide">ID</th>
									<th>Kategori</th>
									<th>&nbsp;</th>
								</thead>
								<tbody>
									@if(count($penggunakategoris) == 0)
									<tr>
										<td colspan="3"><small><i>Data tidak ditemukan.</i></small></td>
									</tr>
									@else
									@foreach($penggunakategoris as $key=>$penggunakategori)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td class="hide">{{ $penggunakategori->id_pengguna_kategori }}</td>
										<td>{{ $penggunakategori->pengguna_kategori }}</td>
										<td><button class="btn btn-flat btn-default btn-xs open penggunakategori" type="button"><i class="fa fa-lock"></i></button></td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
							{{ $penggunakategoris->links() }}
						</div>
					</div>
					<div class="box-footer"><small><i>Data pengguna kategori tidak dapat diubah.</i></small></div>
				</div>
			</div>
		</div>
	</section>
@endsection