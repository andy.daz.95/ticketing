@extends('layouts.app')

@section('content')
	<style type="text/css">
		.datepicker{z-index: 1151 !important;}
	</style>
	<section class="content-header">
		<h1>Guide</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header with-border">Temukan Guide</div>
					<div class="box-body">
						<div class="col-sm-9">
							<form class="form-horizontal" action="{{ action('GuideController@find') }}" method="get">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="textFindNamaGuide">Nama</label>
									<div class="col-sm-4">
										<input class="form-control" id="textFindNamaGuide" type="text" placeholder="Nama Guide">
									</div>
									<label class="col-sm-2 control-label" for="textFindReferensiGuide">Referensi</label>
									<div class="col-sm-4">
										<div class="input-group">
											<input class="form-control" id="textFindReferensiGuide" type="text" placeholder="Referensi Guide">
											<div class="input-group-btn">
												<button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="col-sm-3 text-right">
							<button class="btn btn-flat btn-primary" type="button" data-toggle="modal" data-target="#modalGuide" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Buat Baru</button>
						</div>
					</div>
				</div>
			</div>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Daftar Informasi</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th>ID</th>
									<th>Referensi</th>
									<th>Travel</th>
									<th>Nama</th>
									<th>Gambar</th>
									<th>Status</th>
									<th>&nbsp;</th>
								</thead>
								<?php
									$base_path = "http://192.168.15.150/ticketing/public/fingerprint/";
									echo '<input type="hidden" value="'.$base_path.'" id="urlAction">';
								?>
								<tbody>
									@if(count($guides) == 0)
									<tr>
										<td colspan="8"><small><i>Data tidak ditemukan.</i></small></td>
									</tr>
									@else
									@foreach($guides as $key=>$guide)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td>{{ $guide->id_guide }}</td>
										<td>{{ $guide->id_referensi == '' ? '-' : $guide->id_referensi }}</td>
										<td>{{ $guide->nama_travel == '' ? '-' : $guide->nama_travel }}</td>
										<td>{{ $guide->nama }}</td>
										<td>
											@if($guide->foto != "")
											<img class="img-thumbnail gambar guide" width="80" src="<?php echo $guide->foto; ?>" data-toggle="modal" data-target="#modalGambar">
											@endif
										</td>
										<td>
											@if($guide->status_guide == 1)
											<label class="label label-success"><i class="fa fa-check"></i> Aktif</label>
											@else
											<label class="label label-danger"><i class="fa fa-times"></i> Nonaktif</label>
											@endif
										</td>
										<td>
											<button class="btn btn-flat btn-info btn-xs see guide" type="button" data-id="{{ $guide->id_guide }}" data-toggle="modal" data-target="#modalDetailGuide" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i></button>
											<button class="btn btn-flat btn-success btn-xs open guide" type="button" data-id="{{ $guide->id_guide }}" data-toggle="modal" data-target="#modalGuide" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil"></i></button>
											<button class="btn btn-flat btn-danger btn-xs delete guide" type="button" data-id="{{ $guide->id_guide }}"><i class="fa fa-trash"></i></button>
											<?php
												$sidik_jari=$guide->sidik_jari;
												$id = $guide->id_guide;
												if($sidik_jari=="")
												{
													if($guide->alamat!="")
													{
														$url_register = base64_encode($base_path."register.php?user_id=".$id);
														$register = "<a href='finspot:FingerspotReg;$url_register' class='btn btn-flat btn-xs btn-register btn-primary' onclick=\"user_register('".$id."') \">Sidik Jari</a>";
														echo $register;
													}
												}
												else
												{
													$url_verification	= base64_encode($base_path."verification.php?user_id=".$id);
													$verification = "<a href='finspot:FingerspotVer;$url_verification' class='btn btn-flat btn-xs btn-success'>Verifikasi</a>";
													
													echo $verification;
												}
											?>
											<img style="display:none;" class="loading-pro{{ $guide->id_guide }} user-image" src="{{ URL::asset('theme/images/loading.gif') }}" alt="User Image">
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
							{{ $guides->links() }}
						</div>
					</div>
					<div class="box-footer">
						<small><i>Silahkan melakukan pengolahan data guide.</i></small>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Gambar -->
		<div class="modal fade" id="modalGambar">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
						<h4 class="modal-title">View Gambar</h4>
					</div>
					<div class="modal-body">
						<img width="100%" src="" class="foto detail guide">
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Detail Guide -->
		<div class="modal fade" id="modalDetailGuide">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form class="form-horizontal">
						<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
							<h4 class="modal-title">Detail Guide</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelIDGuide">ID</label>
								<span class="col-sm-4 control-label" id="labelIDGuide"></span>
								<label class="col-sm-2 control-label" for="labelNoIdentitas">No. Identitas</label>
								<span class="col-sm-4 control-label" id="labelNoIdentitas"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelNama">Nama</label>
								<span class="col-sm-4 control-label" id="labelNama"></span>
								<label class="col-sm-2 control-label" for="labelTempatLahir">Tempat Lahir</label>
								<span class="col-sm-4 control-label" id="labelTempatLahir"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelTanggalLahir">Tanggal Lahir</label>
								<span class="col-sm-4 control-label" id="labelTanggalLahir"></span>
								<label class="col-sm-2 control-label" for="labelAlamat">Alamat</label>
								<span class="col-sm-4 control-label" id="labelAlamat"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelJenisKelamin">Jenis Kelamin</label>
								<span class="col-sm-4 control-label" id="labelJenisKelamin"></span>
								<label class="col-sm-2 control-label" for="labelHandphone">Handphone</label>
								<span class="col-sm-4 control-label" id="labelHandphone"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelEmail">Email</label>
								<span class="col-sm-4 control-label" id="labelEmail"></span>
								<label class="col-sm-2 control-label" for="labelJenisGuide">Jenis Guide</label>
								<span class="col-sm-4 control-label" id="labelJenisGuide"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelJenisTransaksi">Jenis Transaksi</label>
								<span class="col-sm-4 control-label" id="labelJenisTransaksi"></span>
								<label class="col-sm-2 control-label" for="labelIDTravel">ID Travel</label>
								<span class="col-sm-4 control-label" id="labelIDTravel"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelTravel">Travel</label>
								<span class="col-sm-4 control-label" id="labelTravel"></span>
								<label class="col-sm-2 control-label" for="labelBank">Bank</label>
								<span class="col-sm-4 control-label" id="labelBank"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelNoRekening">No. Rekening</label>
								<span class="col-sm-4 control-label" id="labelNoRekening"></span>
								<label class="col-sm-2 control-label" for="labelNamaIbuKandung">Ibu Kandung</label>
								<span class="col-sm-4 control-label" id="labelNamaIbuKandung"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelKTP">KTP</label>
								<span class="col-sm-4 control-label" id="labelKTP"></span>
								<label class="col-sm-2 control-label" for="labelSIMA">SIM A</label>
								<span class="col-sm-4 control-label" id="labelSIMA"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelSIMB">SIM B</label>
								<span class="col-sm-4 control-label" id="labelSIMB"></span>
								<label class="col-sm-2 control-label" for="labelSIMC">SIM C</label>
								<span class="col-sm-4 control-label" id="labelSIMC"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelLisensi">Lisensi</label>
								<span class="col-sm-4 control-label" id="labelLisensi"></span>
								<label class="col-sm-2 control-label" for="labelPassport">Passport</label>
								<span class="col-sm-4 control-label" id="labelPassport"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="labelIdentitas">Identitas</label>
								<span class="col-sm-4 control-label" id="labelIdentitas"></span>
								<label class="col-sm-2 control-label" for="labelFoto">Foto</label>
								<span class="col-sm-4 control-label" id="labelFoto"></span>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Keluar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Modal Guide -->
		<div class="modal fade scroll" id="modalGuide">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form class="form-horizontal" id="formGuide" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
							<h4 class="modal-title">Data Guide</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDGuide"><b class="text-danger">*</b> ID</label>
								<div class="col-sm-10">
									<input class="form-control" id="textIDGuide" type="text" placeholder="ID" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textNoIdentitas"><b class="text-danger">*</b> No. Identitas</label>
								<div class="col-sm-4">
									<input class="form-control" id="textNoIdentitas" name="no" type="text" placeholder="No. Identitas">
								</div>
								<label class="col-sm-2 control-label" for="textNama"><b class="text-danger">*</b> Nama</label>
								<div class="col-sm-4">
									<input class="form-control" id="textNama" name="nama" type="text" placeholder="Nama">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTempatLahir"><b class="text-danger">*</b> Tempat Lahir</label>
								<div class="col-sm-4">
									<input class="form-control" id="textTempatLahir" name="tempat" type="text" placeholder="Tempat Lahir">
								</div>
								<label class="col-sm-2 control-label" for="textTanggalLahir"><b class="text-danger">*</b> Tanggal Lahir</label>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textTanggalLahir" name="tanggal" type="text" placeholder="Tanggal Lahir">
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textareaAlamat"><b class="text-danger">*</b> Alamat</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="textareaAlamat" name="alamat"></textarea>
								</div>
								<label class="col-sm-2 control-label" for="selectJenisKelamin"><b class="text-danger">*</b> Jenis Kelamin</label>
								<div class="col-sm-4">
									<select class="form-control" id="selectJenisKelamin" name="kelamin">
										<option value="0" selected>Wanita</option>
										<option value="1">Pria</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textHandphone"><b class="text-danger">*</b> Handphone</label>
								<div class="col-sm-4">
									<input class="form-control" id="textHandphone" name="hp" type="text" placeholder="Handphone">
								</div>
								<label class="col-sm-2 control-label" for="textEmail">Email</label>
								<div class="col-sm-4">
									<input class="form-control" id="textEmail" name="email" type="text" placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="selectJenisGuide"><b class="text-danger">*</b> Jenis Guide</label>
								<div class="col-sm-4">
									<select class="form-control" id="selectJenisGuide" name="guide">
										<option value="0" selected>Freelancer</option>
										<option value="1">Travel</option>
									</select>
								</div>
								<label class="col-sm-2 control-label" for="selectJenisTransaksi"><b class="text-danger">*</b> Jenis Transaksi</label>
								<div class="col-sm-4">
									<select class="form-control" id="selectJenisTransaksi" name="transaksi">
										<option value="0" selected>Cash</option>
										<option value="1">Bank Transfer</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDTravel">ID Travel</label>
								<div class="col-sm-4">
									<input class="form-control" id="textIDTravel" name="travel" type="text" placeholder="ID Travel" readonly>
								</div>
								<label class="col-sm-2 control-label" for="textTravel">Travel</label>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textTravel" type="text" placeholder="Travel" disabled>
										<span class="input-group-btn">
											<button class="btn btn-primary trigger window travel" type="button" data-toggle="modal" data-target="#modalSearchTravel" disabled><i class="fa fa-plus"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textBank"><b class="text-danger">*</b> Bank</label>
								<div class="col-sm-4">
									<input class="form-control" id="textBank" name="bank" type="text" placeholder="Bank">
								</div>
								<label class="col-sm-2 control-label" for="textNoRekening"><b class="text-danger">*</b> No. Rekening</label>
								<div class="col-sm-4">
									<input class="form-control" id="textNoRekening" name="rekening" type="text" placeholder="No. Rekening">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="checkboxKTP">KTP</label>
								<div class="col-sm-1">
									<div class="checkbox">
										<label><input id="checkboxKTP" name="ktp" type="checkbox" value="1"></label>
									</div>
								</div>
								<label class="col-sm-2 control-label" for="checkboxSIMA">SIM A</label>
								<div class="col-sm-1">
									<div class="checkbox">
										<label><input id="checkboxSIMA" name="sima" type="checkbox" value="1"></label>
									</div>
								</div>
								<label class="col-sm-2 control-label" for="checkboxSIMB">SIM B</label>
								<div class="col-sm-1">
									<div class="checkbox">
										<label><input id="checkboxSIMB" name="simb" type="checkbox" value="1"></label>
									</div>
								</div>
								<label class="col-sm-2 control-label" for="checkboxSIMC">SIM C</label>
								<div class="col-sm-1">
									<div class="checkbox">
										<label><input id="checkboxSIMC" name="simc" type="checkbox" value="1"></label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="checkboxLisensi">Lisensi</label>
								<div class="col-sm-1">
									<div class="checkbox">
										<label><input id="checkboxLisensi" name="lisensi" type="checkbox" value="1"></label>
									</div>
								</div>
								<label class="col-sm-2 control-label" for="checkboxPassport">Passport</label>
								<div class="col-sm-1">
									<div class="checkbox">
										<label><input id="checkboxPassport" name="passport" type="checkbox" value="1"></label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textNamaIbuKandung"><b class="text-danger">*</b> Ibu Kandung</label>
								<div class="col-sm-10">
									<input class="form-control" id="textNamaIbuKandung" name="ibu" type="text" placeholder="Ibu Kandung">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="fileIdentitas"><b class="text-danger">*</b> Identitas</label>
								<div class="col-sm-4">
									<input class="filestyle" id="fileIdentitas" name="identitas" type="file" data-icon="false" data-buttonText="Browse" data-placeholder="Identitas">
									<input class="form-control" id="textShadow" name="shadow" type="text" style="opacity:0; height:0; padding:0; margin:0">
									<small class="text-primary"><i>Upload file dalam format .jpg .png</i></small>
								</div>
								<label class="col-sm-2 control-label" for="textFoto"><b class="text-danger">*</b> Foto</label>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textFoto" name="foto" type="text" placeholder="Foto" readonly>
										<span class="input-group-btn">
											<button class="btn btn-default" id="buttonFoto" type="button" data-toggle="modal" data-target="#modalFoto">Browse</button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="selectStatusGuide"><b class="text-danger">*</b> Status Guide</label>
								<div class="col-sm-10">
									<select class="form-control" id="selectStatusGuide" name="status">
										<option value="0">Off</option>
										<option value="1" selected>On</option>
									</select>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Keluar</button>
							<button class="btn btn-flat btn-primary save guide" id="buttonSave" type="submit" data-value="store">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Modal Foto -->
		<div class="modal fade" id="modalFoto">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
						<h4 class="modal-title">Mengambil Gambar</h4>
					</div>
					<div class="modal-body">
						<div class='row'>
							<div class='col-sm-6'>
								<div class="box box-warning">
									<div class="box-header with-border">Kamera</div>
									<div class="box-body">
										<div id="kamera"></div>
									</div>
								</div>
							</div>
							<div class='col-sm-6'>
								<div class="box box-success">
									<div class="box-header with-border">Hasil</div>
									<div class="box-body">
										<div id="hasil"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Keluar</button>
						<button class="btn btn-flat btn-primary take foto" id="buttonTake" type="button" data-value="store">Ambil Gambar</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Sidik Jari -->
		<div class="modal fade" id="modalSidikJari">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
						<h4 class="modal-title">Sidik Jari</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#fileIdentitas").on("change", function(){
					$("#textShadow").val($(this).val());
				});
				
				/*Load*/
				$("#formGuide").attr("action", "{{ url('/guide/primary') }}");

				$("#modalGuide").on("show.bs.modal", function(){
					var myDate = new Date(2000, 0, 1);

					$("#textTanggalLahir").val('01-01-2000');
					$("#textTanggalLahir").datepicker({
						language: 'en',
						dateFormat: 'dd-mm-yyyy',
						maxDate: new Date(),
						startDate: myDate
					});
				});

				/* Modal Guide When Closing */
				$("#modalGuide").on("hidden.bs.modal", function(){
					$("form").trigger('reset');
					$("#buttonSave").data("value", "store");
					$("#formGuide").attr("action", "{{ url('/guide/primary') }}");
					$(":file").filestyle("placeholder", "");
				});

				$("#selectJenisGuide").on("change", function(){
					if($(this).val() == 0){
						$("button.trigger.window").prop("disabled", true);
						$("#textIDTravel").val("");
						$("#textTravel").val("");
					}

					else{
						$("button.trigger.window").prop("disabled", false);
					}
				});

				$("img.gambar.guide").on("click", function(){
					$("img.foto.detail.guide").attr("src", $(this).attr("src"));
				});

				$("#formGuide").validate({
					rules: {
						no:{
							required:true,
							number:true
						},
						nama:{
							required:true,
							pattern:/^[a-zA-Z\ ]{1,100}$/
						},
						tempat:"required",
						tanggal:"required",
						alamat:"required",
						kelamin:"required",
						hp:{
							required:true,
							number:true
						},
						email:{
							email:true,
							pattern:/^\w+@[a-zA-Z\_\-]+?\.[a-zA-Z\.]{2,5}$/
						},
						guide:"required",
						transaksi:"required",
						bank:{
							required:{
								depends: function(element){
									return $("#selectJenisTransaksi").val() == 1
								}
							}
						},
						rekening:{
							required:{
								depends: function(element){
									return $("#selectJenisTransaksi").val() == 1
								}
							},
							number:true
						},
						ibu:"required",
						shadow:"required",
						foto:"required"
					},
					messages: {
						no:{
							required:"Silahkan masukkan no. identitas",
							number:"Silahkan masukkan angka numerik"
						},
						nama:{
							required:"Silahkan masukkan nama",
							pattern:"Silahkan masukkan huruf alphabet atau angka numerik"
						},
						tempat:"Silahkan masukkan tempat lahir",
						tanggal:"Silahkan masukkan tanggal lahir",
						alamat:"Silahkan masukkan alamat",
						kelamin:"Silahkan masukkan jenis kelamin",
						hp:{
							required:"Silahkan masukkan hp",
							number:"Silahkan masukkan angka numerik"
						},
						email:{
							email:"Silahkan masukkan format email",
							pattern:"Silahkan masukkan format email"
						},
						guide:"Silahkan masukkan guide",
						transaksi:"Silahkan masukkan transaksi",
						bank:"Silahkan masukkan bank",
						rekening:{
							required:"Silahkan masukkan no. rekening",
							number:"Silahkan masukkan angka numerik"
						},
						ibu:"Silahkan masukkan nama ibu",
						shadow:"Silahkan masukkan scan identitas",
						foto:"Silahkan masukkan foto"
					},
					highlight: function(element) {
						$(element).closest('.form-group .col-sm-4').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group .col-sm-4').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block',
					errorPlacement: function(error, element) {
						if(element.parent('.input-group').length) {
							error.insertAfter(element.parent());
						}

						else{
							error.insertAfter(element);
						}
					}
				});

				/* Show Detail Guide */
				$("button.see.guide").on("click", function(){
					var id = $(this).data("id");

					$.ajax({
						type:"GET",
						url:"/ticketing/public/guide/" + id,
						dataType:"json",
						success:function(response){
							console.log(response);
							$("#modalDetailGuide").modal();
							$("#labelIDGuide").text(response.id_guide);
							$("#labelNoIdentitas").text(response.no_identitas);
							$("#labelNama").text(response.nama);
							$("#labelJenisKelamin").text(response.jenis_kelamin);
							$("#labelTempatLahir").text(response.tempat_lahir);
							$("#labelTanggalLahir").text(response.tgl_lahir);
							$("#labelAlamat").text(response.alamat);
							$("#labelHandphone").text(response.no_hp);
							$("#labelEmail").text(response.email);
							$("#labelJenisGuide").text(response.jenis_guide);
							$("#labelIDTravel").text(response.id_travel);
							$("#labelTravel").text(response.nama_travel);
							$("#labelJenisTransaksi").text(response.jenis_transaksi);
							$("#labelNoRekening").text(response.no_rek);
							$("#labelBank").text(response.bank);
							$("#labelKTP").text(response.ktp == 1 ? 'Ada' : 'Tidak Ada');
							$("#labelSIMA").text(response.sim_a == 1 ? 'Ada' : 'Tidak Ada');
							$("#labelSIMB").text(response.sim_b == 1 ? 'Ada' : 'Tidak Ada');
							$("#labelSIMC").text(response.sim_c == 1 ? 'Ada' : 'Tidak Ada');
							$("#labelLisensi").text(response.lisensi == 1 ? 'Ada' : 'Tidak Ada');
							$("#labelPassport").text(response.passport == 1 ? 'Ada' : 'Tidak Ada');
							$("#labelIdentitas").html(response.scan_identitas == "" ? "Tidak Ada" : "<img class=\"img-thumbnail\" src=\"/ticketing/public/identitas/" + response.scan_identitas + "\" alt=\"Identitas\">");
							$("#labelFoto").html(response.foto == "" ? "Tidak Ada" : "<img class=\"img-thumbnail\" src=\"" + response.foto + "\" alt=\"Foto\">");
							$("#labelNamaIbuKandung").text(response.nama_ibu_kandung);
							$("#labelStatusGuide").text(response.status_guide);
						}
					});
				});

				/* Show Detail Guide */
				$("button.open.guide").on("click", function(){
					var id = $(this).data("id");

					$.ajax({
						type:"GET",
						url:"/ticketing/public/guide/" + id,
						dataType:"json",
						success:function(response){
							console.log(response);
							$("#modalGuide").modal();
							$("#textIDGuide").val(response.id_guide);
							$("#textNoIdentitas").val(response.no_identitas);
							$("#textNama").val(response.nama);
							$("#selectJenisKelamin").val(response.jenis_kelamin);
							$("#textTempatLahir").val(response.tempat_lahir);
							$("#textTanggalLahir").val(response.tgl_lahir);
							$("#textareaAlamat").val(response.alamat);
							$("#textHandphone").val(response.no_hp);
							$("#textEmail").val(response.email);
							$("#selectJenisGuide").val(response.jenis_guide);
							$("#textIDTravel").val(response.id_travel);
							$("#textTravel").val(response.nama_travel);
							$("#selectJenisTransaksi").val(response.jenis_transaksi);
							$("#textNoRekening").val(response.no_rek);
							$("#textBank").val(response.bank);
							$("#checkboxKTP").prop('checked', response.ktp == 1 ? true : false);
							$("#checkboxSIMA").prop('checked', response.sim_a == 1 ? true : false);
							$("#checkboxSIMB").prop('checked', response.sim_b == 1 ? true : false);
							$("#checkboxSIMC").prop('checked', response.sim_c == 1 ? true : false);
							$("#checkboxLisensi").prop('checked', response.lisensi == 1 ? true : false);
							$("#checkboxPassport").prop('checked', response.passport == 1 ? true : false);
							$("#textNamaIbuKandung").val(response.nama_ibu_kandung);
							$("#selectStatusGuide").val(response.status_guide);
							$("#textFoto").val(response.foto);
							$("#textShadow").val(response.scan_identitas);
							$(":file").filestyle("placeholder", response.scan_identitas);
							$("#buttonSave").data("value", "update");

							$("#formGuide").attr("action", "http://192.168.15.150/ticketing/public/guide/update/" + $("#textIDGuide").val());
						}
					});
				});

				/* Delete Guide */
				$("button.delete.guide").on("click", function(){
					var id = $(this).data("id");

					bootbox.confirm({
						size:"small",
						title:"Konfirmasi",
						message:"Hapus Guide?",
						callback:function(result){
							if(result){
								$.ajax({
									type:"DELETE",
									url:"/ticketing/public/guide/delete/" + id,
									success:function(response){
										toastr.success("Sukses Menjalankan Perintah");
										setTimeout(function(){ window.location.href = location.href; }, 3000);
									}
								});
							}
						}
					});
				});

				/* Take Foto */
				$("button.take.foto").on("click", function(){
					Webcam.snap(function(result){
						var foto = "<img src=\"" + result + "\">";
						$("#hasil").html(foto);
						$("#textFoto").val(result);
					});
				});

				Webcam.set({
					width:250,
					height:188,
					image_format:"jpeg",
					jpeg_quality:100
				});

				Webcam.attach("#kamera");

				$(".box form").on("submit", function(e){
					e.preventDefault();

					var nama = $("#textFindNamaGuide").val() == "" ? "undefined" : $("#textFindNamaGuide").val();
					var referensi = $("#textFindReferensiGuide").val() == "" ? "undefined" : $("#textFindReferensiGuide").val();

					if(nama != "undefined" || referensi != "undefined"){
						var url = this.action + '/' + nama + '/' + referensi;
						window.location.href = url;
					}
				});
			});

			function user_register(id_guide) {
				$('body').ajaxMask();

				regStats = 0;
				regCt = -1;

				notifGagal = "";

				try
				{
					timer_register.stop();
				}

				catch(err)
				{
					console.log('Registration timer has been init');
				}

				var limit = 4;
				var ct = 1;
				var timeout = 5000;

				$('.loading-pro' + id_guide).css('display', 'inline');

				timer_register = $.timer(timeout, function() {
					user_checkregister(id_guide);

					if (ct >= limit || regStats == 1) 
					{
						timer_register.stop();

						console.log("'" + id_guide + "' registration checking end");

						if (ct >= limit && regStats == 0)
						{
							$('.loading-pro' + id_guide).css('display', 'none');
							$('body').ajaxMask({ stop: true });
						}

						if (regStats == 1)
						{
							$('.loading-pro' + id_guide).css('display', 'none');
							$("#jmlSidikJari").val(regCt);

							toastr.success("Sukses Menjalankan Perintah");

							setTimeout(function(){ window.location.href = location.href; }, 3000);

							$('body').ajaxMask({ stop: true });
						}
					}

					ct++;
				});
			}

			function user_checkregister(id_guide, jmlSidikJari) {
				$.ajax({
					url:$("#urlAction").val()+"checkreg.php?action=checkreg&id_guide=" + id_guide,
					type:"GET",
					success:function(data)
					{
						try
						{
							var res = jQuery.parseJSON(data);	
							if (res.result)
							{
								regStats = 1;
								$.each(res, function(key, value){
									if (key == 'current')
									{
										regCt = value;
									}
								});
							}

							else
							{
								notifGagal = res.notif;
							}
						}

						catch(err)
						{
							alert(err.message);
						}
					}
				});
			}
		</script>
	</section>
	@include('shared.searchtravel')
@endsection