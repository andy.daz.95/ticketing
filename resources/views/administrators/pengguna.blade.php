@extends('layouts.app')

@section('content')
	<section class="content-header">
		<h1>Pengguna</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header with-border">Temukan Pengguna</div>
					<div class="box-body">
						<div class="col-sm-9">
							<form class="form-horizontal" action="{{ url('pengguna/find') }}" method="get">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="textFindPengguna">Temukan</label>
									<div class="col-sm-10">
										<div class="input-group">
											<input class="form-control" id="textFindPengguna" type="text" placeholder="Nama Pengguna">
											<div class="input-group-btn">
												<button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="col-sm-3 text-right">
							<button data-trigger="new" class="btn btn-flat btn-primary showModalPengguna" type="button" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Buat Baru</button>
						</div>
					</div>
				</div>
			</div>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Daftar Informasi</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th class="hide">ID</th>
									<th>Kategori</th>
									<th>Nama</th>
									<th>Status</th>
									<th>&nbsp;</th>
								</thead>
								<tbody>
									@if(count($penggunas) == 0)
									<tr>
										<td colspan="5"><small><i>Data tidak ditemukan.</i></small></td>
									</tr>
									@else
									@foreach($penggunas as $key=>$pengguna)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td class="hide">{{ $pengguna->id_pengguna }}</td>
										<td>{{ $pengguna->pengguna_kategori }}</td>
										<td>{{ $pengguna->nama_lengkap }}</td>
										<td>
											@if($pengguna->status_pengguna == 1)
											<label class="label label-success"><i class="fa fa-check"></i> Aktif</label>
											@else
											<label class="label label-danger"><i class="fa fa-times"></i> Nonaktif</label>
											@endif
										</td>
										<td>
											<button data-trigger="change" class="btn btn-flat btn-success btn-xs open pengguna showModalPengguna" type="button" data-id="{{ $pengguna->id_pengguna }}"><i class="fa fa-pencil"></i></button>
											<button class="btn btn-flat btn-danger btn-xs delete pengguna" type="button" data-id="{{ $pengguna->id_pengguna }}"><i class="fa fa-trash"></i></button>
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
							{{ $penggunas->links() }}
						</div>
					</div>
					<div class="box-footer">
						<small><i>Silahkan melakukan pengolahan data pengguna.</i></small>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Pengguna -->
		<div class="modal fade" id="modalPengguna">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form class="form-horizontal" id="formPengguna">
						<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
							<h4 class="modal-title">Data Pengguna</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDPengguna"><b class="text-danger">*</b> ID</label>
								<div class="col-sm-10">
									<input class="form-control" id="textIDPengguna" type="text" placeholder="ID" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><b class="text-danger">*</b> Kategori</label>
								<div class="col-sm-10">
										@foreach ($penggunakategoris as $key)
											@if($key->id_pengguna_kategori != "0")
												<label class="radio-inline"><input name="kategori" type="radio" value="{{ $key->id_pengguna_kategori }}">{{ $key->pengguna_kategori }}</label>
											@endif
										@endforeach
									<br>
									<span id="notif-kategori" class="text-danger" style="display: none;">Silahkan masukkan kategori pengguna</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textNamaLengkap"><b class="text-danger">*</b> Nama Lengkap</label>
								<div class="col-sm-10">
									<input class="form-control" id="textNamaLengkap" name="nama" type="text" placeholder="Nama Lengkap">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="selectStatusPengguna"><b class="text-danger">*</b> Status Pengguna</label>
								<div class="col-sm-10">
									<select class="form-control" id="selectStatusPengguna" name="statusPengguna">
										<option value="">Choose</option>
										<option value="0">Off</option>
										<option value="1" selected>On</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textUserName"><b class="text-danger">*</b> User Name</label>
								<div class="col-sm-10">
									<input class="form-control" id="textUserName" name="username" type="text" placeholder="User Name">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textPassword"><b class="text-danger password">*</b> Password</label>
								<div class="col-sm-10">
									<input class="form-control" id="textPassword" name="password" type="password" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Keluar</button>
							<button class="btn btn-flat btn-primary save pengguna" id="buttonSave" type="submit" data-value="store">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.showModalPengguna').click(function(){
					var trigger=$(this).attr('data-trigger');
					if(trigger=="new")
					{
						$('button.reset.pengguna').attr('disabled','disabled');
					}

					else
					{
						$('button.reset.pengguna').removeAttr('disabled');
						$('b.password').attr('class', 'hide');
					}

					$('#modalPengguna').modal('show');
				});

				$("#textUserName").on("keypress", function(e){
					if(e.which == 32){
						return false;
					}
				});

				$("#formPengguna").validate({
					rules: {
						nama:{
							required:true,
							pattern:/^([a-zA-Z\ ]){1,100}$/
						},
						statusPengguna:{
							required:{
								depends: function(element){
									return $("#textIDPengguna").val() == ""
								}
							}
						},
						username:{
							required:true,
							pattern:/^(?![0-9]*$)[a-zA-Z0-9]+$/
						},
						password:{
							required:{
								depends: function(element){
									return $("#textIDPengguna").val().length == 0
								}
							}
						}
					},
					messages: {
						nama:{
							required:"Silahkan masukkan nama",
							pattern:"Silahkan masukkan huruf alphabet"
						},
						statusPengguna:"Silahkan pilih status pengguna",
						username:{
							required:"Silahkan masukkan username",
							pattern:"Silahkan masukkan huruf alphabet"
						},
						password:"Silahkan masukkan password"
					},
					submitHandler: function(form) {
						if($("#selectStatusPengguna").val() != "" && $('input[name=kategori]:checked', '#formPengguna').val() != undefined)
						{
							var method = "";
							var url = "";
							var data = {
								kategori:$('input[name=kategori]:checked', '#formPengguna').val(),
								nama:$("#textNamaLengkap").val(),
								status:$("#selectStatusPengguna").val(),
								username:$("#textUserName").val(),
								password:$("#textPassword").val()
							};
	
							if($("button.save.pengguna").data("value") == "store"){
								method = "POST";
								url = "/ticketing/public/pengguna/store";
							}
	
							if($("button.save.pengguna").data("value") == "update"){
								var id = $("#textIDPengguna").val();
	
								method = "PUT";
								url = "/ticketing/public/pengguna/update/" + id;
							}
		
							$.ajax({
								type:method,
								url:url,
								data:data,
								success:function(response){
									if(response == '0'){
										toastr.warning("Username Sudah Digunakan");
									}
	
									else{
										toastr.success("Sukses Menjalankan Perintah");
										setTimeout(function(){ window.location.href = location.href; }, 3000);
									}
								}
							});
						}
						else
						{
							if($("#selectStatusPengguna").val() == "")
							{
								$("#selectStatusPengguna").focus();
							}

							if($('input[name=kategori]:checked', '#formPengguna').val() == undefined)
							{
								$('#notif-kategori').css('display', 'inline');
							}
						}
					},
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block',
					errorPlacement: function(error, element) {
						if(element.parent('.input-group').length) {
							error.insertAfter(element.parent());
						}

						else{
							error.insertAfter(element);
						}
					}
				});

				/* Modal Pengguna When Closing */
				$("#modalPengguna").on("hidden.bs.modal", function(){
					$("form").trigger('reset');
					$("#buttonSave").data("value", "store");
				});

				/* Show Detail Pengguna */
				$("button.open.pengguna").on("click", function(){
					var id = $(this).data("id");

					$.ajax({
						type:"GET",
						url:"/ticketing/public/pengguna/" + id,
						dataType:"json",
						success:function(response){
							console.log(response);
							$("#modalPengguna").modal();

							$.each(response, function(index, value){
								$("#textIDPengguna").val(value.id_pengguna);
								$("input:radio[value="+value.id_pengguna_kategori+"]").prop("checked", true);
								$("#textKategori").val(value.pengguna_kategori);
								$("#textNamaLengkap").val(value.nama_lengkap);
								$("#selectStatusPengguna").val(value.status_pengguna);
								$("#textUserName").val(value.nama_pengguna);
							});

							$("#buttonSave").data("value", "update");
						}
					});
				});

				/* Store Or Update Pengguna */
				$("button.save.pengguna").on("click", function(e){
				});

				/* Delete Pengguna */
				$("button.delete.pengguna").on("click", function(){
					var id = $(this).data("id");

					bootbox.confirm({
						size:"small",
						title:"Konfirmasi",
						message:"Hapus Pengguna?",
						callback:function(result){
							if(result){
								$.ajax({
									type:"DELETE",
									url:"/ticketing/public/pengguna/delete/" + id,
									success:function(response){
										toastr.success("Sukses Menjalankan Perintah");
										setTimeout(function(){ window.location.href = location.href; }, 3000);
									}
								});
							}
						}
					});
				});

				/* Reset Password */
				$("button.reset.pengguna").on("click", function(){
					var id = $("#textIDPengguna").val();

					$.ajax({
						type:"PUT",
						url:"/ticketing/public/pengguna/reset/" + id,
						data:{
							password:'123456'
						},
						success:function(response){
							toastr.success("Sukses Mendapatkan Password");
						}
					});
				});

				$(".box form").on("submit", function(e){
					e.preventDefault();

					var id = $("#textFindPengguna").val();

					if(id != ""){
						var url = this.action + '/' + id;
						window.location.href = url;
					}
				});
			});
		</script>
	</section>
	@include('shared.searchpenggunakategori')
@endsection