@extends('layouts.app')

@section('content')
	<section class="content-header">
		<h1>Log</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Daftar Informasi</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th>ID</th>
									<th>Pengguna</th>
									<th>Aktivitas</th>
									<th>Waktu</th>
									<th>&nbsp;</th>
								</thead>
								<tbody>
									@if(count($logs) == 0)
									<tr>
										<td colspan="6"><small><i>Data tidak ditemukan.</i></small></td>
									</tr>
									@else
									@foreach($logs as $key=>$log)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td>{{ $log->id_log }}</td>
										<td>{{ $log->nama_lengkap }}</td>
										<td>{{ $log->aktivitas }}</td>
										<td>{{ $log->wkt_log }}</td>
										<td><button class="btn btn-flat btn-default btn-xs open log" type="button"><i class="fa fa-lock"></i></button></td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
							{{ $logs->links() }}
						</div>
					</div>
					<div class="box-footer"><small><i>Data log tidak dapat diubah.</i></small></div>
				</div>
			</div>
		</div>
	</section>
@endsection