@extends('layouts.app')

@section('content')
	<section class="content">
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<form class="form-horizontal" action="{{ url('laporantransaksitiket/excel') }}" method="post">
						{{ csrf_field() }}
						<div class="box-header with-border">Laporan Harian Transaksi Tiket</div>
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTanggalMulai">Tanggal Mulai</label>
								<div class="col-sm-4">
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textTanggalMulai" name="mulai" type="text" placeholder="Tanggal Mulai">
									</div>
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textWaktuMulai" name="waktuMulai" type="text" placeholder="Waktu Awal">
									</div>
								</div>
								<label class="col-sm-2 control-label" for="textTanggalAkhir">Tanggal Akhir</label>
								<div class="col-sm-4">
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control" id="textTanggalAkhir" name="selesai" type="text" placeholder="Tanggal Akhir">
									</div>
									<div class="col-sm-6" style="padding: 0;">
										<input class="form-control col-sm-6" id="textWaktuAkhir" name="waktuAkhir" type="text" placeholder="Waktu Akhir">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="selectTiket">Tiket</label>
								<div class="col-sm-4">
									<select class="form-control" id="selectTiket" name="tiket">
										<option value="">Semua Tiket</option>
										@foreach($kategoritikets as $kategoritiket)
											<option value="{{ $kategoritiket->id_tiket }}">{{ $kategoritiket->kategori_tiket }}</option>
										@endforeach
									</select>
								</div>
								<label class="col-sm-2 control-label" for="selectPetugasTiket">Petugas Tiket</label>
								<div class="col-sm-4">
									<select class="form-control" id="selectPetugasTiket" name="petugas">
										<option value="">Semua Petugas Tiket</option>
										@foreach($penggunas as $pengguna)
											<option value="{{ $pengguna->id_pengguna }}">{{ $pengguna->nama_pengguna }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="box-footer text-right">
							<button class="btn btn-flat btn-primary search laporantransaksitiket" id="buttonSearch" type="button" data-value="store"><i class="fa fa-search"></i> Cari</button>
							<button class="btn btn-flat btn-success export laporantransaksitiket" id="buttonExport" type="submit" data-value="store"><i class="fa fa-download"></i> Ekspor</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Laporan Harian Transaksi Tiket</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th>ID</th>
									<th>Kategori</th>
									<th>Harga</th>
									<th>Diskon</th>
									<th>Komisi</th>
									<th>Waktu</th>
									<th>Jenis Pembayaran</th>
								</thead>
								<tfoot>
									<tr>
										<td colspan="3"><b>Total</b></td>
										<td class="price">0</td>
										<td colspan="3"></td>
									</tr>
								</tfoot>
								<tbody class="rows laporantransaksitiket">
									<tr><td colspan="8"><small><i>Data tidak ditemukan.</i></small></td></tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer">
						<small><i>Anda juga dapat mengekpor hasil pencarian dalam format excel.</i></small>
					</div>
					<div class="overlay">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("td.price").autoNumeric("init", {aSign:'Rp '});
				$("td.discount").autoNumeric("init", {aSign:' %', pSign:'s'});

				var now     = new Date(); 
                var year    = now.getFullYear();
                var month   = now.getMonth() + 1 + ""; if(month.length == 1)	month = "0" + month;
                var day     = now.getDate() + ""; if(day.length == 1) day = "0" + day;

                $("#textTanggalMulai").val('01-' + month + '-' + year);
                $("#textTanggalAkhir").val(day + '-' + month + '-' + year);
				
				/* Show Date Picker */
				$("#textTanggalMulai, #textTanggalAkhir").datepicker({
					language: 'en',
					dateFormat: 'dd-mm-yyyy',
					maxDate: new Date(),
					setDate: '21-07-2016'
				});

				$("#textWaktuMulai, #textWaktuAkhir").timepicker({
					showMeridian:false
				});

				$("#textWaktuMulai").val('00:00');
				$("#textWaktuAkhir").val('23:59');

				$("#buttonSearch").on("click", function(){
					var tmp = $("#textTanggalMulai").val().split('-');
					var wkt = $("#textWaktuMulai").val();

					if(wkt.length == 4)
					{
						wkt = '0' + wkt;
					}

					var waktuMulai = tmp[2] + '-' + tmp[1] + '-' + tmp[0] + ' ' + wkt + ':00';

					var tmp = $("#textTanggalAkhir").val().split('-');
					var wkt = $("#textWaktuAkhir").val();

					if(wkt.length == 4)
					{
						wkt = '0' + wkt;
					}

					var waktuAkhir = tmp[2] + '-' + tmp[1] + '-' + tmp[0] + ' ' + $("#textWaktuAkhir").val() + ':00';
					
					$.ajax({
						type:"POST",
						url:"/ticketing/public/laporantransaksitiket",
						data:{
							mulai:waktuMulai,
							selesai:waktuAkhir,
							tiket:$("#selectTiket").val(),
							petugas:$("#selectPetugasTiket").val()
						},
						success:function(response){
							var total = $("tfoot td").eq(1);
							var jumlah = 0;
							var row = "";

							if(response.length == 0){
								row = "<tr><td colspan='8'><small><i>Data tidak ditemukan.</i></small></td></tr>";
							}

							else{
								var no = 1;

								$.each(response, function(i, value){
									jumlah = jumlah + parseInt(value.harga, 10);

									var dataJenis = "EDC";

									if(value.jenis_pembayaran == '0')
									{
										dataJenis="Cash";
									}

									row = row + "<tr>" +
												"<td>" + no++ + "</td>" +
												"<td>" + value.id_transaksi + "</td>" +
												"<td>" + value.kategori_tiket + "</td>" +
												"<td class='price'>" + value.harga + "</td>" +
												"<td>" + value.diskon + "%</td>" +
												"<td>" + value.komisi_guide + "%</td>" +
												"<td>" + value.wkt_transaksi + "</td>" +
												"<td>" +  dataJenis + "</td>" +
												"</tr>";
								});
							}

							$("tbody.rows.laporantransaksitiket").html(row);
							$("td.price").autoNumeric("init", {aSign:'Rp '});
							total.autoNumeric("set", jumlah);
						}
					});
				});
			});
		</script>
	</section>
@endsection