@extends('layouts.app')

@section('content')
	<section class="content-header">
		<h1>Travel</h1>
	</section>
	<section class="content">
		<div class='row'>
			<div class="col-sm-12">
				<div class="box">
					<div class="box-header with-border">Temukan Travel</div>
					<div class="box-body">
						<div class="col-sm-9">
							<form class="form-horizontal" action="{{ url('travel/find') }}" method="get">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="textFindTravel">Temukan</label>
									<div class="col-sm-10">
										<div class="input-group">
											<input class="form-control" id="textFindTravel" type="text" placeholder="Nama Travel">
											<div class="input-group-btn">
												<button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="col-sm-3 text-right">
							<button class="btn btn-flat btn-primary" type="button" data-toggle="modal" data-target="#modalTravel" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Buat Baru</button>
						</div>
					</div>
				</div>
			</div>
			<div class='col-sm-12'>
				<div class="box">
					<div class="box-header with-border">Daftar Informasi</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<th>No.</th>
									<th class="hide">ID</th>
									<th>Nama</th>
									<th>Travel Komisi</th>
									<th>Guide Komisi</th>
									<th>Status</th>
									<th>&nbsp;</th>
								</thead>
								<tbody>
									@if(count($travels) == 0)
									<tr>
										<td colspan="6"><small><i>Data tidak ditemukan.</i></small></td>
									</tr>
									@else
									@foreach($travels as $key=>$travel)
									<tr>
										<td>{{ $key + 1 }}</td>
										<td class="hide">{{ $travel->id_travel }}</td>
										<td>{{ $travel->nama_travel }}</td>
										<td>{{ $travel->travel_komisi }}%</td>
										<td>{{ $travel->guide_komisi }}%</td>
										<td>
											@if($travel->status_travel == 1)
											<label class="label label-success"><i class="fa fa-check"></i> Aktif</label>
											@else
											<label class="label label-danger"><i class="fa fa-times"></i> Nonaktif</label>
											@endif
										</td>
										<td>
											<button class="btn btn-flat btn-success btn-xs open travel" type="button" data-id="{{ $travel->id_travel }}"><i class="fa fa-pencil"></i></button>
											<button class="btn btn-flat btn-danger btn-xs delete travel" type="button" data-id="{{ $travel->id_travel }}"><i class="fa fa-trash"></i></button>
										</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
							{{ $travels->links() }}
						</div>
					</div>
					<div class="box-footer">
						<small><i>Silahkan melakukan pengolahan data travel.</i></small>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Travel -->
		<div class="modal fade" id="modalTravel">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form class="form-horizontal" id="formTravel">
						<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
							<h4 class="modal-title">Data Travel</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textIDTravel"><b class="text-danger">*</b> ID</label>
								<div class="col-sm-10">
									<input class="form-control" id="textIDTravel" type="text" placeholder="ID" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textNamaTravel"><b class="text-danger">*</b> Nama Travel</label>
								<div class="col-sm-10">
									<input class="form-control" id="textNamaTravel" name="nama" type="text" placeholder="Nama Travel">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textTravelKomisi"><b class="text-danger">*</b> Travel Komisi</label>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textTravelKomisi" name="travel" type="text" placeholder="Travel Komisi">
										<span class="input-group-addon">%</span>
									</div>
									<small class="text-primary"><i>Tanda desimal dengan tanda baca titik.</i></small>
								</div>
								<label class="col-sm-2 control-label" for="textGuideKomisi"><b class="text-danger">*</b> Guide Komisi</label>
								<div class="col-sm-4">
									<div class="input-group">
										<input class="form-control" id="textGuideKomisi" name="guide" type="text" placeholder="Guide Komisi">
										<span class="input-group-addon">%</span>
									</div>
									<small class="text-primary"><i>Tanda desimal dengan tanda baca titik.</i></small>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textBank"><b class="text-danger">*</b> Bank</label>
								<div class="col-sm-4">
									<input class="form-control" id="textBank" name="bank" type="text" placeholder="Bank">
								</div>
								<label class="col-sm-2 control-label" for="textNoRekening"><b class="text-danger">*</b> No. Rekening</label>
								<div class="col-sm-4">
									<input class="form-control" id="textNoRekening" name="rekening" type="text" placeholder="No. Rekening">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textAtasNamaRekening"><b class="text-danger">*</b> Atas Nama</label>
								<div class="col-sm-4">
									<input class="form-control" id="textAtasNamaRekening" name="atas" type="text" placeholder="Atas Nama Rekening">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="textareaAlamat"><b class="text-danger">*</b> Alamat</label>
								<div class="col-sm-4">
									<textarea class="form-control" id="textareaAlamat" name="alamat"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-flat btn-default" type="button" data-dismiss="modal">Keluar</button>
							<button class="btn btn-flat btn-primary save travel" id="buttonSave" type="submit" data-value="store">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				/* Global Variabel */
				$("#textTravelKomisi").autoNumeric("init", {vMin:'0.00', vMax:'100.00'});
				$("#textGuideKomisi").autoNumeric("init", {vMin:'0.00', vMax:'100.00'});

				/* Modal Travel When Closing */
				$("#modalTravel").on("hidden.bs.modal", function(){
					$("form").trigger('reset');
					$("#buttonSave").data("value", "store");
				});

				$("#formTravel").validate({
					rules: {
						nama:{
							required:true,
							pattern:/^[a-zA-Z0-9\ ]{1,100}$/
						},
						travel:"required",
						guide:"required",
						bank:"required",
						rekening:"required",
						atas:"required",
						alamat:"required"
					},
					messages: {
						nama:{
							required:"Silahkan masukkan nama",
							pattern:"Silahkan masukkan huruf alphabet atau angka numerik"
						},
						travel:"Silahkan masukkan travel komisi",
						guide:"Silahkan masukkan guide komisi",
						bank:"Silahkan masukkan bank",
						rekening:"Silahkan masukkan no. rekening",
						atas:"Silahkan masukkan atas nama rekening",
						alamat:"Silahkan masukkan alamat"
					},
					submitHandler: function(form, event) {
						var method = "";
						var url = "";
						var data = {
							nama:$("#textNamaTravel").val(),
							travel:$("#textTravelKomisi").autoNumeric("get"),
							guide:$("#textGuideKomisi").autoNumeric("get"),
							bank:$("#textBank").val(),
							rekening:$("#textNoRekening").val(),
							atas:$("#textAtasNamaRekening").val(),
							alamat:$("#textareaAlamat").val()
						};

						if($("button.save.travel").data("value") == "store"){
							method = "POST";
							url = "/ticketing/public/travel/store";
						}

						if($("button.save.travel").data("value") == "update"){
							var id = $("#textIDTravel").val();

							method = "PUT";
							url = "/ticketing/public/travel/update/" + id;
						}

						if((parseFloat($("#textTravelKomisi").autoNumeric("get")) + parseFloat($("#textGuideKomisi").autoNumeric("get"))) == 100)
						{
							$.ajax({
								type:method,
								url:url,
								data:data,
								success:function(response){
									toastr.success("Sukses Menjalankan Perintah");
									setTimeout(function(){ window.location.href = location.href; }, 3000);
								}
							});
						}

						else
						{
							event.preventDefault();
							toastr.info("Besaran Persentase Harus 100%");
						}
					},
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block',
					errorPlacement: function(error, element) {
						if(element.parent('.input-group').length) {
							error.insertAfter(element.parent());
						}

						else{
							error.insertAfter(element);
						}
					}
				});

				/* Show Detail Travel */
				$("button.open.travel").on("click", function(){
					var id = $(this).data("id");

					$.ajax({
						type:"GET",
						url:"/ticketing/public/travel/" + id,
						dataType:"json",
						success:function(response){
							console.log(response);
							$("#modalTravel").modal();
							$("#textIDTravel").val(response.id_travel);
							$("#textNamaTravel").val(response.nama_travel);
							$("#textTravelKomisi").autoNumeric("set", response.travel_komisi);
							$("#textGuideKomisi").autoNumeric("set", response.guide_komisi);
							$("#textBank").val(response.bank);
							$("#textNoRekening").val(response.no_rek);
							$("#textAtasNamaRekening").val(response.atas_nama);
							$("#textareaAlamat").val(response.alamat_travel);
							$("#buttonSave").data("value", "update");
						}
					});
				});

				/* Store Or Update Travel */
				$("button.save.travel").on("click", function(){
				});

				/* Delete Travel */
				$("button.delete.travel").on("click", function(){
					var id = $(this).data("id");

					bootbox.confirm({
						size:"small",
						title:"Konfirmasi",
						message:"Hapus Travel?",
						callback:function(result){
							if(result){
								$.ajax({
									type:"DELETE",
									url:"/ticketing/public/travel/delete/" + id,
									success:function(response){
										toastr.success("Sukses Menjalankan Perintah");
										setTimeout(function(){ window.location.href = location.href; }, 3000);
									}
								});
							}
						}
					});
				});

				$(".box form").on("submit", function(e){
					e.preventDefault();

					var id = $("#textFindTravel").val();

					if(id != ""){
						var url = this.action + '/' + id;
						window.location.href = url;
					}
				});
			});
		</script>
	</section>
@endsection