<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf_token" content="{!! csrf_token() !!}">
		<title>Wopika</title>
		<link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="icon" href="{{ URL::asset('favicon.ico') }}">
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo"><a href="#"><b>W</b>opika</a></div>
			<div class="login-box-body">
				<form method="post" action="{{ url('/password/email') }}">
					{{ csrf_field() }}
					<div class="form-group has-feedback">
						<input class="form-control" type="email" name="email" placeholder="Email">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<button class="btn btn-primary btn-block btn-flat" type="submit">Send Password Reset Link</button>
					</div>
				</form>
			</div>
		</div>
		<script src="{{ URL::asset('theme/plugins/jquery/jquery.min.js') }}"></script>
		<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
	</body>
</html>