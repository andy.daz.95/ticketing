<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf_token" content="{!! csrf_token() !!}">
		<title>Ticketing</title>
		<link rel="stylesheet" href="{{ URL::asset('theme/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/dist/css/AdminLTE.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('theme/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="icon" href="{{ URL::asset('favicon.ico') }}">
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo"><a href="#"><b>T</b>icketing</a></div>
			@if(session('failed') == 1)
			<div class="alert alert-danger"><i class="fa fa-warning"></i> Gagal Masuk Username atau Password Salah.</div>
			{{ session()->forget('failed') }}
			@endif
			<div class="login-box-body">
				<form method="post" action="{{ url('/login') }}">
					{{ csrf_field() }}
					<div class="form-group has-feedback">
						<input class="form-control" type="text" name="username" placeholder="Username">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input class="form-control" type="password" name="password" placeholder="Password">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<button class="btn btn-primary btn-block btn-flat" type="submit">Masuk</button>
					</div>
				</form>
			</div>
			<p style="margin-top:40px" class="text-center">
				<small>
					<b><i>Ticketing</i></b> Aplikasi Manajemen Tiket<br>
					OS Team Development<br>
					Copyright &copy;2016
				</small>
			</p>
		</div>
		<script src="{{ URL::asset('theme/plugins/jquery/jquery.min.js') }}"></script>
		<script src="{{ URL::asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
	</body>
</html>